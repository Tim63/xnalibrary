﻿#region File Description
//-----------------------------------------------------------------------------
// OrientedBoundingBox.cs
// 
// Update: 2013/12/23
// References: http://marupeke296.com/
//             http://clb.demon.fi/MathGeoLib/nightly/
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
#endregion

namespace XnaLibrary.Geometry
{
    /// <summary>
    /// 有効境界ボックス（OBB）
    /// </summary>
    public class OrientedBoundingBox
    {
        #region Properties

        /// <summary>
        /// 中心座標
        /// </summary>
        public Vector3 Center { get; private set; }

        /// <summary>
        /// 中心から各辺への長さ
        /// </summary>
        public Vector3 HalfExtent { get; private set; }

        /// <summary>
        /// 大きさ
        /// </summary>
        public Vector3 Size { get { return HalfExtent * 2; } }

        /// <summary>
        /// 方向
        /// </summary>
        public Quaternion Orientation { get; private set; }

        /// <summary>
        /// 軸
        /// </summary>
        public IList<Vector3> Axes
        {
            get { return axes; }
        }
        IList<Vector3> axes;

        /// <summary>
        /// 中心から各辺への長さ
        /// </summary>
        public IList<float> HalfExtents
        {
            get { return halfExtents; }
        }
        IList<float> halfExtents;

        #endregion

        #region Constructor

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public OrientedBoundingBox()
            : this(Vector3.Zero, Vector3.One, Quaternion.Identity)
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="center">中心座標</param>
        /// <param name="size">大きさ</param>
        /// <param name="orientation">方向</param>
        public OrientedBoundingBox(Vector3 center, Vector3 size, Quaternion orientation)
        {
            Center = center;
            HalfExtent = size * 0.5f;
            halfExtents = Array.AsReadOnly(new[] { HalfExtent.X, HalfExtent.Y, HalfExtent.Z });
            Orientation = orientation;
            var rotation = Matrix.CreateFromQuaternion(orientation);
            axes = Array.AsReadOnly(new[] { rotation.Left, rotation.Up, rotation.Backward });
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="center">中心座標</param>
        /// <param name="size">大きさ</param>
        /// <param name="rotation">回転行列</param>
        public OrientedBoundingBox(Vector3 center, Vector3 size, Matrix rotation)
        {
            Center = center;
            HalfExtent = size * 0.5f;
            halfExtents = Array.AsReadOnly(new[] { HalfExtent.X, HalfExtent.Y, HalfExtent.Z });
            Orientation = Quaternion.CreateFromRotationMatrix(rotation);
            axes = Array.AsReadOnly(new[] { rotation.Left, rotation.Up, rotation.Backward });
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="box">バウンディングボックス</param>
        /// <param name="rotation">回転</param>
        public OrientedBoundingBox(BoundingBox box, Matrix rotation)
            : this((box.Max + box.Min) * 0.5f, box.Max - box.Min, rotation)
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="box">バウンディングボックス</param>
        /// <param name="orientation">方向</param>
        public OrientedBoundingBox(BoundingBox box, Quaternion orientation)
            : this((box.Max + box.Min) * 0.5f, box.Max - box.Min, orientation)
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="box">バウンディングボックス</param>
        public OrientedBoundingBox(BoundingBox box)
            : this((box.Max + box.Min) * 0.5f, box.Max - box.Min, Quaternion.Identity)
        {
        }

        #endregion

        #region Collisions

        /// <summary>
        /// 有向境界ボックス（OBB）と交差しているか
        /// </summary>
        /// <param name="box">有向境界ボックス</param>
        /// <returns>交差していると true / そうでなければ false</returns>
        public bool Intersects(OrientedBoundingBox box)
        {
            var leftDir = Axes[0];
            var leftVec = leftDir * HalfExtent.X;
            var upDir = Axes[1];
            var upVec = upDir * HalfExtent.Y;
            var backDir = Axes[2];
            var backVec = backDir * HalfExtent.Z;

            var otherRightDir = box.Axes[0];
            var otherRightVec = otherRightDir * box.HalfExtent.X;
            var otherUpDir = box.Axes[1];
            var otherUpVec = otherUpDir * box.HalfExtent.Y;
            var otherBackDir = box.Axes[2];
            var otherBackVec = otherBackDir * box.HalfExtent.Z;

            var distance = Center - box.Center;

            float dot;
            Vector3.Dot(ref distance, ref leftDir, out dot);
            if (Math.Abs(dot) >
                leftVec.Length() + LengthSegmentOnSeparateAxis(ref leftDir, ref otherRightVec, ref otherUpVec, ref otherBackVec))
            {
                return false;
            }

            Vector3.Dot(ref distance, ref upDir, out dot);
            if (Math.Abs(dot) >
                upVec.Length() + LengthSegmentOnSeparateAxis(ref upDir, ref otherRightVec, ref otherUpVec, ref otherBackVec))
            {
                return false;
            }

            Vector3.Dot(ref distance, ref backDir, out dot);
            if (Math.Abs(dot) >
                backVec.Length() + LengthSegmentOnSeparateAxis(ref backDir, ref otherRightVec, ref otherUpVec, ref otherBackVec))
            {
                return false;
            }

            Vector3.Dot(ref distance, ref otherRightDir, out dot);
            if (Math.Abs(dot) >
                LengthSegmentOnSeparateAxis(ref otherRightDir, ref leftVec, ref upVec, ref backVec) + otherRightVec.Length())
            {
                return false;
            }

            Vector3.Dot(ref distance, ref otherUpDir, out dot);
            if (Math.Abs(dot) >
                LengthSegmentOnSeparateAxis(ref otherUpDir, ref leftVec, ref upVec, ref backVec) + otherUpVec.Length())
            {
                return false;
            }

            Vector3.Dot(ref distance, ref otherBackDir, out dot);
            if (Math.Abs(dot) >
                LengthSegmentOnSeparateAxis(ref otherBackDir, ref leftVec, ref upVec, ref backVec) + otherBackVec.Length())
            {
                return false;
            }


            Vector3 cross;
            Vector3.Cross(ref leftDir, ref otherRightDir, out cross);
            Vector3.Dot(ref distance, ref cross, out dot);
            if (Math.Abs(dot) >
                LengthSegmentOnSeparateAxis(ref cross, ref upVec, ref backVec) + LengthSegmentOnSeparateAxis(ref cross, ref otherUpVec, ref otherBackVec))
            {
                return false;
            }

            Vector3.Cross(ref leftDir, ref otherUpDir, out cross);
            Vector3.Dot(ref distance, ref cross, out dot);
            if (Math.Abs(dot) >
                LengthSegmentOnSeparateAxis(ref cross, ref upVec, ref backVec) + LengthSegmentOnSeparateAxis(ref cross, ref otherRightVec, ref otherBackVec))
            {
                return false;
            }

            Vector3.Cross(ref leftDir, ref otherBackDir, out cross);
            Vector3.Dot(ref distance, ref cross, out dot);
            if (Math.Abs(dot) >
                LengthSegmentOnSeparateAxis(ref cross, ref upVec, ref backVec) + LengthSegmentOnSeparateAxis(ref cross, ref otherRightVec, ref  otherUpVec))
            {
                return false;
            }

            Vector3.Cross(ref upDir, ref otherRightDir, out cross);
            Vector3.Dot(ref distance, ref cross, out dot);
            if (Math.Abs(dot) >
                LengthSegmentOnSeparateAxis(ref cross, ref  leftVec, ref  backVec) + LengthSegmentOnSeparateAxis(ref cross, ref otherUpVec, ref  otherBackVec))
            {
                return false;
            }

            Vector3.Cross(ref upDir, ref otherUpDir, out cross);
            Vector3.Dot(ref distance, ref cross, out dot);
            if (Math.Abs(dot) >
                LengthSegmentOnSeparateAxis(ref cross, ref  leftVec, ref backVec) + LengthSegmentOnSeparateAxis(ref cross, ref otherRightVec, ref  otherBackVec))
            {
                return false;
            }

            Vector3.Cross(ref upDir, ref  otherBackDir, out cross);
            Vector3.Dot(ref distance, ref cross, out dot);
            if (Math.Abs(dot) >
                LengthSegmentOnSeparateAxis(ref cross, ref leftVec, ref  backVec) + LengthSegmentOnSeparateAxis(ref cross, ref otherRightVec, ref  otherUpVec))
            {
                return false;
            }

            Vector3.Cross(ref backDir, ref  otherRightDir, out cross);
            Vector3.Dot(ref distance, ref cross, out dot);
            if (Math.Abs(dot) >
                LengthSegmentOnSeparateAxis(ref cross, ref leftVec, ref upVec) + LengthSegmentOnSeparateAxis(ref cross, ref otherUpVec, ref  otherBackVec))
            {
                return false;
            }

            Vector3.Cross(ref backDir, ref  otherUpDir, out cross);
            Vector3.Dot(ref distance, ref cross, out dot);
            if (Math.Abs(dot) >
                LengthSegmentOnSeparateAxis(ref cross, ref  leftVec, ref upVec) + LengthSegmentOnSeparateAxis(ref cross, ref  otherRightVec, ref otherBackVec))
            {
                return false;
            }

            Vector3.Cross(ref backDir, ref otherBackDir, out cross);
            Vector3.Dot(ref distance, ref cross, out dot);
            if (Math.Abs(dot) >
                LengthSegmentOnSeparateAxis(ref cross, ref  leftVec, ref  upVec) + LengthSegmentOnSeparateAxis(ref cross, ref  otherRightVec, ref  otherUpVec))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// １つでも有向境界ボックス（OBB）と交差しているか
        /// </summary>
        /// <param name="boxes">有向境界ボックス</param>
        /// <returns>１つでも交差していると true / そうでなければ false</returns>
        public bool Intersects(params OrientedBoundingBox[] boxes)
        {
            foreach (var other in boxes)
            {
                if (Intersects(other))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 軸並行境界ボックス（AABB）と交差しているか
        /// </summary>
        /// <param name="box">軸並行境界ボックス</param>
        /// <returns>交差していると true / そうでなければ false</returns>
        public bool Intersects(BoundingBox box)
        {
            return Intersects(new OrientedBoundingBox(box));
        }

        /// <summary>
        /// １つでも軸並行境界ボックス（AABB）と交差しているか
        /// </summary>
        /// <param name="boxes">軸並行境界ボックス</param>
        /// <returns>１つでも交差していると true / そうでなければ false</returns>
        public bool Intersects(params BoundingBox[] boxes)
        {
            foreach (var other in boxes)
            {
                if (Intersects(other))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 境界球と交差しているか
        /// </summary>
        /// <param name="sphere">境界球</param>
        /// <returns>交差していると true / そうでなければ false</returns>
        public bool Intersects(BoundingSphere sphere)
        {
            return Vector3.DistanceSquared(sphere.Center, GetClosestPoint(sphere.Center)) <= sphere.Radius * sphere.Radius;
        }

        /// <summary>
        /// １つでも境界球と交差しているか
        /// </summary>
        /// <param name="spheres">境界球</param>
        /// <returns>１つでも交差していると true / そうでなければ false</returns>
        public bool Intersects(params BoundingSphere[] spheres)
        {
            foreach (var other in spheres)
            {
                if (Intersects(other))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 平面と交差しているか
        /// </summary>
        /// <param name="plane">平面</param>
        /// <returns>交差していると true / そうでなければ false</returns>
        public bool Intersects(Plane plane)
        {
            var position = plane.Normal * plane.D;
            float length = 0;
            for (int i = 0; i < 3; i++)
            {
                length += Math.Abs(Vector3.Dot(Axes[i] * HalfExtents[i], plane.Normal));
            }
            return Vector3.Dot(Center - position, plane.Normal) - length < 0;
        }

        /// <summary>
        /// １つでも平面と交差しているか
        /// </summary>
        /// <param name="planes">平面</param>
        /// <returns>１つでも交差していると true / そうでなければ false</returns>
        public bool Intersects(params Plane[] planes)
        {
            foreach (var other in planes)
            {
                if (Intersects(other))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 点が有効境界ボックス（OBB）内かどうか
        /// </summary>
        /// <param name="position">点</param>
        /// <returns>有効境界ボックス（OBB）内だと true / そうでなければ false</returns>
        public bool Contains(ref Vector3 position)
        {
            var distance = position - Center;
            for (int i = 0; i < 3; i++)
            {
                float dot;
                var axis = Axes[i];
                Vector3.Dot(ref distance, ref axis, out dot);
                if (Math.Abs(dot) > HalfExtents[i])
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 点が有効境界ボックス（OBB）内かどうか
        /// </summary>
        /// <param name="position">点</param>
        /// <returns>有効境界ボックス（OBB）内だと true / そうでなければ false</returns>
        public bool Contains(Vector3 position)
        {
            return Contains(ref position);
        }

        /// <summary>
        /// １つでも点が有効境界ボックス（OBB）内かどうか
        /// </summary>
        /// <param name="positions">点</param>
        /// <returns>１つでも有効境界ボックス（OBB）内だと true / そうでなければ false</returns>
        public bool Contains(params Vector3[] positions)
        {
            foreach (var position in positions)
            {
                if (Contains(position))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// この有向境界ボックスが平面にどれだけ貫通しているかを取得
        /// </summary>
        /// <param name="position">平面上の点</param>
        /// <param name="normal">平面の法線</param>
        /// <param name="penetration">どれだけ貫通しているか</param>
        public void GetPenetration(ref Vector3 position, ref Vector3 normal, out Vector3 penetration)
        {
            float length = 0;
            float dot;
            for (int i = 0; i < 3; i++)
            {
                var vector = Axes[i] * HalfExtents[i];
                Vector3.Dot(ref vector, ref normal, out dot);
                length += Math.Abs(dot);
            }

            var distance = Center - position;
            Vector3.Dot(ref distance, ref normal, out dot);
            penetration = normal * (length - dot);
        }

        /// <summary>
        /// この有向境界ボックスが平面にどれだけ貫通しているかを取得
        /// </summary>
        /// <param name="position">平面上の点</param>
        /// <param name="normal">平面の法線</param>
        /// <returns>どれだけ貫通しているか</returns>
        public Vector3 GetPenetration(Vector3 position, Vector3 normal)
        {
            Vector3 penetration;
            GetPenetration(ref position, ref normal, out penetration);
            return penetration;
        }

        /// <summary>
        /// この有向境界ボックスが平面にどれだけ貫通しているかを取得
        /// </summary>
        /// <param name="plane">平面</param>
        /// <param name="penetration">どれだけ貫通しているか</param>
        public void GetPenetration(ref Plane plane, out Vector3 penetration)
        {
            var position = plane.Normal * plane.D;
            var normal = plane.Normal;
            GetPenetration(ref position, ref normal, out penetration);
        }

        /// <summary>
        /// この有向境界ボックスが平面にどれだけ貫通しているかを取得
        /// </summary>
        /// <param name="plane">平面</param>
        /// <returns>どれだけ貫通しているか</returns>
        public Vector3 GetPenetration(Plane plane)
        {
            Vector3 penetration;
            GetPenetration(ref plane, out penetration);
            return penetration;
        }

        #endregion

        #region Helpers

        /// <summary>
        /// 分離軸に投影された軸成分から投影線分長を算出
        /// </summary>
        /// <param name="separateAxis">分離軸</param>
        /// <param name="e1">軸成分１</param>
        /// <param name="e2">軸成分２</param>
        /// <param name="e3">軸成分３</param>
        /// <returns>投影した線分の長さ</returns>
        private float LengthSegmentOnSeparateAxis(ref Vector3 separateAxis, ref Vector3 e1, ref Vector3 e2, ref Vector3 e3)
        {
            return Math.Abs(Vector3.Dot(separateAxis, e1)) + Math.Abs(Vector3.Dot(separateAxis, e2)) + Math.Abs(Vector3.Dot(separateAxis, e3));
        }

        /// <summary>
        /// 分離軸に投影された軸成分から投影線分長を算出
        /// </summary>
        /// <param name="separateAxis">分離軸</param>
        /// <param name="e1">軸成分１</param>
        /// <param name="e2">軸成分２</param>
        /// <returns>投影した線分の長さ</returns>
        private float LengthSegmentOnSeparateAxis(ref Vector3 separateAxis, ref Vector3 e1, ref Vector3 e2)
        {
            return Math.Abs(Vector3.Dot(separateAxis, e1)) + Math.Abs(Vector3.Dot(separateAxis, e2));
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// ある位置からの最接近点を取得
        /// </summary>
        /// <param name="position">位置</param>
        /// <param name="point">最接近点</param>
        public void GetClosestPoint(ref Vector3 position, out Vector3 point)
        {
            var dist = position - Center;
            point = Center;

            for (int i = 0; i < 3; i++)
            {
                var axis = Axes[i];
                var extent = HalfExtents[i];
                point += axis * MathHelper.Clamp(Vector3.Dot(dist, axis), -extent, extent);
            }
        }

        /// <summary>
        /// ある位置からの最接近点を取得
        /// </summary>
        /// <param name="position">位置</param>
        /// <returns>最接近点</returns>
        public Vector3 GetClosestPoint(Vector3 position)
        {
            Vector3 point;
            GetClosestPoint(ref position, out point);
            return point;
        }

        /// <summary>
        /// 8 つの コーナーを取得
        /// </summary>
        /// <param name="corners"> 8 つの コーナーの座標</param>
        public void GetCorners(out Vector3[] corners)
        {
            Vector3 leftVec = Axes[0] * HalfExtent.X;
            Vector3 upVec = Axes[1] * HalfExtent.Y;
            Vector3 backVec = Axes[2] * HalfExtent.Z;

            int i = 0;
            corners = new Vector3[8];
            corners[i++] = Center - leftVec + upVec + backVec;
            corners[i++] = Center + leftVec + upVec + backVec;
            corners[i++] = Center + leftVec - upVec + backVec;
            corners[i++] = Center - leftVec - upVec + backVec;
            corners[i++] = Center - leftVec + upVec - backVec;
            corners[i++] = Center + leftVec + upVec - backVec;
            corners[i++] = Center + leftVec - upVec - backVec;
            corners[i++] = Center - leftVec - upVec - backVec;
        }

        /// <summary>
        /// 8 つの コーナーを取得
        /// </summary>
        /// <returns> 8 つの コーナーの座標</returns>
        public Vector3[] GetCorners()
        {
            Vector3[] corners;
            GetCorners(out corners);
            return corners;
        }

        /// <summary>
        /// 境界ボックスに変換
        /// </summary>
        /// <param name="box">境界ボックス</param>
        public void ToBoundingBox(out BoundingBox box)
        {
            box = BoundingBox.CreateFromPoints(GetCorners());
        }

        /// <summary>
        /// 境界ボックスに変換
        /// </summary>
        /// <returns>境界ボックス</returns>
        public BoundingBox ToBoundingBox()
        {
            BoundingBox box;
            ToBoundingBox(out box);
            return box;
        }

        /// <summary>
        /// 境界球に変換
        /// </summary>
        /// <param name="sphere">境界球</param>
        public void ToBoundingSphere(out BoundingSphere sphere)
        {
            sphere = BoundingSphere.CreateFromPoints(GetCorners());
        }

        /// <summary>
        /// 境界球に変換
        /// </summary>
        /// <returns>境界球</returns>
        public BoundingSphere ToBoundingSphere()
        {
            BoundingSphere sphere;
            ToBoundingSphere(out sphere);
            return sphere;
        }

        /// <summary>
        /// 移動
        /// </summary>
        /// <param name="vector">移動量</param>
        public void Move(ref Vector3 vector)
        {
            Center += vector;
        }

        /// <summary>
        /// 移動
        /// </summary>
        /// <param name="vector">移動量</param>
        public void Move(Vector3 vector)
        {
            Move(ref vector);
        }

        /// <summary>
        /// 位置を設定
        /// </summary>
        /// <param name="position">位置</param>
        public void SetPosition(ref Vector3 position)
        {
            Center = position;
        }

        /// <summary>
        /// 位置を設定
        /// </summary>
        /// <param name="position">位置</param>
        public void SetPosition(Vector3 position)
        {
            SetPosition(ref position);
        }

        /// <summary>
        /// 回転
        /// </summary>
        /// <param name="rotation">回転</param>
        public void Rotate(ref Quaternion rotation)
        {
            Quaternion newOrientation;
            var orientation = Orientation;
            Quaternion.Concatenate(ref orientation, ref rotation, out newOrientation);
            newOrientation.Normalize();
            Orientation = newOrientation;
            Matrix rotationMatrix;
            Matrix.CreateFromQuaternion(ref newOrientation, out rotationMatrix);
            axes = Array.AsReadOnly(new[] { rotationMatrix.Left, rotationMatrix.Up, rotationMatrix.Backward });
        }

        /// <summary>
        /// 回転
        /// </summary>
        /// <param name="rotation">回転</param>
        public void Rotate(Quaternion rotation)
        {
            Rotate(ref rotation);
        }

        /// <summary>
        /// 方向を設定
        /// </summary>
        /// <param name="orientation">回転行列</param>
        public void SetOrientation(ref Quaternion orientation)
        {
            Orientation = orientation;
            Matrix rotationMatrix;
            Matrix.CreateFromQuaternion(ref orientation, out rotationMatrix);
            axes = Array.AsReadOnly(new[] { rotationMatrix.Left, rotationMatrix.Up, rotationMatrix.Backward });
        }

        /// <summary>
        /// 方向を設定
        /// </summary>
        /// <param name="orientation">回転行列</param>
        public void SetOrientation(Quaternion orientation)
        {
            SetOrientation(ref orientation);
        }

        /// <summary>
        /// 大きさを設定
        /// </summary>
        /// <param name="size">大きさ</param>
        public void SetSize(ref Vector3 size)
        {
            HalfExtent = size * 0.5f;
            halfExtents = Array.AsReadOnly(new[] { HalfExtent.X, HalfExtent.Y, HalfExtent.Z });
        }

        /// <summary>
        /// 大きさを設定
        /// </summary>
        /// <param name="size">大きさ</param>
        public void SetSize(Vector3 size)
        {
            SetSize(ref size);
        }

        #endregion
    }

    /// <summary>
    /// 有効境界ボックスとの拡張メソッド
    /// </summary>
    public static class OrientedBoundingBoxExtensionMethods
    {
        #region OrientedBoundingBox

        /// <summary>
        /// 有向境界ボックス（OBB）と交差しているか
        /// </summary>
        /// <param name="boxes">有向境界ボックス</param>
        /// <param name="box">有向境界ボックス</param>
        /// <returns>交差しているかどうか</returns>
        public static bool Intersects(this OrientedBoundingBox[] boxes, OrientedBoundingBox box)
        {
            return box.Intersects(boxes);
        }

        /// <summary>
        /// 軸並行境界ボックス（AABB）と交差しているか
        /// </summary>
        /// <param name="boxes">有向境界ボックス</param>
        /// <param name="box">軸並行境界ボックス</param>
        /// <returns>交差しているかどうか</returns>
        public static bool Intersects(this OrientedBoundingBox[] boxes, BoundingBox box)
        {
            return box.Intersects(boxes);
        }

        /// <summary>
        /// 境界球 と交差しているか
        /// </summary>
        /// <param name="boxes">有向境界ボックス</param>
        /// <param name="sphere">境界球</param>
        /// <returns>交差しているかどうか</returns>
        public static bool Intersects(this OrientedBoundingBox[] boxes, BoundingSphere sphere)
        {
            return sphere.Intersects(boxes);
        }

        #endregion

        #region BoundingBox

        /// <summary>
        /// 有向境界ボックス（OBB）と交差しているか
        /// </summary>
        /// <param name="box">軸並行境界ボックス</param>
        /// <param name="other">有向境界ボックス</param>
        /// <returns>交差しているかどうか</returns>
        /// <returns></returns>
        public static bool Intersects(this BoundingBox box, OrientedBoundingBox other)
        {
            return other.Intersects(box);
        }

        /// <summary>
        /// 有向境界ボックス（OBB）と交差しているか
        /// </summary>
        /// <param name="box">軸並行境界ボックス</param>
        /// <param name="boxes">有向境界ボックス</param>
        /// <returns>交差しているかどうか</returns>
        /// <returns></returns>
        public static bool Intersects(this BoundingBox box, params OrientedBoundingBox[] boxes)
        {
            foreach (var other in boxes)
            {
                if (box.Intersects(other))
                {
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region BoundingSphere

        /// <summary>
        /// 有向境界ボックス（OBB）と交差しているか
        /// </summary>
        /// <param name="sphere">境界球</param>
        /// <param name="box">有向境界ボックス</param>
        /// <returns>交差しているかどうか</returns>
        public static bool Intersects(this BoundingSphere sphere, OrientedBoundingBox box)
        {
            return box.Intersects(sphere);
        }

        /// <summary>
        /// 有向境界ボックス（OBB）と交差しているか
        /// </summary>
        /// <param name="sphere">境界球</param>
        /// <param name="boxes">有向境界ボックス</param>
        /// <returns>交差しているかどうか</returns>
        public static bool Intersects(this BoundingSphere sphere, params OrientedBoundingBox[] boxes)
        {
            foreach (var other in boxes)
            {
                if (sphere.Intersects(other))
                {
                    return true;
                }
            }
            return false;
        }

        #endregion
    }
}

﻿#region File Description
//-----------------------------------------------------------------------------
// SoundManager.cs
// 
// Update: 2014/04/24
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;
#endregion

namespace XnaLibrary.Sound
{
    using Path = System.IO.Path;

    /// <summary>
    /// サウンドマネージャー
    /// </summary>
    public class SoundManager : IDisposable
    {
        #region Constants

        const string BgmIdentifier = "[BGM_CUE]";

        #endregion

        #region Fields

        /// <summary>
        /// オーディオエンジン
        /// </summary>
        AudioEngine audioEngine;

        /// <summary>
        /// サウンドバンク
        /// </summary>
        Dictionary<string, SoundBank> soundBanks;

        /// <summary>
        /// 最後に利用したサウンドバンク
        /// </summary>
        KeyValuePair<string, SoundBank> lastSoundBank;

        /// <summary>
        /// ウェイブバンク
        /// </summary>
        Dictionary<string, WaveBank> waveBanks;

        /// <summary>
        /// サウンドデータ
        /// </summary>
        Dictionary<string, SoundData> soundData;

        #endregion

        #region Properties

        /// <summary>
        /// 初期化済みかどうか
        /// </summary>
        public bool IsInitialized { get; private set; }

        /// <summary>
        /// リソースの解放済みかどうか
        /// </summary>
        public bool IsDisposed { get; private set; }

        #endregion

        #region Initialization

        /// <summary>
        /// コンストラクタ
        /// </summary>
        internal protected SoundManager()
        {
            IsInitialized = false;
            IsDisposed = false;

            soundBanks = new Dictionary<string, SoundBank>();
            waveBanks = new Dictionary<string, WaveBank>();

            soundData = new Dictionary<string, SoundData>();
        }

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="xgsPath">xgs ファイルのパス</param>
        public void Initialize(string xgsPath)
        {
            audioEngine = new AudioEngine(xgsPath);

            IsInitialized = true;
        }

        /// <summary>
        /// サウンドバンクを追加
        /// </summary>
        /// <param name="xsbPath">xsb ファイル（Sound Bank）のパス</param>
        public void AddSoundBank(string xsbPath)
        {
            if (!IsInitialized)
            {
                throw new InvalidOperationException("初期化されていません");
            }

            lastSoundBank = new KeyValuePair<string, SoundBank>(Path.GetFileNameWithoutExtension(xsbPath), new SoundBank(audioEngine, xsbPath));
            soundBanks.Add(lastSoundBank.Key, lastSoundBank.Value);
        }

        /// <summary>
        /// ウェイブバンクを追加
        /// </summary>
        /// <param name="xwbPath">xwb ファイル（Wave Bank）のパス</param>
        public void AddWaveBank(string xwbPath)
        {
            if (!IsInitialized)
            {
                throw new InvalidOperationException("初期化されていません");
            }

            waveBanks.Add(Path.GetFileNameWithoutExtension(xwbPath), new WaveBank(audioEngine, xwbPath));
        }

        /// <summary>
        /// デストラクタ
        /// </summary>
        ~SoundManager()
        {
            Dispose(false);
        }

        /// <summary>
        /// 使用されている リソースの開放を行う
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 使用されている リソースの開放を行う
        /// </summary>
        /// <param name="disposing">
        /// アンマネージ リソース と マネージ リソースの両方開放するなら true
        /// アンマネージ リソースだけを解放する場合は false
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {
                    StopSoundAll();

                    if (audioEngine != null)
                    {
                        audioEngine.Dispose();
                        audioEngine = null;
                    }

                    if (soundBanks != null)
                    {
                        foreach (var bank in soundBanks.Values)
                        {
                            bank.Dispose();
                        }
                        soundBanks.Clear();
                        soundBanks = null;
                    }

                    if (waveBanks != null)
                    {
                        foreach (var bank in waveBanks.Values)
                        {
                            bank.Dispose();
                        }
                        waveBanks.Clear();
                        waveBanks = null;
                    }
                }
                IsDisposed = true;
            }
        }

        #endregion

        #region Public Methods

        #region Update

        /// <summary>
        /// 更新
        /// </summary>
        public void Update()
        {
            if (!IsInitialized)
            {
                return;
            }

            audioEngine.Update();

            var removeKeys = new List<string>();
            var replaySounds = new Dictionary<string, SoundData>();
            foreach (var item in soundData)
            {
                var data = item.Value;
                if (data == null)
                {
                    removeKeys.Add(item.Key);
                    continue;
                }

                var cue = data.Cue;
                if (cue.IsStopped)
                {
                    if (data.IsLoop)
                    {
                        replaySounds.Add(item.Key, new SoundData(data.SoundBank, cue.Name, true, data.Listener, data.Emmiter));
                    }

                    removeKeys.Add(item.Key);
                    data.Dispose();
                }
                else
                {
                    data.Apply3D();
                }
            }

            foreach (var key in removeKeys)
            {
                soundData.Remove(key);
            }

            foreach (var item in replaySounds)
            {
                soundData.Add(item.Key, item.Value);
            }
        }

        #endregion

        #region Play Methods

        /// <summary>
        /// サウンドを再生
        /// </summary>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="cueName">サウンド名</param>
        public void PlaySound(string bankName, string cueName)
        {
            PlaySound(bankName, cueName, null, null, false, null);
        }

        /// <summary>
        /// サウンドを再生
        /// </summary>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="cueName">サウンド名</param>
        /// <param name="isLoop">ループ再生するかどうか</param>
        public void PlaySound(string bankName, string cueName, bool isLoop)
        {
            PlaySound(bankName, cueName, null, null, isLoop, null);
        }

        /// <summary>
        /// サウンドを再生
        /// </summary>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="cueName">サウンド名</param>
        /// <param name="isLoop">ループ再生するかどうか</param>
        /// <param name="identifier">識別子</param>
        public void PlaySound(string bankName, string cueName, bool isLoop, string identifier)
        {
            PlaySound(bankName, cueName, null, null, isLoop, identifier);
        }

        /// <summary>
        /// サウンドを再生
        /// </summary>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="cueName">サウンド名</param>
        /// <param name="listener">リスナー</param>
        /// <param name="emitter">エミッター</param>
        public void PlaySound(string bankName, string cueName, AudioListener listener, AudioEmitter emitter)
        {
            PlaySound(bankName, cueName, listener, emitter, false, null);
        }

        /// <summary>
        /// サウンドを再生
        /// </summary>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="cueName">サウンド名</param>
        /// <param name="listener">リスナー</param>
        /// <param name="emitter">エミッター</param>
        /// <param name="isLoop">ループ再生するかどうか</param>
        public void PlaySound(string bankName, string cueName, AudioListener listener, AudioEmitter emitter, bool isLoop)
        {
            PlaySound(bankName, cueName, listener, emitter, isLoop, null);
        }

        /// <summary>
        /// サウンドを再生
        /// </summary>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="cueName">サウンド名</param>
        /// <param name="listener">リスナー</param>
        /// <param name="emitter">エミッター</param>
        /// <param name="isLoop">ループ再生するかどうか</param>
        /// <param name="identifier">識別子</param>
        public void PlaySound(string bankName, string cueName, AudioListener listener, AudioEmitter emitter, bool isLoop, string identifier)
        {
            var key = CreateKey(bankName, cueName, identifier);
            if (soundData.ContainsKey(key))
            {
                return;
            }

            if (bankName != lastSoundBank.Key)
            {
                lastSoundBank = new KeyValuePair<string, SoundBank>(bankName, soundBanks[bankName]);
            }

            soundData.Add(key, new SoundData(lastSoundBank.Value, cueName, isLoop, listener, emitter));
        }

        /// <summary>
        /// BGM を再生
        /// </summary>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="bgmName">BGM の名前</param>
        public void PlayBgm(string bankName, string bgmName)
        {
            PlayBgm(bankName, bgmName, true);
        }

        /// <summary>
        /// BGM を再生
        /// </summary>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="bgmName">BGM の名前</param>
        /// <param name="isLoop">ループするか</param>
        public void PlayBgm(string bankName, string bgmName, bool isLoop)
        {
            SoundData data;
            if (soundData.TryGetValue(BgmIdentifier, out data))
            {
                if (data != null)
                {
                    if (data.Cue.Name == bgmName)
                    {
                        return;
                    }

                    data.Dispose();
                }
            }

            soundData[BgmIdentifier] = new SoundData(soundBanks[bankName], bgmName, isLoop);
        }

        /// <summary>
        /// BGM をはじめから再生しなおす
        /// </summary>
        public void RestartBgm()
        {
            SoundData data;
            if (soundData.TryGetValue(BgmIdentifier, out data))
            {
                if (data != null)
                {
                    soundData[BgmIdentifier] = new SoundData(data.SoundBank, data.Cue.Name, data.IsLoop);
                    data.Dispose();
                }
            }
        }

        #endregion

        #region Stop Methods

        /// <summary>
        /// サウンドを停止
        /// </summary>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="cueName">サウンド名</param>
        public void StopSound(string bankName, string cueName)
        {
            StopSound(bankName, cueName, null);
        }

        /// <summary>
        /// サウンドを停止
        /// </summary>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="cueName">サウンド名</param>
        /// <param name="identifier">識別子</param>
        public void StopSound(string bankName, string cueName, string identifier)
        {
            var key = CreateKey(bankName, cueName, identifier);
            SoundData data;
            if (soundData.TryGetValue(key, out data))
            {
                if (data != null)
                {
                    data.Dispose();
                }
                soundData.Remove(key);
            }
        }

        /// <summary>
        /// 全てのサウンドを停止
        /// </summary>
        public void StopSoundAll()
        {
            foreach (var data in soundData.Values)
            {
                if (data != null)
                {
                    data.Dispose();
                }
            }

            soundData.Clear();
        }

        /// <summary>
        /// BGM を停止
        /// </summary>
        public void StopBgm()
        {
            SoundData data;
            if (!soundData.TryGetValue(BgmIdentifier, out data))
            {
                return;
            }

            if (data != null)
            {
                data.Dispose();
            }
            soundData.Remove(BgmIdentifier);
        }

        /// <summary>
        /// BGM を除く全てのサウンドを停止
        /// </summary>
        public void StopSoundWithoutBgm()
        {
            SoundData bgm = null;
            foreach (var item in soundData)
            {
                if (item.Key == BgmIdentifier)
                {
                    bgm = item.Value;
                    continue;
                }

                if (item.Value != null)
                {
                    item.Value.Dispose();
                }
            }

            soundData.Clear();

            if (bgm != null)
            {
                soundData.Add(BgmIdentifier, bgm);
            }
        }

        #endregion

        #region Pause Methods

        /// <summary>
        /// サウンドを一時停止
        /// </summary>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="cueName">サウンド名</param>
        public void PauseSound(string bankName, string cueName)
        {
            PauseSound(bankName, cueName, null);
        }

        /// <summary>
        /// サウンドを一時停止
        /// </summary>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="cueName">サウンド名</param>
        /// <param name="identifier">識別子</param>
        public void PauseSound(string bankName, string cueName, string identifier)
        {
            var key = CreateKey(bankName, cueName, identifier);

            SoundData data;
            if (soundData.TryGetValue(key, out data))
            {
                if (data != null)
                {
                    if (data.Cue.IsPlaying && !data.Cue.IsPaused)
                    {
                        data.Cue.Pause();
                    }
                }
            }
        }

        /// <summary>
        /// 全てのサウンドを一時停止
        /// </summary>
        public void PauseSoundAll()
        {
            foreach (var data in soundData.Values)
            {
                if (data != null)
                {
                    if (data.Cue.IsPlaying && !data.Cue.IsPaused)
                    {
                        data.Cue.Pause();
                    }
                }
            }
        }

        /// <summary>
        /// BGM を一時停止
        /// </summary>
        public void PauseBgm()
        {
            SoundData data;
            if (soundData.TryGetValue(BgmIdentifier, out data))
            {
                if (data != null)
                {
                    if (data.Cue.IsPlaying && !data.Cue.IsPaused)
                    {
                        data.Cue.Pause();
                    }
                }
            }
        }

        /// <summary>
        /// BGM を除く全てのサウンドを一時停止
        /// </summary>
        public void PauseWithoutBgm()
        {
            foreach (var item in soundData)
            {
                if (item.Key == BgmIdentifier)
                {
                    continue;
                }

                var data = item.Value;
                if (data != null)
                {
                    if (data.Cue.IsPlaying && !data.Cue.IsPaused)
                    {
                        data.Cue.Pause();
                    }
                }
            }
        }

        #endregion

        #region Resume Methods

        /// <summary>
        /// サウンドを再開
        /// </summary>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="cueName">サウンド名</param>
        public void ResumeSound(string bankName, string cueName)
        {
            ResumeSound(bankName, cueName, null);
        }

        /// <summary>
        /// サウンドを再開
        /// </summary>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="cueName">サウンド名</param>
        /// <param name="identifier">識別子</param>
        public void ResumeSound(string bankName, string cueName, string identifier)
        {
            var key = CreateKey(bankName, cueName, identifier);

            SoundData data;
            if (soundData.TryGetValue(key, out data))
            {
                if (data != null)
                {
                    if (data.Cue.IsPaused)
                    {
                        data.Cue.Resume();
                    }
                }
            }
        }

        /// <summary>
        /// 全てのサウンドを再開
        /// </summary>
        public void ResumeSoundAll()
        {
            foreach (var data in soundData.Values)
            {
                if (data != null)
                {
                    if (data.Cue.IsPaused)
                    {
                        data.Cue.Resume();
                    }
                }
            }
        }

        /// <summary>
        /// BGM を再開
        /// </summary>
        public void ResumeBgm()
        {
            SoundData data;
            if (soundData.TryGetValue(BgmIdentifier, out data))
            {
                if (data != null)
                {
                    if (data.Cue.IsPaused)
                    {
                        data.Cue.Resume();
                    }
                }
            }
        }

        /// <summary>
        /// BGM を除く全てのサウンドを再開
        /// </summary>
        public void ResumeWithoutBgm()
        {
            foreach (var item in soundData)
            {
                if (item.Key == BgmIdentifier)
                {
                    continue;
                }

                var data = item.Value;
                if (data != null)
                {
                    if (data.Cue.IsPaused)
                    {
                        data.Cue.Resume();
                    }
                }
            }
        }

        #endregion

        #region Common Methods

        /// <summary>
        /// 音量の設定
        /// </summary>
        /// <param name="categoryName">カテゴリーの名前</param>
        /// <param name="volume">音量（1.0 が最高音量、0.0 で無音（-96 dB）、2.0 で最高音量から +6 dB）</param>
        public void SetVolume(string categoryName, float volume)
        {
            audioEngine.GetCategory(categoryName).SetVolume(volume);
        }

        /// <summary>
        /// グローバル変数の取得
        /// </summary>
        /// <param name="name">変数名</param>
        /// <returns>変数の値</returns>
        public float GetGlobalVariable(string name)
        {
            return audioEngine.GetGlobalVariable(name);
        }

        /// <summary>
        /// グローバル変数の設定
        /// </summary>
        /// <param name="name">変数名</param>
        /// <param name="value">設定する値</param>
        public void SetGlobalVariable(string name, float value)
        {
            audioEngine.SetGlobalVariable(name, value);
        }

        /// <summary>
        /// キューの変数を取得
        /// </summary>
        /// <param name="variableName">変数名</param>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="cueName">サウンド名</param>
        /// <returns>変数の値</returns>
        public float GetCueVariable(string variableName, string bankName, string cueName)
        {
            return GetCueVariable(variableName, bankName, cueName, null);
        }

        /// <summary>
        /// キューの変数を取得
        /// </summary>
        /// <param name="variableName">変数名</param>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="cueName">サウンド名</param>
        /// <param name="identifier">識別子</param>
        /// <returns>変数の値</returns>
        public float GetCueVariable(string variableName, string bankName, string cueName, string identifier)
        {
            return GetCueVariable(CreateKey(bankName, cueName, identifier), variableName);
        }

        /// <summary>
        /// BGM キューの変数を取得
        /// </summary>
        /// <param name="variableName">変数名</param>
        /// <returns>変数の値</returns>
        public float GetBgmCueVariable(string variableName)
        {
            return GetCueVariable(BgmIdentifier, variableName);
        }

        /// <summary>
        /// キューの変数を設定
        /// </summary>
        /// <param name="variableName">変数名</param>
        /// <param name="value">設定する値</param>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="cueName">サウンド名</param>
        public void SetCueVariable(string variableName, float value, string bankName, string cueName)
        {
            SetCueVariable(variableName, value, bankName, cueName, null);
        }

        /// <summary>
        /// キューの変数を設定
        /// </summary>
        /// <param name="variableName">変数名</param>
        /// <param name="value">設定する値</param>
        /// <param name="bankName">サウンドバンク名</param>
        /// <param name="cueName">サウンド名</param>
        /// <param name="identifier">識別子</param>
        public void SetCueVariable(string variableName, float value, string bankName, string cueName, string identifier)
        {
            SetCueVariable(CreateKey(bankName, cueName, identifier), variableName, value);
        }

        /// <summary>
        /// BGM キューの変数を設定
        /// </summary>
        /// <param name="variableName">変数名</param>
        /// <param name="value">設定する値</param>
        public void SetBgmCueVariable(string variableName, float value)
        {
            SetCueVariable(BgmIdentifier, variableName, value);
        }

        #endregion

        #endregion

        #region Helpers

        private string CreateKey(string bankName, string cueName, string identifier)
        {
            return cueName + "_" + bankName + "_" + (identifier ?? cueName.GetHashCode().ToString());
        }

        private float GetCueVariable(string key, string variableName)
        {
            return soundData[key].Cue.GetVariable(variableName);
        }

        private void SetCueVariable(string key, string variableName, float value)
        {
            soundData[key].Cue.SetVariable(variableName, value);
        }

        #endregion
    }
}

﻿#region File Description
//-----------------------------------------------------------------------------
// SoundData.cs
// 
// Update: 2014/04/24
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework.Audio;
#endregion

namespace XnaLibrary.Sound
{
    /// <summary>
    /// サウンドデータ
    /// </summary>
    class SoundData : IDisposable
    {
        /// <summary>
        /// キュー
        /// </summary>
        public Cue Cue { get; private set; }

        /// <summary>
        /// サウンドバンク
        /// </summary>
        public SoundBank SoundBank { get; private set; }

        /// <summary>
        /// リスナー
        /// </summary>
        public AudioListener Listener { get; private set; }

        /// <summary>
        /// エミッター
        /// </summary>
        public AudioEmitter Emmiter { get; private set; }

        /// <summary>
        /// ループ再生するかどうか
        /// </summary>
        public bool IsLoop { get; private set; }

        /// <summary>
        /// 3D 効果を利用するサウンドかどうか
        /// </summary>
        public bool Is3DSound { get; private set; }

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="soundBank">キューがあるサウンドバンク</param>
        /// <param name="name">サウンド名</param>
        public SoundData(SoundBank soundBank, string name)
            : this(soundBank, name, false, null, null)
        {
        }

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="soundBank">キューがあるサウンドバンク</param>
        /// <param name="name">サウンド名</param>
        /// <param name="isLoop">ループ再生するかどうか</param>
        public SoundData(SoundBank soundBank, string name, bool isLoop)
            : this(soundBank, name, isLoop, null, null)
        {
        }

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="soundBank">キューがあるサウンドバンク</param>
        /// <param name="name">サウンド名</param>
        /// <param name="isLoop">ループ再生するかどうか</param>
        /// <param name="listener">リスナー</param>
        /// <param name="emmiter">エミッター</param>
        public SoundData(SoundBank soundBank, string name, bool isLoop, AudioListener listener, AudioEmitter emmiter)
        {
            SoundBank = soundBank;
            Cue = SoundBank.GetCue(name);
            IsLoop = isLoop;
            Listener = listener;
            Emmiter = emmiter;
            Is3DSound = Listener != null && Emmiter != null;
            Apply3D();
            Cue.Play();
        }

        /// <summary>
        /// 使用されている リソースの開放を行う
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 使用されている リソースの開放を行う
        /// </summary>
        /// <param name="disposing">
        /// アンマネージ リソース と マネージ リソースの両方開放するなら true
        /// アンマネージ リソースだけを解放する場合は false
        /// </param>
        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (Cue != null)
                {
                    if (Cue.IsPlaying)
                    {
                        Cue.Stop(AudioStopOptions.Immediate);
                    }
                    Cue.Dispose();
                    Cue = null;
                }
                SoundBank = null;
                Listener = null;
                Emmiter = null;
            }
        }

        /// <summary>
        /// 3D 効果を適用する
        /// </summary>
        public void Apply3D()
        {
            if (!Is3DSound)
            {
                return;
            }

            Cue.Apply3D(Listener, Emmiter);
        }
    }
}

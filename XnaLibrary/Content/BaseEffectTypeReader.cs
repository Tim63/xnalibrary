﻿#region File Description
//-----------------------------------------------------------------------------
// BaseEffectTypeReader.cs
// 
// Update: 2014/04/01
//
// Reference: Microsoft.Xna.Framework.Graphics.dll
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace XnaLibrary.Content
{
    using Effect = Microsoft.Xna.Framework.Graphics.Effect;

    internal abstract class BaseEffectTypeReader<T> : ContentTypeReader<T>
        where T : Effect
    {
        const string BuiltInEffectWrongTextureType = "{0} の読み込み中にエラーが発生しました。ファイルは {1} を参照していますが {2} は {3} を必要としています。";

        static Dictionary<GraphicsDevice, T> sharedEffects = new Dictionary<GraphicsDevice, T>();

        protected static T GetClonedEffect(ContentReader reader, Converter<GraphicsDevice, T> createEffect)
        {
            var graphicsDevice = GraphicsContentHelper.FromContentReader(reader);
            T value;
            if (!BaseEffectTypeReader<T>.sharedEffects.TryGetValue(graphicsDevice, out value))
            {
                value = createEffect(graphicsDevice);
                BaseEffectTypeReader<T>.sharedEffects.Add(graphicsDevice, value);
                graphicsDevice.Disposing += new EventHandler<EventArgs>(BaseEffectTypeReader<T>.RemoveDevice);
            }
            return value.Clone() as T;
        }

        private static void RemoveDevice(object sender, EventArgs e)
        {
            var key = sender as GraphicsDevice;
            T t = BaseEffectTypeReader<T>.sharedEffects[key];
            t.Dispose();
            BaseEffectTypeReader<T>.sharedEffects.Remove(key);
        }

        protected static TextureType ReadTexture<TextureType>(ContentReader reader) where TextureType : Texture
        {
            var texture = reader.ReadExternalReference<Texture>();
            if (texture == null)
            {
                return default(TextureType);
            }

            var textureType = texture as TextureType;
            if (textureType == null)
            {
                throw ContentLoadExceptionHelper.Create(reader.AssetName, null, BuiltInEffectWrongTextureType, new object[]
                {
                    texture.GetType().Name,
                    typeof(T).Name,
                    typeof(TextureType).Name
                });
            }

            return textureType;
        }
    }
}

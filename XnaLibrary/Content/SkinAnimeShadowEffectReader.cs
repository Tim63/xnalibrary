﻿#region File Description
//-----------------------------------------------------------------------------
// SkinAnimeShadowEffectReader.cs
// 
// Update: 2014/04/24
//
// Reference: Microsoft.Xna.Framework.Graphics.dll
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using XnaLibrary.Graphics.Effect;
#endregion

namespace XnaLibrary.Content
{
    internal class SkinAnimeShadowEffectReader : BaseEffectTypeReader<SkinAnimeShadowEffect>
    {
        protected override SkinAnimeShadowEffect Read(ContentReader input, SkinAnimeShadowEffect existingInstance)
        {
            var effect = BaseEffectTypeReader<SkinAnimeShadowEffect>.GetClonedEffect(input, (device) => new SkinAnimeShadowEffect(device));
            effect.Texture = BaseEffectTypeReader<SkinAnimeShadowEffect>.ReadTexture<Texture2D>(input);
            effect.DiffuseColor = input.ReadVector3();
            effect.EmissiveColor = input.ReadVector3();
            effect.SpecularColor = input.ReadVector3();
            effect.SpecularPower = input.ReadSingle();
            effect.Alpha = input.ReadSingle();
            return effect;
        }
    }
}

﻿#region File Description
//-----------------------------------------------------------------------------
// TypeReaderHelpers.cs
// 
// Update: 2014/04/01
//
// Reference: Microsoft.Xna.Framework.Graphics.dll
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Globalization;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace XnaLibrary.Content
{
    internal class ContentLoadExceptionHelper
    {
        internal static ContentLoadException Create(string assetName, Exception innerException, string format)
        {
            var message = string.Format(CultureInfo.CurrentCulture, format, assetName);
            return new ContentLoadException(message, innerException);
        }

        internal static ContentLoadException Create(string assetName, Exception innerException, string format, params object[] messageArgs)
        {
            var array = new object[messageArgs.Length + 1];
            array[0] = assetName;
            messageArgs.CopyTo(array, 1);
            var message = string.Format(CultureInfo.CurrentCulture, format, array);
            return new ContentLoadException(message, innerException);
        }
    }

    internal class GraphicsContentHelper
    {
        //static FieldInfo readerProfileField = typeof(ContentReader).GetField("graphicsProfile", BindingFlags.Instance | BindingFlags.NonPublic);

        //const string BadXnbGraphicsProfile = "{0} の読み込み中にエラーが発生しました。このファイルは {1} プロファイル用にコンパイルされたもので、{2} GraphicsDevice には読み込めません。";
        const string NoGraphicsDeviceContent = "{0} の読み込み中にエラーが発生しました。GraphicsDevice コンポーネントが見つかりませんでした。";

        internal static GraphicsDevice FromContentReader(ContentReader contentReader)
        {
            var graphicsDeviceService = contentReader.ContentManager.ServiceProvider.GetService(typeof(IGraphicsDeviceService)) as IGraphicsDeviceService;
            if (graphicsDeviceService == null)
            {
                throw ContentLoadExceptionHelper.Create(contentReader.AssetName, null, NoGraphicsDeviceContent);
            }

            var graphicsDevice = graphicsDeviceService.GraphicsDevice;
            if (graphicsDevice == null)
            {
                throw ContentLoadExceptionHelper.Create(contentReader.AssetName, null, NoGraphicsDeviceContent);
            }

            /*var graphicsProfile = graphicsDevice.GraphicsProfile;

            var contentProfile = (GraphicsProfile)readerProfileField.GetValue(contentReader);
            if (!IsProfileCompatible(graphicsProfile, contentProfile))
            {
                throw ContentLoadExceptionHelper.Create(contentReader.AssetName, null, BadXnbGraphicsProfile, new object[] { contentProfile, graphicsProfile });
            }*/

            return graphicsDevice;
        }

        static bool IsProfileCompatible(GraphicsProfile deviceProfile, GraphicsProfile contentProfile)
        {
            switch (deviceProfile)
            {
                case GraphicsProfile.Reach:
                    return contentProfile == GraphicsProfile.Reach;

                case GraphicsProfile.HiDef:
                    return contentProfile == GraphicsProfile.Reach || contentProfile == GraphicsProfile.HiDef;

                default:
                    return false;
            }
        }
    }
}

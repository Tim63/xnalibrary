﻿#region File Description
//-----------------------------------------------------------------------------
// AnimationClipReader.cs
//
// Update: 2014/04/08
//
// Orignal File:
//     TypeReaders.cs
//     Microsoft XNA Community Game Platform
//     Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using XnaLibrary.Graphics.Model;
#endregion

namespace XnaLibrary.Content
{
    /// <summary>
    /// AnimationClip の読み込み
    /// </summary>
    public class AnimationClipReader : ContentTypeReader<AnimationClip>
    {
        /// <summary>
        /// 読み込み
        /// </summary>
        /// <param name="input">コンテントリーダー</param>
        /// <param name="existingInstance">既存のオブジェクト</param>
        /// <returns>読み込んだ AnimationClip</returns>
        protected override AnimationClip Read(ContentReader input, AnimationClip existingInstance)
        {
            var name = input.ReadObject<string>();
            var duration = input.ReadObject<TimeSpan>();
            var keyframes = input.ReadObject<IList<Keyframe>>();

            return new AnimationClip(name, duration, keyframes);
        }
    }
}

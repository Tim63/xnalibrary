﻿#region File Description
//-----------------------------------------------------------------------------
// ShadowEffectReader.cs
// 
// Update: 2014/04/24
//
// Reference: Microsoft.Xna.Framework.Graphics.dll
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using XnaLibrary.Graphics.Effect;
#endregion

namespace XnaLibrary.Content
{
    internal class ShadowEffectReader : BaseEffectTypeReader<ShadowEffect>
    {
        protected override ShadowEffect Read(ContentReader input, ShadowEffect existingInstance)
        {
            var effect = BaseEffectTypeReader<ShadowEffect>.GetClonedEffect(input, (device) => new ShadowEffect(device));
            effect.Texture = BaseEffectTypeReader<ShadowEffect>.ReadTexture<Texture2D>(input);
            effect.DiffuseColor = input.ReadVector3();
            effect.EmissiveColor = input.ReadVector3();
            effect.SpecularColor = input.ReadVector3();
            effect.SpecularPower = input.ReadSingle();
            effect.Alpha = input.ReadSingle();
            return effect;
        }
    }
}

﻿#region File Description
//-----------------------------------------------------------------------------
// SkinningDataReader.cs
//
// Update: 2014/01/08
//
// Orignal File:
//     TypeReaders.cs
//     Microsoft XNA Community Game Platform
//     Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using XnaLibrary.Graphics.Model;
#endregion

namespace XnaLibrary.Content
{
    /// <summary>
    /// SkinningData の読み込み
    /// </summary>
    public class SkinningDataReader : ContentTypeReader<SkinningData>
    {
        /// <summary>
        /// 読み込み
        /// </summary>
        /// <param name="input">コンテントリーダー</param>
        /// <param name="existingInstance">既存のオブジェクト</param>
        /// <returns>読み込んだ SkinningData</returns>
        protected override SkinningData Read(ContentReader input, SkinningData existingInstance)
        {
            var animationClips = input.ReadObject<IDictionary<string, AnimationClip>>();
            var bindPose = input.ReadObject<IList<Matrix>>();
            var inverseBindPose = input.ReadObject<IList<Matrix>>();
            var skeletonHierarchy = input.ReadObject<IList<int>>();
            var boneIndices = input.ReadObject<IDictionary<string, int>>();

            return new SkinningData(animationClips, bindPose, inverseBindPose, skeletonHierarchy, boneIndices);
        }
    }
}

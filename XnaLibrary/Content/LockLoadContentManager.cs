﻿#region File Description
//-----------------------------------------------------------------------------
// LockLoadContentManager.cs
//
// Update: 2013/12/22
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework.Content;
#endregion

namespace XnaLibrary.Content
{
    /// <summary>
    /// 読み込み時にロックされるコンテントマネージャー
    /// </summary>
    public class LockLoadContentManager : ContentManager
    {
        /// <summary>
        /// ロック用オブジェクト
        /// </summary>
        public static object LoadLockObject = new object();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="serviceProvider">サービスプロバイダー</param>
        public LockLoadContentManager(IServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="serviceProvider">サービスプロバイダー</param>
        /// <param name="rootDirectory">ルートディレクトリ</param>
        public LockLoadContentManager(IServiceProvider serviceProvider, string rootDirectory)
            : base(serviceProvider, rootDirectory)
        {
        }

        /// <summary>
        /// アセットを読み込む
        /// </summary>
        /// <typeparam name="T">アセットの型</typeparam>
        /// <param name="assetName">アセット名</param>
        /// <returns>読み込んだアセット</returns>
        public override T Load<T>(string assetName)
        {
            lock (LoadLockObject)
            {
                return base.Load<T>(assetName);
            }
        }

        /// <summary>
        /// ロックせずにアセットを読み込む
        /// </summary>
        /// <typeparam name="T">アセットの型</typeparam>
        /// <param name="assetName">アセット名</param>
        /// <returns>読み込んだアセット</returns>
        public T LoadWithoutLock<T>(string assetName)
        {
            return base.Load<T>(assetName);
        }
    }
}

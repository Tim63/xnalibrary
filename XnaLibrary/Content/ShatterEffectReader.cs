﻿#region File Description
//-----------------------------------------------------------------------------
// ShatterEffectReader.cs
// 
// Update: 2014/04/24
//
// Reference: Microsoft.Xna.Framework.Graphics.dll
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using XnaLibrary.Graphics.Effect;
#endregion

namespace XnaLibrary.Content
{
    internal class ShatterEffectReader : BaseEffectTypeReader<ShatterEffect>
    {
        protected override ShatterEffect Read(ContentReader input, ShatterEffect existingInstance)
        {
            var effect = BaseEffectTypeReader<ShatterEffect>.GetClonedEffect(input, (device) => new ShatterEffect(device));
            effect.Texture = BaseEffectTypeReader<ShatterEffect>.ReadTexture<Texture2D>(input);
            effect.DiffuseColor = input.ReadVector3();
            effect.EmissiveColor = input.ReadVector3();
            effect.SpecularColor = input.ReadVector3();
            effect.SpecularPower = input.ReadSingle();
            effect.Alpha = input.ReadSingle();
            return effect;
        }
    }
}

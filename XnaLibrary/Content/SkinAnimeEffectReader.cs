﻿#region File Description
//-----------------------------------------------------------------------------
// SkinAnimeEffectReader.cs
// 
// Update: 2014/04/22
//
// Reference: Microsoft.Xna.Framework.Graphics.dll
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using XnaLibrary.Graphics.Effect;
#endregion

namespace XnaLibrary.Content
{
    internal class SkinAnimeEffectReader : BaseEffectTypeReader<SkinAnimeEffect>
    {
        protected override SkinAnimeEffect Read(ContentReader input, SkinAnimeEffect existingInstance)
        {
            var effect = BaseEffectTypeReader<SkinAnimeEffect>.GetClonedEffect(input, (device) => new SkinAnimeEffect(device));
            effect.Texture = BaseEffectTypeReader<SkinAnimeEffect>.ReadTexture<Texture2D>(input);
            effect.DiffuseColor = input.ReadVector3();
            effect.EmissiveColor = input.ReadVector3();
            effect.SpecularColor = input.ReadVector3();
            effect.SpecularPower = input.ReadSingle();
            effect.Alpha = input.ReadSingle();
            return effect;
        }
    }
}

﻿#region File Description
//-----------------------------------------------------------------------------
// KeyframeReader.cs
//
// Update: 2014/04/08
//
// Orignal File:
//     TypeReaders.cs
//     Microsoft XNA Community Game Platform
//     Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using XnaLibrary.Graphics.Model;
#endregion

namespace XnaLibrary.Content
{
    /// <summary>
    /// Keyframe の読み込み
    /// </summary>
    public class KeyframeReader : ContentTypeReader<Keyframe>
    {
        /// <summary>
        /// 読み込み
        /// </summary>
        /// <param name="input">コンテントリーダー</param>
        /// <param name="existingInstance">既存のオブジェクト</param>
        /// <returns>読み込んだ Keyframe</returns>
        protected override Keyframe Read(ContentReader input, Keyframe existingInstance)
        {
            var bone = input.ReadObject<int>();
            var time = input.ReadObject<TimeSpan>();
            var transform = input.ReadObject<Matrix>();

            return new Keyframe(bone, time, transform);
        }
    }
}

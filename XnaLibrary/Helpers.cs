﻿#region File Description
//-----------------------------------------------------------------------------
// Helpers.cs
// 
// Update: 2014/04/24
// References: https://github.com/willcraftia/TestXna/
//             http://xboxforums.create.msdn.com/forums/p/4574/23763.aspx
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
#endregion

namespace XnaLibrary
{
    /// <summary>
    /// 乱数クラスの単一化用ヘルパークラス
    /// </summary>
    public static class RandomHelper
    {
        static Random random = null;
        static bool isInitialized = false;

        /// <summary>
        /// 初期化
        /// </summary>
        public static void Initialize()
        {
            if (!isInitialized)
            {
                random = new Random();
            }
        }

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="seed">シード</param>
        public static void Initialize(int seed)
        {
            if (!isInitialized)
            {
                random = new Random(seed);
            }
        }

        /// <summary>
        /// 0 以上 System.Int32.MaxValue 未満の範囲で乱数を生成
        /// </summary>
        /// <returns>生成された乱数</returns>
        public static int Next()
        {
            if (!isInitialized)
            {
                throw new InvalidOperationException("初期化されていません");
            }
            return random.Next();
        }

        /// <summary>
        /// 0 以上 maxValue 未満の範囲で乱数を生成
        /// </summary>
        /// <param name="maxValue">乱数の上限値</param>
        /// <returns>生成された乱数</returns>
        public static int Next(int maxValue)
        {
            if (!isInitialized)
            {
                throw new InvalidOperationException("初期化されていません");
            }
            return random.Next(maxValue);
        }

        /// <summary>
        /// minValue 以上 maxValue 未満の範囲で乱数を生成
        /// </summary>
        /// <param name="minValue">乱数の下限値</param>
        /// <param name="maxValue">乱数の上限値</param>
        /// <returns>生成された乱数</returns>
        public static int Next(int minValue, int maxValue)
        {
            if (!isInitialized)
            {
                throw new InvalidOperationException("初期化されていません");
            }
            return random.Next(minValue, maxValue);
        }

        /// <summary>
        /// 0.0 以上 1.0 未満 の範囲で乱数を生成
        /// </summary>
        /// <returns>生成された乱数</returns>
        public static float NextFloat()
        {
            if (!isInitialized)
            {
                throw new InvalidOperationException("初期化されていません");
            }
            return (float)random.NextDouble();
        }
    }

    /// <summary>
    /// Vector2 に対するヘルパークラス
    /// </summary>
    public static class Vector2Helpers
    {
        /// <summary>
        /// 外積を計算
        /// </summary>
        /// <param name="vec1">ベクトル</param>
        /// <param name="vec2">ベクトル</param>
        /// <param name="cross">外積</param>
        public static void Cross(ref Vector2 vec1, ref Vector2 vec2, out float cross)
        {
            cross = vec1.X * vec2.Y - vec1.Y * vec2.X;
        }

        /// <summary>
        /// 外積を計算
        /// </summary>
        /// <param name="vec1">ベクトル</param>
        /// <param name="vec2">ベクトル</param>
        /// <returns>外積</returns>
        public static float Cross(Vector2 vec1, Vector2 vec2)
        {
            float cross;
            Cross(ref vec1, ref vec2, out cross);
            return cross;
        }
    }

    /// <summary>
    /// Vector3 に対するヘルパークラス
    /// </summary>
    public static class Vector3Helpers
    {
        /// <summary>
        /// 度をラジアンに変換
        /// </summary>
        /// <param name="rotation">回転</param>
        /// <returns>変換された回転</returns>
        public static Vector3 ToRadians(Vector3 rotation)
        {
            Vector3 result;
            ToRadians(ref rotation, out result);
            return result;
        }

        /// <summary>
        /// 度をラジアンに変換
        /// </summary>
        /// <param name="rotation">回転</param>
        /// <param name="result">変換された回転</param>
        public static void ToRadians(ref Vector3 rotation, out Vector3 result)
        {
            result = Vector3.Zero;
            result.X = MathHelper.ToRadians(rotation.X);
            result.Y = MathHelper.ToRadians(rotation.Y);
            result.Z = MathHelper.ToRadians(rotation.Z);
        }

        /// <summary>
        /// ラジアンを度に変換
        /// </summary>
        /// <param name="rotation">回転</param>
        /// <returns>変換された回転</returns>
        public static Vector3 ToDegrees(Vector3 rotation)
        {
            Vector3 result;
            ToDegrees(ref rotation, out result);
            return result;
        }

        /// <summary>
        /// ラジアンを度に変換
        /// </summary>
        /// <param name="rotation">回転</param>
        /// <param name="result">変換された回転</param>
        public static void ToDegrees(ref Vector3 rotation, out Vector3 result)
        {
            result = Vector3.Zero;
            result.X = MathHelper.ToDegrees(rotation.X);
            result.Y = MathHelper.ToDegrees(rotation.Y);
            result.Z = MathHelper.ToDegrees(rotation.Z);
        }
    }

    /// <summary>
    /// Quaternion に対するヘルパークラス
    /// </summary>
    public static class QuaternionHelpers
    {
        const float ZeroTolerance = 1e-6f;

        /// <summary>
        /// オイラー角に変換（ヨーピッチロール回転、ラジアン）
        /// </summary>
        /// <param name="orientation">方向</param>
        /// <returns>オイラー角</returns>
        public static Vector3 ToEuler(Quaternion orientation)
        {
            Vector3 result;
            ToEuler(ref orientation, out result);
            return result;
        }

        /// <summary>
        /// オイラー角に変換（ヨーピッチロール回転、ラジアン）
        /// </summary>
        /// <param name="orientation">方向</param>
        /// <param name="rotation">オイラー角</param>
        public static void ToEuler(ref Quaternion orientation, out Vector3 rotation)
        {
            var forward = Vector3.Forward;
            Vector3.Transform(ref forward, ref orientation, out forward);
            var up = Vector3.Up;
            Vector3.Transform(ref up, ref orientation, out up);

            rotation = Vector3.Zero;
            rotation.X = (float)Math.Asin(forward.Y);
            if (Math.Abs(rotation.X - MathHelper.PiOver2) < ZeroTolerance)
            {
                rotation.Y = (float)Math.Atan2(up.X, up.Z);
                rotation.Z = 0;
            }
            else if (Math.Abs(rotation.X + MathHelper.PiOver2) < ZeroTolerance)
            {
                rotation.Y = (float)Math.Atan2(-up.X, -up.Z);
                rotation.Z = 0;
            }
            else
            {
                rotation.Y = (float)Math.Atan2(-forward.X, -forward.Z);
                Matrix rot;
                Vector3 temp;
                Matrix.CreateRotationY(-rotation.Y, out rot);
                Vector3.Transform(ref up, ref rot, out temp);
                Matrix.CreateRotationX(-rotation.X, out rot);
                Vector3.Transform(ref temp, ref rot, out up);
                rotation.Z = (float)Math.Atan2(-up.X, up.Y);
            }
        }

        /// <summary>
        /// ２つのベクトル間の回転を生成
        /// </summary>
        /// <param name="from">基準となるベクトル</param>
        /// <param name="to">回転後のベクトル</param>
        /// <returns>回転</returns>
        public static Quaternion CreateRotation(Vector3 from, Vector3 to)
        {
            Quaternion result;
            CreateRotation(ref from, ref to, out result);
            return result;
        }

        /// <summary>
        /// ２つのベクトル間の回転を生成
        /// </summary>
        /// <param name="from">基準となるベクトル</param>
        /// <param name="to">回転後のベクトル</param>
        /// <param name="result">回転</param>
        public static void CreateRotation(ref Vector3 from, ref Vector3 to, out Quaternion result)
        {
            Vector3 fromN;
            Vector3.Normalize(ref from, out fromN);
            Vector3 toN;
            Vector3.Normalize(ref to, out toN);

            float dot;
            Vector3.Dot(ref fromN, ref toN, out dot);

            if ((1.0f - ZeroTolerance) < dot)
            {
                result = Quaternion.Identity;
            }
            else if (dot < (ZeroTolerance - 1.0f))
            {
                var axis = Vector3.UnitZ;
                Vector3.Cross(ref axis, ref fromN, out axis);

                if (axis.LengthSquared() < ZeroTolerance)
                {
                    axis = Vector3.UnitX;
                    Vector3.Cross(ref axis, ref fromN, out axis);
                }
                axis.Normalize();

                Quaternion.CreateFromAxisAngle(ref axis, MathHelper.Pi, out result);
            }
            else
            {
                float s = (float)Math.Sqrt((1.0f + dot) * 2);
                float invertS = 1 / s;

                Vector3 axis;
                Vector3.Cross(ref fromN, ref toN, out axis);

                result = Quaternion.Identity;
                result.X = axis.X * invertS;
                result.Y = axis.Y * invertS;
                result.Z = axis.Z * invertS;
                result.W = s * 0.5f;
            }
        }
    }
}

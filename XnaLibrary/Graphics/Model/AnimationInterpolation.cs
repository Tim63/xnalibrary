﻿#region File Description
//-----------------------------------------------------------------------------
// AnimationInterpolation.cs
//
// Update: 2013/09/24
//-----------------------------------------------------------------------------
#endregion

namespace XnaLibrary.Graphics.Model
{
    /// <summary>
    /// アニメーションの補完方法
    /// </summary>
    public enum AnimationInterpolation
    {
        /// <summary>
        /// 前回のクリップと補完
        /// </summary>
        LastClip,

        /// <summary>
        /// クリップ変更時点の状態と補完
        /// </summary>
        ChangeState,
    }
}
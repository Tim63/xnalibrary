﻿#region File Description
//-----------------------------------------------------------------------------
// ShatterModelComponent.cs
// 
// Update: 2014/04/24
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using XnaLibrary.Graphics.Effect;
#endregion

namespace XnaLibrary.Graphics.Model
{
    using Model = Microsoft.Xna.Framework.Graphics.Model;

    /// <summary>
    /// 粉砕処理を実装したモデルコンポーネント
    /// </summary>
    public class ShatterModelComponent : ShadowModelComponent, IModelComponent, IShadowDrawable, IShadowModelComponent, IShatterModelComponent
    {
        #region Properties

        /// <summary>
        /// 粉砕開始してからの時間
        /// </summary>
        public float Time { get; protected set; }

        /// <summary>
        /// 地面の高さ（Y座標）
        /// </summary>
        public float GroundHeight { get; set; }

        /// <summary>
        /// 粉砕の状態
        /// </summary>
        public ShatterState ShatterState { get; private set; }

        /// <summary>
        /// １秒当たりの移動量
        /// </summary>
        public float TranslationRate { get; private set; }

        /// <summary>
        /// １秒当たりの回転量
        /// </summary>
        public float RotationRate { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="model">モデル</param>
        /// <param name="game">ゲームのメインクラス</param>
        public ShatterModelComponent(Model model, Game game)
            : this(model, game, 25, MathHelper.Pi * 3)
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="model">モデル</param>
        /// <param name="game">ゲームのメインクラス</param>
        /// <param name="translationRate">１秒当たりの移動量</param>
        /// <param name="rotationRate">１秒当たりの回転量</param>
        public ShatterModelComponent(Model model, Game game, float translationRate, float rotationRate)
            : base(model, game)
        {
            TranslationRate = translationRate;
            RotationRate = rotationRate;

            Time = 0;
            GroundHeight = 0;
            ShatterState = ShatterState.None;

            ModifyEffect += (effect, mesh) =>
            {
                ShatterEffect shatterEffect = effect as ShatterEffect;
                if (shatterEffect != null)
                {
                    shatterEffect.CreateShadowMap = false;
                    shatterEffect.ShadowEnabled = false;

                    shatterEffect.DirectionalLight0.Direction = LightDirection;

                    shatterEffect.TranslationAmount = TranslationRate * Time;
                    shatterEffect.RotationAmount = RotationRate * Time;
                    shatterEffect.Time = Time;
                    shatterEffect.GroundHeight = GroundHeight;
                }
            };
            ModifyCreateShadowMapEffect = (effect, mesh) =>
            {
                ShatterEffect shatterEffect = effect as ShatterEffect;
                if (shatterEffect != null)
                {
                    shatterEffect.CreateShadowMap = true;

                    shatterEffect.DirectionalLight0.Direction = LightDirection;

                    var transform = BoneTransforms[mesh.ParentBone.Index];
                    var world = WorldMatrix;
                    Matrix.Multiply(ref transform, ref world, out world);
                    shatterEffect.World = world;

                    shatterEffect.LightView = LightView;
                    shatterEffect.LightProjection = LightProjection;

                    shatterEffect.TranslationAmount = TranslationRate * Time;
                    shatterEffect.RotationAmount = RotationRate * Time;
                    shatterEffect.Time = Time;
                    shatterEffect.GroundHeight = GroundHeight;
                }
            };
            ModifyDrawWithShadowEffect = (effect, mesh) =>
            {
                ShatterEffect shatterEffect = effect as ShatterEffect;
                if (shatterEffect != null)
                {
                    shatterEffect.CreateShadowMap = false;
                    shatterEffect.ShadowEnabled = true;

                    shatterEffect.EnableDefaultLighting();
                    shatterEffect.DirectionalLight0.Direction = LightDirection;

                    var transform = BoneTransforms[mesh.ParentBone.Index];
                    var world = WorldMatrix;
                    Matrix.Multiply(ref transform, ref world, out world);
                    shatterEffect.World = world;
                    shatterEffect.View = Camera.View;
                    shatterEffect.Projection = Camera.Projection;

                    shatterEffect.LightView = LightView;
                    shatterEffect.LightProjection = LightProjection;

                    shatterEffect.ShadowMap = ShadowMap;
                    shatterEffect.ShadowMapSize = ShadowMap.Width;

                    shatterEffect.TranslationAmount = TranslationRate * Time;
                    shatterEffect.RotationAmount = RotationRate * Time;
                    shatterEffect.Time = Time;
                    shatterEffect.GroundHeight = GroundHeight;
                }
            };
        }

        #endregion

        #region Update

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime">ゲームタイム</param>
        public override void Update(GameTime gameTime)
        {
            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (ShatterState == ShatterState.Shattering)
            {
                Time += elapsedTime;
            }
            else if (ShatterState == ShatterState.Building)
            {
                Time = Math.Max(0.0f, Time - elapsedTime);
            }

            base.Update(gameTime);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// 粉砕を始める
        /// </summary>
        public void StartShattering()
        {
            ShatterState = ShatterState.Shattering;
        }

        /// <summary>
        /// 組み立てを始める
        /// </summary>
        public void StartBuilding()
        {
            ShatterState = ShatterState.Building;
        }

        /// <summary>
        /// 粉砕／組み立てを停止する
        /// </summary>
        public void Stop()
        {
            ShatterState = ShatterState.None;
        }

        /// <summary>
        /// リセット
        /// </summary>
        public virtual void Reset()
        {
            ShatterState = ShatterState.None;
            Time = 0;
        }

        #endregion
    }
}

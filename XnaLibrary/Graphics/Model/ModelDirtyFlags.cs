﻿#region File Description
//-----------------------------------------------------------------------------
// ModelDirtyFlags.cs
// 
// Update: 2013/12/23
//-----------------------------------------------------------------------------
#endregion

namespace XnaLibrary.Graphics.Model
{
    [System.Flags]
    internal enum ModelDirtyFlags
    {
        None = 0x0,
        Translation = 0x1,
        Rotation = 0x2,
        Scale = 0x4,
        World = 0x8,
        BoundingBox = 0x10,
        All = -1
    }
}
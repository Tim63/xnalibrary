﻿#region File Description
//-----------------------------------------------------------------------------
// AnimationClip.cs
//
// Update: 2014/04/24
//
// Orignal File:
//     Microsoft XNA Community Game Platform
//     Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
#endregion

namespace XnaLibrary.Graphics.Model
{
    /// <summary>
    /// アニメーションに必要な情報を保持するクラス
    /// １つのアニメーションに必要な情報です。
    /// </summary>
    public class AnimationClip
    {
        #region Properties

        /// <summary>
        /// 合計時間の取得
        /// </summary>
        public TimeSpan Duration { get; private set; }

        /// <summary>
        /// キーフレーム配列の取得
        /// 時間によってソートされている必要があります。
        /// </summary>
        public IList<Keyframe> Keyframes { get; private set; }

        /// <summary>
        /// クリップ名の取得
        /// </summary>
        public string Name { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public AnimationClip(string name, TimeSpan duration, IList<Keyframe> keyframes)
        {
            Name = name;
            Duration = duration;
            Keyframes = keyframes;
        }

        #endregion
    }
}

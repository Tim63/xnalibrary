﻿#region File Description
//-----------------------------------------------------------------------------
// ShatterState.cs
// 
// Update: 2013/08/12
//-----------------------------------------------------------------------------
#endregion

namespace XnaLibrary.Graphics.Model
{
    /// <summary>
    /// 粉砕の状態
    /// </summary>
    public enum ShatterState
    {
        /// <summary>
        /// 何もなし
        /// </summary>
        None,

        /// <summary>
        /// 粉砕中
        /// </summary>
        Shattering,

        /// <summary>
        /// 組み立て中
        /// </summary>
        Building
    }
}

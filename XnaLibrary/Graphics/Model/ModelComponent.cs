﻿#region File Description
//-----------------------------------------------------------------------------
// ModelComponent.cs
// 
// Update: 2014/04/24
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace XnaLibrary.Graphics.Model
{
    using Model = Microsoft.Xna.Framework.Graphics.Model;

    /// <summary>
    /// モデルコンポーネント
    /// </summary>
    public class ModelComponent : IModelComponent, IDisposable
    {
        #region Fields

        /// <summary>
        /// ダーティーフラグ
        /// </summary>
        ModelDirtyFlags dirtyFlag;

        #endregion

        #region Properties

        /// <summary>
        /// ゲーム
        /// </summary>
        public Game Game { get; private set; }

        /// <summary>
        /// グラフィックデバイス
        /// </summary>
        public GraphicsDevice GraphicsDevice { get; private set; }

        /// <summary>
        /// カメラ
        /// </summary>
        protected ICamera Camera { get; private set; }

        /// <summary>
        /// モデル
        /// </summary>
        protected Model Model { get; private set; }

        /// <summary>
        /// ボーンのトランスフォーム
        /// </summary>
        public IList<Matrix> BoneTransforms { get; private set; }

        /// <summary>
        /// 位置
        /// </summary>
        public Vector3 Position { get; private set; }

        /// <summary>
        /// 方向
        /// </summary>
        public Quaternion Orientation { get; private set; }

        /// <summary>
        /// 回転量（ヨーピッチロール回転、ラジアン）
        /// </summary>
        public Vector3 Rotation
        {
            get
            {
                Vector3 rotation;
                Quaternion orientation = Orientation;
                QuaternionHelpers.ToEuler(ref orientation, out rotation);
                return rotation;
            }
        }

        /// <summary>
        /// 拡縮
        /// </summary>
        public Vector3 Scale { get; private set; }

        /// <summary>
        /// 大きさ（拡縮適応済み）
        /// </summary>
        public Vector3 Size { get { return OriginalSize * Scale; } }

        /// <summary>
        /// 元々の大きさ
        /// </summary>
        public Vector3 OriginalSize { get; private set; }

        /// <summary>
        /// ワールド行列
        /// </summary>
        public Matrix WorldMatrix
        {
            get
            {
                if ((dirtyFlag & ModelDirtyFlags.World) != 0)
                {
                    CalculateWorldMatrix(out worldMatrix);
                    dirtyFlag &= ~ModelDirtyFlags.World;
                }
                return worldMatrix;
            }
        }
        Matrix worldMatrix;

        /// <summary>
        /// 拡縮行列
        /// </summary>
        protected Matrix ScaleMatrix
        {
            get
            {
                if ((dirtyFlag & ModelDirtyFlags.Scale) != 0)
                {
                    CalculateScaleMatrix(out scaleMatrix);
                    dirtyFlag &= ~ModelDirtyFlags.Scale;
                }
                return scaleMatrix;
            }
        }
        Matrix scaleMatrix;

        /// <summary>
        /// 回転行列
        /// </summary>
        protected Matrix RotationMatrix
        {
            get
            {
                if ((dirtyFlag & ModelDirtyFlags.Rotation) != 0)
                {
                    Orientation.Normalize();
                    CalculateRotationMatrix(out rotationMatrix);
                    dirtyFlag &= ~ModelDirtyFlags.Rotation;
                }
                return rotationMatrix;
            }
        }
        Matrix rotationMatrix;

        /// <summary>
        /// 平行移動行列
        /// </summary>
        protected Matrix TranslationMatrix
        {
            get
            {
                if ((dirtyFlag & ModelDirtyFlags.Translation) != 0)
                {
                    CalculateTranslationMatrix(out translationMatrix);
                    dirtyFlag &= ~ModelDirtyFlags.Translation;
                }
                return translationMatrix;
            }
        }
        Matrix translationMatrix;

        /// <summary>
        /// 境界ボックス
        /// </summary>
        public BoundingBox BoundingBox
        {
            get
            {
                if ((dirtyFlag & ModelDirtyFlags.BoundingBox) != 0)
                {
                    CalcurateBoundingBox(out boundingBox);
                    dirtyFlag &= ~ModelDirtyFlags.BoundingBox;
                }
                return boundingBox;
            }
        }
        BoundingBox boundingBox;

        /// <summary>
        /// グラフィックデバイスの設定
        /// 変更する場合、RestoreGraphicsDevice も変更して元に戻すようにする
        /// </summary>
        public ModifyGraphicsDevice ModifyGraphicsDevice { get; set; }

        /// <summary>
        /// グラフィックデバイスを元に戻す
        /// </summary>
        public ModifyGraphicsDevice RestoreGraphicsDevice { get; set; }

        /// <summary>
        /// エフェクトの設定
        /// </summary>
        public ModifyEffect ModifyEffect { get; set; }

        /// <summary>
        /// リソースの解放済みかどうか
        /// </summary>
        public bool IsDisposed { get; private set; }

        #endregion

        #region Initialization

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="model">モデル</param>
        /// <param name="game">ゲームのメインクラス</param>
        public ModelComponent(Model model, Game game)
        {
            Game = game;
            GraphicsDevice = Game.GraphicsDevice;

            Model = model;
            var boneTransforms = new Matrix[Model.Bones.Count];
            Model.CopyAbsoluteBoneTransformsTo(boneTransforms);
            BoneTransforms = Array.AsReadOnly(boneTransforms);
            boneTransforms = null;

            IsDisposed = false;
            dirtyFlag = ModelDirtyFlags.All;

            boundingBox = new BoundingBox();

            ModifyGraphicsDevice = (graphicsDevice) =>
            {
                graphicsDevice.BlendState = BlendState.Opaque;
                graphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
                graphicsDevice.DepthStencilState = DepthStencilState.Default;
            };
            RestoreGraphicsDevice = (graphicsDevice) =>
            {
                graphicsDevice.BlendState = BlendState.Opaque;
                graphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
                graphicsDevice.DepthStencilState = DepthStencilState.Default;
            };
            ModifyEffect = (effect, mesh) =>
            {
                var matrices = effect as IEffectMatrices;
                if (matrices != null)
                {
                    var transform = BoneTransforms[mesh.ParentBone.Index];
                    var world = WorldMatrix;
                    Matrix.Multiply(ref transform, ref world, out world);
                    matrices.World = world;
                    matrices.View = Camera.View;
                    matrices.Projection = Camera.Projection;
                }
            };
        }

        /// <summary>
        /// 初期化
        /// </summary>
        public void Initialize()
        {
            Initialize(Vector3.Zero, Quaternion.Identity, Vector3.One);
        }

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="position">位置</param>
        protected void Initialize(Vector3 position)
        {
            Initialize(position, Quaternion.Identity, Vector3.One);
        }

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="position">位置</param>
        /// <param name="orientation">方向</param>
        protected void Initialize(Vector3 position, Quaternion orientation)
        {
            Initialize(position, orientation, Vector3.One);
        }

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="position">位置</param>
        /// <param name="orientation">方向</param>
        /// <param name="scale">拡縮</param>
        protected virtual void Initialize(Vector3 position, Quaternion orientation, Vector3 scale)
        {
            OriginalSize = CalculateSize();
            Position = position;
            Orientation = orientation;
            Scale = scale;
        }

        /// <summary>
        /// モデルの大きさを計算
        /// </summary>
        /// <returns>大きさ</returns>
        protected virtual Vector3 CalculateSize()
        {
            var min = new Vector3(float.MaxValue);
            var max = new Vector3(float.MinValue);

            foreach (var mesh in Model.Meshes)
            {
                foreach (var part in mesh.MeshParts)
                {
                    int stride = part.VertexBuffer.VertexDeclaration.VertexStride;

                    VertexPositionColor[] data = new VertexPositionColor[part.NumVertices];
                    part.VertexBuffer.GetData(part.VertexOffset * stride, data, 0, part.NumVertices, stride);

                    var transform = BoneTransforms[mesh.ParentBone.Index];
                    for (int i = 0; i < data.Length; i++)
                    {
                        var pos = Vector3.Transform(data[i].Position, transform);

                        min = Vector3.Min(min, pos);
                        max = Vector3.Max(max, pos);
                    }
                }
            }

            return max - min;
        }

        /// <summary>
        /// 使用されている リソースの開放を行う
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 使用されている リソースの開放を行う
        /// </summary>
        /// <param name="disposing">
        /// アンマネージ リソース と マネージ リソースの両方開放するなら true
        /// アンマネージ リソースだけを解放する場合は false
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {
                    Game = null;
                    GraphicsDevice = null;
                    Camera = null;

                    Model = null;
                    BoneTransforms = null;

                    ModifyGraphicsDevice = null;
                    RestoreGraphicsDevice = null;
                    ModifyEffect = null;
                }
                IsDisposed = true;
            }
        }

        /// <summary>
        /// デストラクタ
        /// </summary>
        ~ModelComponent()
        {
            Dispose(false);
        }

        #endregion

        #region Update and Draw

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime">ゲームタイム</param>
        public virtual void Update(GameTime gameTime) { }

        /// <summary>
        /// ワールド行列の計算
        /// </summary>
        /// <param name="worldMatrix">ワールド行列</param>
        protected virtual void CalculateWorldMatrix(out Matrix worldMatrix)
        {
            var scale = ScaleMatrix;
            var rotation = RotationMatrix;
            var translation = TranslationMatrix;
            Matrix tempWorld;
            Matrix.Multiply(ref scale, ref rotation, out tempWorld);
            Matrix.Multiply(ref tempWorld, ref translation, out worldMatrix);
        }

        /// <summary>
        /// 拡縮行列の計算
        /// </summary>
        /// <param name="scaleMatrix">拡縮行列</param>
        protected virtual void CalculateScaleMatrix(out Matrix scaleMatrix)
        {
            var scale = Scale;
            Matrix.CreateScale(ref scale, out scaleMatrix);
        }

        /// <summary>
        /// 回転行列の計算
        /// </summary>
        /// <param name="rotationMatrix">回転行列</param>
        protected virtual void CalculateRotationMatrix(out Matrix rotationMatrix)
        {
            var orientation = Orientation;
            Matrix.CreateFromQuaternion(ref orientation, out rotationMatrix);
        }

        /// <summary>
        /// 平行移動行列の計算
        /// </summary>
        /// <param name="translationMatrix">平行移動行列</param>
        protected virtual void CalculateTranslationMatrix(out Matrix translationMatrix)
        {
            var position = Position;
            Matrix.CreateTranslation(ref position, out translationMatrix);
        }

        /// <summary>
        /// 境界ボックスの計算
        /// </summary>
        /// <returns>境界ボックス</returns>
        protected virtual void CalcurateBoundingBox(out BoundingBox boundingBox)
        {
            var min = new Vector3(float.MaxValue);
            var max = new Vector3(float.MinValue);
            var world = WorldMatrix;

            foreach (var mesh in Model.Meshes)
            {
                var transform = BoneTransforms[mesh.ParentBone.Index];
                Matrix.Multiply(ref transform, ref world, out transform);

                foreach (var part in mesh.MeshParts)
                {
                    int stride = part.VertexBuffer.VertexDeclaration.VertexStride;
                    var data = new VertexPositionColor[part.NumVertices];
                    part.VertexBuffer.GetData(part.VertexOffset * stride, data, 0, part.NumVertices, stride);

                    for (int i = 0; i < data.Length; i++)
                    {
                        var pos = Vector3.Transform(data[i].Position, transform);

                        min = Vector3.Min(min, pos);
                        max = Vector3.Max(max, pos);
                    }
                }
            }

            boundingBox.Min = min;
            boundingBox.Max = max;
        }

        /// <summary>
        /// 描画
        /// </summary>
        /// <param name="gameTime">ゲームタイム</param>
        public virtual void Draw(GameTime gameTime)
        {
            ModifyGraphicsDevice(GraphicsDevice);

            foreach (var mesh in Model.Meshes)
            {
                foreach (var effect in mesh.Effects)
                {
                    ModifyEffect(effect, mesh);
                }
                mesh.Draw();
            }

            RestoreGraphicsDevice(GraphicsDevice);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// 位置を設定
        /// </summary>
        /// <param name="position">位置</param>
        public virtual void SetPosition(ref Vector3 position)
        {
            Position = position;
            dirtyFlag |= ModelDirtyFlags.Translation | ModelDirtyFlags.World | ModelDirtyFlags.BoundingBox;
        }

        /// <summary>
        /// 位置を設定
        /// </summary>
        /// <param name="position">位置</param>
        public void SetPosition(Vector3 position)
        {
            SetPosition(ref position);
        }

        /// <summary>
        /// 位置を設定
        /// </summary>
        /// <param name="x"> X 軸方向の位置</param>
        /// <param name="y"> Y 軸方向の位置</param>
        /// <param name="z"> Z 軸方向の位置</param>
        public void SetPosition(float x, float y, float z)
        {
            var position = new Vector3(x, y, z);
            SetPosition(ref position);
        }

        /// <summary>
        /// 方向を設定
        /// </summary>
        /// <param name="orientation">方向</param>
        public virtual void SetOrientation(ref Quaternion orientation)
        {
            Orientation = orientation;
            dirtyFlag |= ModelDirtyFlags.Rotation | ModelDirtyFlags.World | ModelDirtyFlags.BoundingBox;
        }

        /// <summary>
        /// 方向を設定
        /// </summary>
        /// <param name="orientation">方向</param>
        public void SetOrientation(Quaternion orientation)
        {
            SetOrientation(ref orientation);
        }

        /// <summary>
        /// 方向を設定
        /// </summary>
        /// <param name="yaw"> Y 軸の回転量（ラジアン）</param>
        /// <param name="pitch"> X 軸の回転量（ラジアン）</param>
        /// <param name="roll"> Z 軸の回転量（ラジアン）</param>
        public void SetOrientation(float yaw, float pitch, float roll)
        {
            Quaternion orientation;
            Quaternion.CreateFromYawPitchRoll(yaw, pitch, roll, out orientation);
            SetOrientation(ref orientation);
        }

        /// <summary>
        /// 移動する
        /// </summary>
        /// <param name="vector">移動量</param>
        public void Move(ref Vector3 vector)
        {
            var position = Position + vector;
            SetPosition(ref position);
        }

        /// <summary>
        /// 移動する
        /// </summary>
        /// <param name="vector">移動量</param>
        public void Move(Vector3 vector)
        {
            Move(ref vector);
        }

        /// <summary>
        /// 移動する
        /// </summary>
        /// <param name="x"> X 軸方向の移動量</param>
        /// <param name="y"> Y 軸方向の移動量</param>
        /// <param name="z"> Z 軸方向の移動量</param>
        public void Move(float x, float y, float z)
        {
            var vector = new Vector3(x, y, z);
            Move(ref vector);
        }

        /// <summary>
        /// 回転する
        /// </summary>
        /// <param name="rotation">回転</param>
        public void Rotate(ref Quaternion rotation)
        {
            Quaternion orientation = Orientation;
            Quaternion.Concatenate(ref orientation, ref rotation, out orientation);
            orientation.Normalize();
            SetOrientation(ref orientation);
        }

        /// <summary>
        /// 回転する
        /// </summary>
        /// <param name="rotation">回転</param>
        public void Rotate(Quaternion rotation)
        {
            Rotate(ref rotation);
        }

        /// <summary>
        /// 回転する
        /// </summary>
        /// <param name="yaw"> Y 軸の回転量（ラジアン）</param>
        /// <param name="pitch"> X 軸の回転量（ラジアン）</param>
        /// <param name="roll"> Z 軸の回転量（ラジアン）</param>
        public void Rotate(float yaw, float pitch, float roll)
        {
            Quaternion rotation;
            Quaternion.CreateFromYawPitchRoll(yaw, pitch, roll, out rotation);
            Rotate(ref rotation);
        }

        /// <summary>
        /// 拡縮を設定
        /// </summary>
        /// <param name="scale">拡縮</param>
        public virtual void SetScale(ref Vector3 scale)
        {
            Scale = scale;
            dirtyFlag |= ModelDirtyFlags.Scale | ModelDirtyFlags.World | ModelDirtyFlags.BoundingBox;
        }

        /// <summary>
        /// 拡縮を設定
        /// </summary>
        /// <param name="scale">拡縮</param>
        public virtual void SetScale(Vector3 scale)
        {
            SetScale(ref scale);
        }

        /// <summary>
        /// 拡縮を設定
        /// </summary>
        /// <param name="scale">拡縮</param>
        public void SetScale(float scale)
        {
            var scaleVec = new Vector3(scale);
            SetScale(ref scaleVec);
        }

        /// <summary>
        /// 拡縮を設定
        /// </summary>
        /// <param name="x"> X 軸の拡縮</param>
        /// <param name="y"> Y 軸の拡縮</param>
        /// <param name="z"> Z 軸の拡縮</param>
        public void SetScale(float x, float y, float z)
        {
            var scale = new Vector3(x, y, z);
            SetScale(ref scale);
        }

        /// <summary>
        /// カメラを設定
        /// </summary>
        /// <param name="camera">カメラ</param>
        public virtual void SetCamera(ICamera camera)
        {
            Camera = camera;
        }

        #endregion
    }
}

﻿#region File Description
//-----------------------------------------------------------------------------
// IShatterModelComponent.cs
// 
// Update: 2014/04/24
//-----------------------------------------------------------------------------
#endregion

namespace XnaLibrary.Graphics.Model
{
    /// <summary>
    /// 粉砕処理を実装したモデルコンポーネントの基本となるプロパティ、メソッドを定義
    /// </summary>
    public interface IShatterModelComponent : IModelComponent, IShadowModelComponent, IShadowDrawable
    {
        #region Properties

        /// <summary>
        /// 粉砕開始してからの時間
        /// </summary>
        float Time { get; }

        /// <summary>
        /// 地面の高さ（Y座標）
        /// </summary>
        float GroundHeight { get; }

        /// <summary>
        /// 粉砕の状態
        /// </summary>
        ShatterState ShatterState { get; }

        /// <summary>
        /// １秒当たりの移動量
        /// </summary>
        float TranslationRate { get; }

        /// <summary>
        /// １秒当たりの回転量
        /// </summary>
        float RotationRate { get; }

        #endregion

        #region Methods

        /// <summary>
        /// 粉砕を始める
        /// </summary>
        void StartShattering();

        /// <summary>
        /// 組み立てを始める
        /// </summary>
        void StartBuilding();

        /// <summary>
        /// 粉砕／組み立てを停止する
        /// </summary>
        void Stop();

        /// <summary>
        /// リセット
        /// </summary>
        void Reset();

        #endregion
    }
}

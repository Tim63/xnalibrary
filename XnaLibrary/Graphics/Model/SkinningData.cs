﻿#region File Description
//-----------------------------------------------------------------------------
// SkinningData.cs
//
// Update: 2014/04/24
//
// Orignal File:
//     Microsoft XNA Community Game Platform
//     Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System.Collections.Generic;
using Microsoft.Xna.Framework;
#endregion

namespace XnaLibrary.Graphics.Model
{
    /// <summary>
    /// アニメーションデータをまとめたクラス
    /// ModelクラスのTagプロパティにセットされます。
    /// </summary>
    public class SkinningData
    {
        #region Properties

        /// <summary>
        /// アニメーションクリップ
        /// </summary>
        public IDictionary<string, AnimationClip> AnimationClips
        {
            get { return animationClipsValue; }
        }
        IDictionary<string, AnimationClip> animationClipsValue;

        /// <summary>
        /// ボーンの行列
        /// </summary>
        public IList<Matrix> BindPose
        {
            get { return bindPoseValue; }
        }
        IList<Matrix> bindPoseValue;

        /// <summary>
        /// ボーンの逆行列
        /// </summary>
        public IList<Matrix> InverseBindPose
        {
            get { return inverseBindPoseValue; }
        }
        IList<Matrix> inverseBindPoseValue;

        /// <summary>
        /// ボーン階層のインデックス
        /// </summary>
        public IList<int> SkeletonHierarchy
        {
            get { return skeletonHierarchyValue; }
        }
        IList<int> skeletonHierarchyValue;

        /// <summary>
        /// ボーン名とそのインデックス
        /// </summary>
        public IDictionary<string, int> BoneIndices
        {
            get { return boneIndicesValue; }
        }
        IDictionary<string, int> boneIndicesValue;

        #endregion

        #region Constractor

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public SkinningData(IDictionary<string, AnimationClip> animationClips,
                            IList<Matrix> bindPose, IList<Matrix> inverseBindPose,
                            IList<int> skeletonHierarchy,
                            IDictionary<string, int> boneIndices)
        {
            animationClipsValue = animationClips;
            bindPoseValue = bindPose;
            inverseBindPoseValue = inverseBindPose;
            skeletonHierarchyValue = skeletonHierarchy;
            boneIndicesValue = boneIndices;
        }

        #endregion
    }
}

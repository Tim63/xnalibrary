﻿#region File Description
//-----------------------------------------------------------------------------
// IShadowModelComponent.cs
// 
// Update: 2014/04/24
//-----------------------------------------------------------------------------
#endregion

namespace XnaLibrary.Graphics.Model
{
    /// <summary>
    /// 影の描画を実装したモデルコンポーネントの基本となるプロパティを定義
    /// </summary>
    public interface IShadowModelComponent : IModelComponent, IShadowDrawable
    {
        /// <summary>
        /// シャドウマップの作成に使うエフェクトの設定
        /// </summary>
        ModifyEffect ModifyCreateShadowMapEffect { get; set; }

        /// <summary>
        /// 影をつけて描画する際のエフェクトの設定
        /// </summary>
        ModifyEffect ModifyDrawWithShadowEffect { get; set; }
    }
}

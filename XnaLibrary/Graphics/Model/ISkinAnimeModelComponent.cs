﻿#region File Description
//-----------------------------------------------------------------------------
// ISkinnedModelComponent.cs
// 
// Update: 2014/04/24
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System.Collections.Generic;
#endregion

namespace XnaLibrary.Graphics.Model
{
    /// <summary>
    /// スキンアニメーションに対応したモデルコンポーネントの基本となるメソッド、プロパティを定義
    /// </summary>
    public interface ISkinAnimeModelComponent : IModelComponent
    {
        #region Properties

        /// <summary>
        /// スキニングデータ
        /// </summary>
        SkinningData SkinningData { get; }

        /// <summary>
        /// アニメーションプレイヤー
        /// </summary>
        AnimationPlayer AnimationPlayer { get; }

        /// <summary>
        /// アニメーションの速度
        /// </summary>
        float AnimationSpeed { get; set; }

        /// <summary>
        /// ループ再生中か
        /// </summary>
        bool IsLoopAnimation { get; }

        /// <summary>
        /// アニメーションのループ回数
        /// 0 以下で無限ループ
        /// </summary>
        int AnimationLoopCount { get; set; }

        /// <summary>
        /// クリップ名
        /// </summary>
        IList<string> ClipNames { get; }

        #endregion

        #region Methods

        /// <summary>
        /// アニメーションのクリップを変更
        /// </summary>
        /// <param name="clipName">クリップ名</param>
        /// <param name="isLoop">ループするかどうか</param>
        /// <param name="weight">ウェイト値</param>
        void ChangeAnimationClip(string clipName, bool isLoop, float weight);

        /// <summary>
        /// アニメーションを完了したかどうか
        /// </summary>
        /// <returns>アニメーションを完了したか</returns>
        bool IsAnimationCompleted();

        #endregion
    }
}

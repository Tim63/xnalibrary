﻿#region File Description
//-----------------------------------------------------------------------------
// AnimationPlayer.cs
//
// Update: 2014/04/24
//
// Orignal File:
//     Microsoft XNA Community Game Platform
//     Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
#endregion

namespace XnaLibrary.Graphics.Model
{
    /// <summary>
    /// スキンモデルのアニメーションを行います
    /// </summary>
    public class AnimationPlayer
    {
        #region Fields

        // 現在のクリップのキーフレーム
        int currentKeyframe;

        // 現在のクリップに関する各ボーンマトリックス
        Matrix[] boneTransforms;
        Matrix[] worldTransforms;
        Matrix[] skinTransforms;

        // スキンモデルのボーンマトリックスやスケルトン階層データ
        SkinningData skinningDataValue;

        // 前回のクリップのアニメーション再生に関する情報
        AnimationClip previousClipValue;
        TimeSpan previousTimeValue;
        int previousKeyframe;

        // 前回のクリップのボーンマトリックス
        Matrix[] previousBoneTransforms;

        // 補間の際のウェイト（重み）に関する情報
        float interpolationWeight;
        float interpolationWeightCount;

        // ループ再生の許可／禁止
        bool loopEnable;

        #endregion

        #region Properties

        /// <summary>
        /// 現在のクリップを取得
        /// </summary>
        public AnimationClip CurrentClip
        {
            get { return currentClipValue; }
        }
        AnimationClip currentClipValue;

        /// <summary>
        /// 現在再生している箇所(時間)を取得
        /// </summary>
        public TimeSpan CurrentTime
        {
            get { return currentTimeValue; }
        }
        TimeSpan currentTimeValue;

        /// <summary>
        /// 現在のループ回数
        /// </summary>
        public int CurrentLoopCount
        {
            get { return currentLoopCountValue; }
        }
        int currentLoopCountValue;

        /// <summary>
        /// アニメーションの補完方法
        /// </summary>
        public AnimationInterpolation AnimationInterpolation { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public AnimationPlayer(SkinningData skinningData)
        {
            if (skinningData == null)
            {
                throw new ArgumentNullException("skinningData");
            }

            skinningDataValue = skinningData;

            boneTransforms = new Matrix[skinningData.BindPose.Count];
            worldTransforms = new Matrix[skinningData.BindPose.Count];
            skinTransforms = new Matrix[skinningData.BindPose.Count];

            previousBoneTransforms = new Matrix[skinningData.BindPose.Count];

            // デフォルトはループ再生を許可
            loopEnable = true;

            interpolationWeight = 0.0f;
            interpolationWeightCount = 0.0f;
            AnimationInterpolation = AnimationInterpolation.LastClip;
        }

        #endregion

        #region Update

        /// <summary>
        /// 各ボーンの行列を更新
        /// </summary>
        /// <param name="time">時間</param>
        /// <param name="relativeToCurrentTime">前回からの相対時間か</param>
        public void Update(ref TimeSpan time, bool relativeToCurrentTime)
        {
            // 補間中か？
            if (interpolationWeight > 0)
            {
                if (AnimationInterpolation == AnimationInterpolation.LastClip)
                {
                    // 前回と現在のクリップのボーンマトリックスを更新してから補間を行う
                    UpdateBoneTransforms(time,
                                         relativeToCurrentTime,
                                         ref previousBoneTransforms,
                                         ref previousClipValue,
                                         ref previousTimeValue,
                                         ref previousKeyframe);
                }

                UpdateBoneTransforms(time,
                                     relativeToCurrentTime,
                                     ref boneTransforms,
                                     ref currentClipValue,
                                     ref currentTimeValue,
                                     ref currentKeyframe);

                UpdateInterpolationBoneTransforms();
            }
            else
            {
                // 現在のクリップのボーンマトリックスを更新する
                UpdateBoneTransforms(time,
                                     relativeToCurrentTime,
                                     ref boneTransforms,
                                     ref currentClipValue,
                                     ref currentTimeValue,
                                     ref currentKeyframe);
            }

            UpdateWorldTransforms();
            UpdateSkinTransforms();
        }

        /// <summary>
        /// 指定されたクリップのボーンマトリックス配列を更新
        /// 今回更新されるボーンマトリックスをkeyframesから取得して
        /// ボーンマトリックス配列を更新します。
        /// </summary>
        /// <param name="time">時間</param>
        /// <param name="relativeToCurrentTime">前回からの相対時間か</param>
        /// <param name="targetBoneTransform">ボーンの行列</param>
        /// <param name="targetClip">アニメーションクリップ</param>
        /// <param name="targetTime">再生時間</param>
        /// <param name="targetKeyframe">キーフレーム</param>
        public void UpdateBoneTransforms(TimeSpan time,
                                         bool relativeToCurrentTime,
                                         ref Matrix[] targetBoneTransform,
                                         ref AnimationClip targetClip,
                                         ref TimeSpan targetTime,
                                         ref int targetKeyframe)
        {
            if (targetClip == null)
            {
                throw new InvalidOperationException("AnimationPlayer.Update was called before StartClip");
            }

            // 今回再生するキーフレームの時間を更新
            if (relativeToCurrentTime)
            {
                time += targetTime;

                // 最後まで再生したら時間を戻す
                while (time >= targetClip.Duration)
                {
                    time -= targetClip.Duration;
                }
            }

            // もし時間が範囲外の場合は例外とする
            if ((time < TimeSpan.Zero) || (time >= targetClip.Duration))
            {
                throw new ArgumentOutOfRangeException("time");
            }

            // 時間が戻っているか？
            if (time < targetTime)
            {
                if (loopEnable)
                {
                    // ループ再生する場合は現在のキーフレームとボーンマトリックスを初期化
                    targetKeyframe = 0;
                    skinningDataValue.BindPose.CopyTo(targetBoneTransform, 0);
                    currentLoopCountValue++;
                }
                else
                {
                    // ループ再生しない場合は現在の再生時間をクリップの最後の再生時間を設定
                    targetTime = targetClip.Duration;
                    return;
                }
            }

            targetTime = time;

            // 今回再生するキーフレームのボーンマトリックスを設定
            var keyframes = targetClip.Keyframes;
            while (targetKeyframe < keyframes.Count)
            {
                Keyframe keyframe = keyframes[targetKeyframe];

                // 今回再生する分が終わったらループを抜ける
                if (keyframe.Time > targetTime)
                {
                    break;
                }

                targetBoneTransform[keyframe.Bone] = keyframe.Transform;

                targetKeyframe++;
            }
        }

        /// <summary>
        /// 現在と前回のクリップのボーンの行列を補間
        /// </summary>
        private void UpdateInterpolationBoneTransforms()
        {
            if (interpolationWeight > 0)
            {
                float amount = 1 - interpolationWeight;
                var bindPose = skinningDataValue.BindPose;
                for (int i = 0; i < bindPose.Count; i++)
                {
                    Matrix.Lerp(ref previousBoneTransforms[i], ref boneTransforms[i], amount, out boneTransforms[i]);
                }

                interpolationWeight -= interpolationWeightCount;
            }
        }

        /// <summary>
        /// ワールド行列を更新
        /// </summary>
        public void UpdateWorldTransforms()
        {
            // ルートボーンのマトリックスをワールドマトリックスに変換
            worldTransforms[0] = boneTransforms[0];

            // ルートボーン以外の親子関係を持ったボーンのマトリックスをワールドマトリックスに変換
            var hierarchy = skinningDataValue.SkeletonHierarchy;
            for (int i = 1; i < worldTransforms.Length; i++)
            {
                int parentBone = hierarchy[i];

                Matrix.Multiply(ref boneTransforms[i], ref worldTransforms[parentBone], out worldTransforms[i]);
            }
        }

        /// <summary>
        /// スキニング行列を更新
        /// </summary>
        public void UpdateSkinTransforms()
        {
            var invBindPoses = skinningDataValue.InverseBindPose;
            for (int bone = 0; bone < skinTransforms.Length; bone++)
            {
                var invBindPose = invBindPoses[bone];
                Matrix.Multiply(ref invBindPose, ref worldTransforms[bone], out skinTransforms[bone]);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// アニメーションクリップの再生を開始（ループ有り、ウェイトなし）
        /// </summary>
        /// <param name="clip">アニメーションクリップ</param>
        public void StartClip(AnimationClip clip)
        {
            StartClip(clip, true, 0.0f);
        }

        /// <summary>
        /// アニメーションクリップの再生を開始
        /// </summary>
        /// <param name="clip">アニメーションクリップ</param>
        /// <param name="loop">ループするか</param>
        /// <param name="weight">ウェイト値</param>
        public void StartClip(AnimationClip clip, bool loop, float weight)
        {
            if (clip == null)
            {
                throw new ArgumentNullException("clip");
            }

            interpolationWeight = 0.0f;
            interpolationWeightCount = 0.0f;

            // 補間をする場合は前回のデータを保存する
            if (weight > 0.0f)
            {
                //補間時のウェイトを設定
                interpolationWeight = 1.0f;
                interpolationWeightCount = 1.0f / weight;

                // 再生していたアニメーションの情報を保存
                previousClipValue = currentClipValue;
                previousTimeValue = currentTimeValue;
                previousKeyframe = currentKeyframe;

                // 再生していたアニメーションのボーンマトリックスを保存
                boneTransforms.CopyTo(previousBoneTransforms, 0);
            }

            // 指定されたクリップの保存と初期化
            if (currentClipValue == null || currentClipValue.Name != clip.Name)
            {
                currentLoopCountValue = 1;
            }
            currentClipValue = clip;
            currentTimeValue = TimeSpan.Zero;
            currentKeyframe = 0;

            // 指定されたループ再生の許可／禁止を設定
            loopEnable = loop;

            // ボーンマトリックスをバインドポーズで初期化
            skinningDataValue.BindPose.CopyTo(boneTransforms, 0);
        }

        /// <summary>
        /// 現在のボーンの行列を取得
        /// </summary>
        public Matrix[] GetBoneTransforms()
        {
            return boneTransforms;
        }

        /// <summary>
        /// 現在のワールド行列を取得
        /// </summary>
        public Matrix[] GetWorldTransforms()
        {
            return worldTransforms;
        }

        /// <summary>
        /// 現在のスキニング行列を取得
        /// </summary>
        public Matrix[] GetSkinTransforms()
        {
            return skinTransforms;
        }

        #endregion
    }
}

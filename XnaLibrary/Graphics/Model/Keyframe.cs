﻿#region File Description
//-----------------------------------------------------------------------------
// Keyframe.cs
//
// Update: 2014/04/24
//
// Orignal File:
//     Microsoft XNA Community Game Platform
//     Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
#endregion

namespace XnaLibrary.Graphics.Model
{
    /// <summary>
    /// キーフレームの情報を保持するクラス
    /// </summary>
    public class Keyframe
    {
        #region Properties

        /// <summary>
        /// ボーンインデックス
        /// </summary>
        public int Bone
        {
            get { return boneValue; }
        }
        int boneValue;

        /// <summary>
        /// 時間情報
        /// </summary>
        public TimeSpan Time
        {
            get { return timeValue; }
        }
        TimeSpan timeValue;

        /// <summary>
        /// ボーンの行列
        /// </summary>
        public Matrix Transform
        {
            get { return transformValue; }
        }
        Matrix transformValue;

        #endregion

        #region Constractor

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Keyframe(int bone, TimeSpan time, Matrix transform)
        {
            boneValue = bone;
            timeValue = time;
            transformValue = transform;
        }

        #endregion
    }
}

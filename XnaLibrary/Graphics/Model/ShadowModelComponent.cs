﻿#region File Description
//-----------------------------------------------------------------------------
// ShadowModelComponent.cs
// 
// Update: 2014/04/24
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XnaLibrary.Graphics.Effect;
#endregion

namespace XnaLibrary.Graphics.Model
{
    using Model = Microsoft.Xna.Framework.Graphics.Model;

    /// <summary>
    /// 影の描画を実装したモデルコンポーネント
    /// </summary>
    public class ShadowModelComponent : ModelComponent, IModelComponent, IShadowModelComponent, IShadowDrawable
    {
        #region Properties

        /// <summary>
        /// シャドウマップ
        /// </summary>
        public RenderTarget2D ShadowMap
        {
            get { return shadowMap; }
            set
            {
                if (value.Width != value.Height)
                {
                    new ArgumentException("ShadowMap の幅と高さが異なります");
                }
                shadowMap = value;
            }
        }
        RenderTarget2D shadowMap;

        /// <summary>
        /// ライトの進行方向
        /// </summary>
        public Vector3 LightDirection { get; set; }

        /// <summary>
        /// 光源のビュー行列
        /// </summary>
        public Matrix LightView { get; set; }

        /// <summary>
        /// 光源の射影行列
        /// </summary>
        public Matrix LightProjection { get; set; }

        /// <summary>
        /// シャドウマップの作成に使うエフェクトの設定
        /// </summary>
        public ModifyEffect ModifyCreateShadowMapEffect { get; set; }

        /// <summary>
        /// 影をつけて描画する際のエフェクトの設定
        /// </summary>
        public ModifyEffect ModifyDrawWithShadowEffect { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="model">モデル</param>
        /// <param name="game">ゲームのメインクラス</param>
        public ShadowModelComponent(Model model, Game game)
            : base(model, game)
        {
            ModifyEffect += (effect, mesh) =>
            {
                var shadowEffect = effect as IShadowEffect;
                if (shadowEffect != null)
                {
                    shadowEffect.CreateShadowMap = false;
                    shadowEffect.ShadowEnabled = false;

                    shadowEffect.DirectionalLight0.Direction = LightDirection;
                }
            };
            ModifyCreateShadowMapEffect = (effect, mesh) =>
            {
                var shadowEffect = effect as IShadowEffect;
                if (shadowEffect != null)
                {
                    shadowEffect.CreateShadowMap = true;

                    shadowEffect.DirectionalLight0.Direction = LightDirection;

                    var transform = BoneTransforms[mesh.ParentBone.Index];
                    var world = WorldMatrix;
                    Matrix.Multiply(ref transform, ref world, out world);
                    shadowEffect.World = world;

                    shadowEffect.LightView = LightView;
                    shadowEffect.LightProjection = LightProjection;
                }
            };
            ModifyDrawWithShadowEffect = (effect, mesh) =>
            {
                var shadowEffect = effect as IShadowEffect;
                if (shadowEffect != null)
                {
                    shadowEffect.CreateShadowMap = false;
                    shadowEffect.ShadowEnabled = true;

                    shadowEffect.EnableDefaultLighting();
                    shadowEffect.DirectionalLight0.Direction = LightDirection;

                    var transform = BoneTransforms[mesh.ParentBone.Index];
                    var world = WorldMatrix;
                    Matrix.Multiply(ref transform, ref world, out world);
                    shadowEffect.World = world;
                    shadowEffect.View = Camera.View;
                    shadowEffect.Projection = Camera.Projection;

                    shadowEffect.LightView = LightView;
                    shadowEffect.LightProjection = LightProjection;

                    shadowEffect.ShadowMap = ShadowMap;
                    shadowEffect.ShadowMapSize = ShadowMap.Width;
                }
            };
        }

        #endregion

        #region Draw

        /// <summary>
        /// シャドウマップの作成
        /// </summary>
        public virtual void CreateShadowMap()
        {
            ModifyGraphicsDevice(GraphicsDevice);
            Game.GraphicsDevice.RasterizerState = RasterizerState.CullNone;

            foreach (var mesh in Model.Meshes)
            {
                foreach (var effect in mesh.Effects)
                {
                    ModifyCreateShadowMapEffect(effect, mesh);
                }
                mesh.Draw();
            }

            RestoreGraphicsDevice(GraphicsDevice);
        }

        /// <summary>
        /// 影をつけて描画
        /// </summary>
        /// <param name="gameTime">ゲームタイム</param>
        public virtual void DrawWithShadow(GameTime gameTime)
        {
            ModifyGraphicsDevice(GraphicsDevice);

            foreach (var mesh in Model.Meshes)
            {
                foreach (var effect in mesh.Effects)
                {
                    ModifyDrawWithShadowEffect(effect, mesh);
                }
                mesh.Draw();
            }

            RestoreGraphicsDevice(GraphicsDevice);
        }

        #endregion
    }
}

﻿#region File Description
//-----------------------------------------------------------------------------
// ModelManager.cs
// 
// Update: 2014/04/24
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using XnaLibrary.Screen;
#endregion

namespace XnaLibrary.Graphics.Model
{
    using Model = Microsoft.Xna.Framework.Graphics.Model;

    /// <summary>
    /// モデルマネージャー
    /// </summary>
    public class ModelManager : IDisposable
    {
        #region Fields

        Dictionary<string, Model> models;

        #endregion

        #region Properties

        /// <summary>
        /// 親画面
        /// </summary>
        public BaseScreen ParentScreen { get; private set; }

        /// <summary>
        /// コンテンツマネージャー
        /// </summary>
        public ContentManager Content { get; private set; }

        /// <summary>
        /// リソースの解放済みかどうか
        /// </summary>
        public bool IsDisposed { get; private set; }

        #endregion

        #region Initialization

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="parentScreen">親画面</param>
        public ModelManager(BaseScreen parentScreen)
        {
            ParentScreen = parentScreen;
            Content = parentScreen.Content;

            models = new Dictionary<string, Model>();

            IsDisposed = false;
        }

        /// <summary>
        /// デストラクタ
        /// </summary>
        ~ModelManager()
        {
            Dispose(false);
        }

        /// <summary>
        /// 使用されている リソースの開放を行う
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 使用されている リソースの開放を行う
        /// </summary>
        /// <param name="disposing">
        /// アンマネージ リソース と マネージ リソースの両方開放するなら true
        /// アンマネージ リソースだけを解放する場合は false
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {
                    if (models != null)
                    {
                        models.Clear();
                        models = null;
                    }

                    ParentScreen = null;
                    Content = null;
                }
                IsDisposed = true;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// モデルの読み込み
        /// </summary>
        /// <param name="key">キー</param>
        /// <param name="path">パス</param>
        public void LoadModel(string key, string path)
        {
            models.Add(key, Content.Load<Model>(path));
        }

        /// <summary>
        /// モデルの取得
        /// </summary>
        /// <param name="key">キー</param>
        /// <returns>モデル</returns>
        public Model GetModel(string key)
        {
            return models[key];
        }

        #endregion
    }
}

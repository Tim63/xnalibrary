﻿#region File Description
//-----------------------------------------------------------------------------
// SkinAnimeModelComponent.cs
// 
// Update: 2014/04/24
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using XnaLibrary.Graphics.Effect;

#endregion

namespace XnaLibrary.Graphics.Model
{
    using Model = Microsoft.Xna.Framework.Graphics.Model;

    /// <summary>
    /// スキンアニメーションに対応したモデルコンポーネント
    /// </summary>
    public class SkinAnimeModelComponent : ModelComponent, IModelComponent, ISkinAnimeModelComponent
    {
        #region Properties

        /// <summary>
        /// スキニングデータ
        /// </summary>
        public SkinningData SkinningData { get; private set; }

        /// <summary>
        /// アニメーションプレイヤー
        /// </summary>
        public AnimationPlayer AnimationPlayer { get; private set; }

        /// <summary>
        /// アニメーションの速度
        /// </summary>
        public float AnimationSpeed { get; set; }

        /// <summary>
        /// ループ再生中か
        /// </summary>
        public bool IsLoopAnimation { get; private set; }

        /// <summary>
        /// アニメーションのループ回数
        /// 0 以下で無限ループ
        /// </summary>
        public int AnimationLoopCount { get; set; }

        /// <summary>
        /// クリップ名
        /// </summary>
        public IList<string> ClipNames { get; private set; }

        #endregion

        #region Initialization

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="model">モデル</param>
        /// <param name="game">ゲームのメインクラス</param>
        public SkinAnimeModelComponent(Model model, Game game)
            : base(model, game)
        {
            SkinningData = Model.Tag as SkinningData;

            AnimationPlayer = new AnimationPlayer(SkinningData);

            AnimationSpeed = 1.0f;
            AnimationLoopCount = 0;

            var clipNames = new string[SkinningData.AnimationClips.Keys.Count];
            SkinningData.AnimationClips.Keys.CopyTo(clipNames, 0);
            ClipNames = Array.AsReadOnly(clipNames);
            ChangeAnimationClip(ClipNames[0], true);

            ModifyEffect += (effect, mesh) =>
            {
                var skinnedEffect = effect as ISkinAnimeEffect;
                if (skinnedEffect != null)
                {
                    skinnedEffect.SetBoneTransforms(AnimationPlayer.GetSkinTransforms());
                }
            };
        }

        /// <summary>
        /// 使用されている リソースの開放を行う
        /// </summary>
        /// <param name="disposing">
        /// アンマネージ リソース と マネージ リソースの両方開放するなら true
        /// アンマネージ リソースだけを解放する場合は false
        /// </param>
        protected override void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {
                    SkinningData = null;
                    AnimationPlayer = null;
                    ClipNames = null;
                }
            }

            base.Dispose(disposing);
        }

        #endregion

        #region Update

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime">ゲームタイム</param>
        public override void Update(GameTime gameTime)
        {
            var time = new TimeSpan((long)(gameTime.ElapsedGameTime.Ticks * AnimationSpeed));
            AnimationPlayer.Update(ref time, true);

            base.Update(gameTime);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// アニメーションのクリップを変更
        /// </summary>
        /// <param name="clipName">クリップ名</param>
        /// <param name="isLoop">ループするかどうか</param>
        /// <param name="weight">ウェイト値</param>
        /// <param name="isForceChanging">強制的にアニメーションを変更するかどうか</param>
        public void ChangeAnimationClip(string clipName, bool isLoop, float weight, bool isForceChanging)
        {
            if (SkinningData == null)
            {
                return;
            }

            if (!SkinningData.AnimationClips.ContainsKey(clipName))
            {
                throw new System.ArgumentException("ClipName '" + clipName + "' が存在しません。");
            }

            if (isLoop)
            {
                if (AnimationLoopCount > 0 && AnimationLoopCount - 1 < AnimationPlayer.CurrentLoopCount)
                {
                    isLoop = false;
                }
            }

            if (AnimationPlayer.CurrentClip == null ||
                isForceChanging ||
                AnimationPlayer.CurrentClip.Name != clipName ||
                IsLoopAnimation != isLoop)
            {
                IsLoopAnimation = isLoop;
                var clip = SkinningData.AnimationClips[clipName];
                AnimationPlayer.StartClip(clip, isLoop, weight);
            }
        }

        /// <summary>
        /// アニメーションのクリップを変更
        /// </summary>
        /// <param name="clipName">クリップ名</param>
        /// <param name="isLoop">ループするかどうか</param>
        /// <param name="weight">ウェイト値</param>
        public void ChangeAnimationClip(string clipName, bool isLoop, float weight)
        {
            ChangeAnimationClip(clipName, isLoop, weight, false);
        }

        /// <summary>
        /// アニメーションのクリップを変更
        /// </summary>
        /// <param name="clipName">クリップ名</param>
        /// <param name="isLoop">ループするかどうか</param>
        public void ChangeAnimationClip(string clipName, bool isLoop)
        {
            ChangeAnimationClip(clipName, isLoop, 0.0f, false);
        }

        /// <summary>
        /// アニメーションのクリップを変更
        /// </summary>
        /// <param name="clipName">クリップ名</param>
        public void ChangeAnimationClip(string clipName)
        {
            ChangeAnimationClip(clipName, false, 0.0f, false);
        }

        /// <summary>
        /// アニメーションを完了したかどうか
        /// </summary>
        /// <returns>アニメーションを完了したか</returns>
        public bool IsAnimationCompleted()
        {
            return IsLoopAnimation ? false : AnimationPlayer.CurrentClip.Duration <= AnimationPlayer.CurrentTime;
        }

        #endregion
    }
}

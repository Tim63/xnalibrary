﻿#region File Description
//-----------------------------------------------------------------------------
// IModelComponent.cs
// 
// Update: 2014/04/24
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace XnaLibrary.Graphics.Model
{
    using Effect = Microsoft.Xna.Framework.Graphics.Effect;

    /// <summary>
    /// モデルコンポーネントの基本となるプロパティ、メソッドを定義
    /// </summary>
    public interface IModelComponent
    {
        #region Properties

        /// <summary>
        /// ゲーム
        /// </summary>
        Game Game { get; }

        /// <summary>
        /// グラフィックデバイス
        /// </summary>
        GraphicsDevice GraphicsDevice { get; }

        /// <summary>
        /// 位置
        /// </summary>
        Vector3 Position { get; }

        /// <summary>
        /// 大きさ（拡縮適応済み）
        /// </summary>
        Vector3 Size { get; }

        /// <summary>
        /// 元々の大きさ
        /// </summary>
        Vector3 OriginalSize { get; }

        /// <summary>
        /// 方向
        /// </summary>
        Quaternion Orientation { get; }

        /// <summary>
        /// 拡縮
        /// </summary>
        Vector3 Scale { get; }

        /// <summary>
        /// ボーンのトランスフォーム
        /// </summary>
        IList<Matrix> BoneTransforms { get; }

        /// <summary>
        /// ワールド行列
        /// </summary>
        Matrix WorldMatrix { get; }

        /// <summary>
        /// エフェクトの設定
        /// </summary>
        ModifyEffect ModifyEffect { get; set; }

        /// <summary>
        /// グラフィックデバイスの設定
        /// </summary>
        ModifyGraphicsDevice ModifyGraphicsDevice { get; set; }

        /// <summary>
        /// グラフィックデバイスを元に戻す
        /// </summary>
        ModifyGraphicsDevice RestoreGraphicsDevice { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// 初期化
        /// </summary>
        void Initialize();

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime">ゲームタイム</param>
        void Update(GameTime gameTime);

        /// <summary>
        /// 描画
        /// </summary>
        /// <param name="gameTime">ゲームタイム</param>
        void Draw(GameTime gameTime);

        /// <summary>
        /// 位置を設定
        /// </summary>
        /// <param name="position">位置</param>
        void SetPosition(ref Vector3 position);

        /// <summary>
        /// 方向を設定
        /// </summary>
        /// <param name="orientation">方向</param>
        void SetOrientation(ref Quaternion orientation);

        /// <summary>
        /// 移動する
        /// </summary>
        /// <param name="vector">移動量</param>
        void Move(ref Vector3 vector);

        /// <summary>
        /// 回転する
        /// </summary>
        /// <param name="rotation">回転</param>
        void Rotate(ref Quaternion rotation);

        /// <summary>
        /// カメラを設定
        /// </summary>
        /// <param name="camera">カメラ</param>
        void SetCamera(ICamera camera);

        #endregion
    }

    /// <summary>
    /// エフェクトの設定
    /// </summary>
    /// <param name="effect">エフェクト</param>
    /// <param name="mesh">メッシュ</param>
    public delegate void ModifyEffect(Effect effect, ModelMesh mesh);

    /// <summary>
    /// グラフィックデバイスの設定
    /// </summary>
    /// <param name="graphicsDevice">グラフィックデバイス</param>
    public delegate void ModifyGraphicsDevice(GraphicsDevice graphicsDevice);
}

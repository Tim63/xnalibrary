﻿#region File Description
//-----------------------------------------------------------------------------
// WireFrameDrawer.cs
// 
// Update: 2013/12/22
// 
// Orignal File:
//     DebugDraw.cs
//     http://xbox.create.msdn.com/ja-JP/education/catalog/sample/collision
//     Microsoft XNA Community Game Platform
//     Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XnaLibrary.Geometry;
#endregion

namespace XnaLibrary.Graphics
{
    /// <summary>
    /// ワイヤーフレームの描画
    /// </summary>
    public class WireFrameDrawer : IDisposable
    {
        #region Constants

        /// <summary>
        /// 頂点の最大数
        /// </summary>
        public const int MaxVertices = 2000;

        /// <summary>
        /// インデックスの最大数
        /// </summary>
        public const int MaxIndices = 2000;

        /// <summary>
        /// キューブのインデックス
        /// </summary>
        static readonly ushort[] cubeIndices = new ushort[] { 0, 1, 1, 2, 2, 3, 3, 0, 4, 5, 5, 6, 6, 7, 7, 4, 0, 4, 1, 5, 2, 6, 3, 7 };

        #endregion

        #region Fields

        /// <summary>
        /// ベーシックエフェクト
        /// </summary>
        BasicEffect basicEffect;

        /// <summary>
        /// 頂点バッファ
        /// </summary>
        DynamicVertexBuffer vertexBuffer;

        /// <summary>
        /// インデックスバッファ
        /// </summary>
        DynamicIndexBuffer indexBuffer;

        /// <summary>
        /// 頂点
        /// </summary>
        VertexPositionColor[] vertices = new VertexPositionColor[MaxVertices];

        /// <summary>
        /// インデックス
        /// </summary>
        ushort[] indices = new ushort[MaxIndices];

        /// <summary>
        /// 頂点の数
        /// </summary>
        int vertexCount;

        /// <summary>
        /// インデックスの数
        /// </summary>
        int indexCount;

        #endregion

        #region Properties

        /// <summary>
        /// リソースの解放済みかどうか
        /// </summary>
        public bool IsDisposed { get; private set; }

        #endregion

        #region Initialization

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="device">グラフィックデバイス</param>
        public WireFrameDrawer(GraphicsDevice device)
        {
            vertexBuffer = new DynamicVertexBuffer(device, typeof(VertexPositionColor), MaxVertices, BufferUsage.WriteOnly);
            indexBuffer = new DynamicIndexBuffer(device, typeof(ushort), MaxIndices, BufferUsage.WriteOnly);

            basicEffect = new BasicEffect(device);
            basicEffect.LightingEnabled = false;
            basicEffect.VertexColorEnabled = true;
            basicEffect.TextureEnabled = false;

            IsDisposed = false;
        }

        /// <summary>
        /// デストラクタ
        /// </summary>
        ~WireFrameDrawer()
        {
            Dispose(false);
        }

        /// <summary>
        /// 使用されている リソースの開放を行う
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 使用されている リソースの開放を行う
        /// </summary>
        /// <param name="disposing">
        /// アンマネージ リソース と マネージ リソースの両方開放するなら true
        /// アンマネージ リソースだけを解放する場合は false
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {
                    if (vertexBuffer != null)
                    {
                        vertexBuffer.Dispose();
                        vertexBuffer = null;
                    }

                    if (indexBuffer != null)
                    {
                        indexBuffer.Dispose();
                        indexBuffer = null;
                    }

                    if (basicEffect != null)
                    {
                        basicEffect.Dispose();
                        basicEffect = null;
                    }

                    vertices = null;
                    indices = null;
                }

                IsDisposed = true;
            }
        }

        #endregion

        #region Draw

        /// <summary>
        /// 描画を開始
        /// </summary>
        /// <param name="view">ビュー行列</param>
        /// <param name="projection">射影行列</param>
        public void Begin(ref Matrix view, ref Matrix projection)
        {
            basicEffect.World = Matrix.Identity;
            basicEffect.View = view;
            basicEffect.Projection = projection;

            vertexCount = 0;
            indexCount = 0;
        }

        /// <summary>
        /// 描画を開始
        /// </summary>
        /// <param name="camera">カメラ</param>
        public void Begin(Camera camera)
        {
            basicEffect.World = Matrix.Identity;
            basicEffect.View = camera.View;
            basicEffect.Projection = camera.Projection;

            vertexCount = 0;
            indexCount = 0;
        }

        /// <summary>
        /// 描画を終了
        /// </summary>
        public void End()
        {
            FlushDrawing();
        }

        /// <summary>
        /// キューに入っている図形を描画し、ライン バッファーをリセット
        /// </summary>
        private void FlushDrawing()
        {
            if (indexCount > 0)
            {
                vertexBuffer.SetData(vertices, 0, vertexCount, SetDataOptions.Discard);
                indexBuffer.SetData(indices, 0, indexCount, SetDataOptions.Discard);

                var device = basicEffect.GraphicsDevice;
                device.SetVertexBuffer(vertexBuffer);
                device.Indices = indexBuffer;

                foreach (var pass in basicEffect.CurrentTechnique.Passes)
                {
                    pass.Apply();
                    device.DrawIndexedPrimitives(PrimitiveType.LineList, 0, 0, vertexCount, 0, indexCount / 2);
                }

                device.SetVertexBuffer(null);
                device.Indices = null;
            }
            indexCount = 0;
            vertexCount = 0;
        }

        /// <summary>
        /// 指定された頂点/インデックス数の図形を描画するための
        /// 十分な空間があるかどうかをチェックします。
        /// 必要であれば FlushDrawing() を呼び出して空間を広げます。
        /// </summary>
        /// <param name="numVerts">頂点の数</param>
        /// <param name="numIndices">インデックスの数</param>
        /// <returns>描画できるかどうか</returns>
        private bool Reserve(int numVerts, int numIndices)
        {
            if (numVerts > MaxVertices || numIndices > MaxIndices)
            {
                // 種類を問わず図形を描画できない場合
                return false;
            }

            if (vertexCount + numVerts > MaxVertices || indexCount + numIndices >= MaxIndices)
            {
                // 図形を描画できるが、まず空間を広げる必要がある場合
                FlushDrawing();
            }
            return true;
        }

        /// <summary>
        /// シェイプの描画
        /// </summary>
        /// <param name="positionArray">位置</param>
        /// <param name="indexArray">インデックス</param>
        /// <param name="color">線の色</param>
        public void DrawWireShape(Vector3[] positionArray, ushort[] indexArray, Color color)
        {
            if (Reserve(positionArray.Length, indexArray.Length))
            {
                for (int i = 0; i < indexArray.Length; i++)
                {
                    indices[indexCount++] = (ushort)(vertexCount + indexArray[i]);
                }

                for (int i = 0; i < positionArray.Length; i++)
                {
                    vertices[vertexCount++] = new VertexPositionColor(positionArray[i], color);
                }
            }
        }

        /// <summary>
        /// 2D グリッドを描画 (Begin/End ペアの内部で呼び出す)
        /// </summary>
        /// <param name="xAxis">グリッドのローカル X 軸方向のベクトル方向</param>
        /// <param name="yAxis">グリッドのローカル Y 軸方向のベクトル方向</param>
        /// <param name="origin">グリッドの 3D 開始アンカー ポイント</param>
        /// <param name="iXDivisions">ローカル X 軸方向の分割数</param>
        /// <param name="iYDivisions">ローカル Y 軸方向の分割数</param>
        /// <param name="color">線の色</param>
        public void DrawWireGrid(Vector3 xAxis, Vector3 yAxis, Vector3 origin, int iXDivisions, int iYDivisions, Color color)
        {
            Vector3 pos, step;

            pos = origin;
            step = xAxis / iXDivisions;
            for (int i = 0; i <= iXDivisions; i++)
            {
                DrawLine(pos, pos + yAxis, color);
                pos += step;
            }

            pos = origin;
            step = yAxis / iYDivisions;
            for (int i = 0; i <= iYDivisions; i++)
            {
                DrawLine(pos, pos + xAxis, color);
                pos += step;
            }
        }

        /// <summary>
        /// 視錐台の輪郭を描画
        /// </summary>
        /// <param name="frustum">視錐台</param>
        /// <param name="color">線の色</param>
        public void DrawWireFrustum(BoundingFrustum frustum, Color color)
        {
            DrawWireShape(frustum.GetCorners(), cubeIndices, color);
        }

        /// <summary>
        /// 軸平行境界ボックスの輪郭を描画
        /// </summary>
        /// <param name="box">軸平行境界ボックス</param>
        /// <param name="color">線の色</param>
        public void DrawWireBox(BoundingBox box, Color color)
        {
            DrawWireShape(box.GetCorners(), cubeIndices, color);
        }

        /// <summary>
        /// 有向境界ボックスの輪郭を描画
        /// </summary>
        /// <param name="box">有向境界ボックス</param>
        /// <param name="color">線の色</param>
        public void DrawWireBox(OrientedBoundingBox box, Color color)
        {
            DrawWireShape(box.GetCorners(), cubeIndices, color);
        }

        /// <summary>
        /// 環状リングを描画
        /// </summary>
        /// <param name="origin">環状リングの中心点</param>
        /// <param name="majorAxis">円の主軸の方向</param>
        /// <param name="minorAxis">円の短軸の方向</param>
        /// <param name="segments">線の数</param>
        /// <param name="color">線の色</param>
        public void DrawRing(Vector3 origin, Vector3 majorAxis, Vector3 minorAxis, int segments, Color color)
        {
            if (Reserve(segments, segments * 2))
            {
                for (int i = 0; i < segments; i++)
                {
                    indices[indexCount++] = (ushort)(vertexCount + i);
                    indices[indexCount++] = (ushort)(vertexCount + (i + 1) % segments);
                }
                float delta = MathHelper.TwoPi / segments;
                float cosDelta = (float)Math.Cos(delta);
                float sinDelta = (float)Math.Sin(delta);

                float cosAcc = 1;
                float sinAcc = 0;

                for (int i = 0; i < segments; ++i)
                {
                    Vector3 pos = new Vector3(majorAxis.X * cosAcc + minorAxis.X * sinAcc + origin.X,
                                              majorAxis.Y * cosAcc + minorAxis.Y * sinAcc + origin.Y,
                                              majorAxis.Z * cosAcc + minorAxis.Z * sinAcc + origin.Z);

                    vertices[vertexCount++] = new VertexPositionColor(pos, color);

                    float newCos = cosAcc * cosDelta - sinAcc * sinDelta;
                    float newSin = cosAcc * sinDelta + sinAcc * cosDelta;

                    cosAcc = newCos;
                    sinAcc = newSin;
                }
            }
        }

        /// <summary>
        /// 境界球の輪郭を描画 (各軸平面に環状リングを描画)
        /// </summary>
        /// <param name="sphere">境界球</param>
        /// <param name="segments">線の数</param>
        /// <param name="color">線の色</param>
        public void DrawWireSphere(BoundingSphere sphere, int segments, Color color)
        {
            DrawRing(sphere.Center, Vector3.Right * sphere.Radius, Vector3.Up * sphere.Radius, segments, color);
            DrawRing(sphere.Center, Vector3.Up * sphere.Radius, Vector3.Backward * sphere.Radius, segments, color);
            DrawRing(sphere.Center, Vector3.Backward * sphere.Radius, Vector3.Right * sphere.Radius, segments, color);
        }

        /// <summary>
        /// 境界球の輪郭を描画 (各軸平面に環状リングを描画)
        /// </summary>
        /// <param name="sphere">境界球</param>
        /// <param name="color">線の色</param>
        public void DrawWireSphere(BoundingSphere sphere, Color color)
        {
            DrawWireSphere(sphere, 32, color);
        }

        /// <summary>
        /// 光線を描画
        /// </summary>
        /// <param name="ray">光線</param>
        /// <param name="color">線の色</param>
        /// <param name="length">長さ</param>
        public void DrawRay(Ray ray, Color color, float length)
        {
            DrawLine(ray.Position, ray.Position + ray.Direction * length, color);
        }

        /// <summary>
        /// 線を描画
        /// </summary>
        /// <param name="startPosition">始点</param>
        /// <param name="endPosition">終点</param>
        /// <param name="color">線の色</param>
        public void DrawLine(Vector3 startPosition, Vector3 endPosition, Color color)
        {
            if (Reserve(2, 2))
            {
                indices[indexCount++] = (ushort)vertexCount;
                indices[indexCount++] = (ushort)(vertexCount + 1);
                vertices[vertexCount++] = new VertexPositionColor(startPosition, color);
                vertices[vertexCount++] = new VertexPositionColor(endPosition, color);
            }
        }

        /// <summary>
        /// 三角形の輪郭を描画
        /// </summary>
        /// <param name="position1">頂点１</param>
        /// <param name="position2">頂点２</param>
        /// <param name="position3">頂点３</param>
        /// <param name="color">線の色</param>
        public void DrawWireTriangle(Vector3 position1, Vector3 position2, Vector3 position3, Color color)
        {
            if (Reserve(3, 6))
            {
                indices[indexCount++] = (ushort)(vertexCount + 0);
                indices[indexCount++] = (ushort)(vertexCount + 1);
                indices[indexCount++] = (ushort)(vertexCount + 1);
                indices[indexCount++] = (ushort)(vertexCount + 2);
                indices[indexCount++] = (ushort)(vertexCount + 2);
                indices[indexCount++] = (ushort)(vertexCount + 0);

                vertices[vertexCount++] = new VertexPositionColor(position1, color);
                vertices[vertexCount++] = new VertexPositionColor(position2, color);
                vertices[vertexCount++] = new VertexPositionColor(position3, color);
            }
        }

        #endregion
    }
}

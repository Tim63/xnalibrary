﻿#region File Description
//-----------------------------------------------------------------------------
// CameraDirtyFlags.cs
//
// Update: 2014/04/10
//-----------------------------------------------------------------------------
#endregion

namespace XnaLibrary.Graphics
{
    [System.Flags]
    internal enum CameraDirtyFlags
    {
        None = 0x0,
        View = (1 << 0),
        Projection = (1 << 1),
        Frustum = (1 << 2),
        All = -1,
    }
}

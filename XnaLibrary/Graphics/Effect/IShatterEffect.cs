﻿#region File Description
//-----------------------------------------------------------------------------
// IShatterEffect.cs
// 
// Update: 2014/04/18
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace XnaLibrary.Graphics.Effect
{
    /// <summary>
    /// 粉砕エフェクトに対応したエフェクトの基本となるプロパティを定義
    /// </summary>
    public interface IShatterEffect : IEffectMatrices, IEffectLights
    {
        /// <summary>
        /// 回転量
        /// </summary>
        float RotationAmount { get; set; }

        /// <summary>
        /// 移動量
        /// </summary>
        float TranslationAmount { get; set; }

        /// <summary>
        /// 粉砕開始してからの時間
        /// </summary>
        float Time { get; set; }

        /// <summary>
        /// 地面の高さ（Y座標）
        /// </summary>
        float GroundHeight { get; set; }

        /// <summary>
        /// 重力
        /// </summary>
        float Gravity { get; set; }
    }
}

﻿#region File Description
//-----------------------------------------------------------------------------
// ISkinAnimeEffect.cs
// 
// Update: 2014/04/18
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace XnaLibrary.Graphics.Effect
{
    /// <summary>
    /// スキンアニメーションに対応したエフェクトの基本となるメソッドを定義
    /// </summary>
    public interface ISkinAnimeEffect : IEffectMatrices, IEffectLights
    {
        /// <summary>
        /// ボーンの行列を設定
        /// </summary>
        /// <param name="boneTransforms">ボーンの行列</param>
        void SetBoneTransforms(Matrix[] boneTransforms);

        /// <summary>
        /// ボーンの行列のコピーの取得
        /// </summary>
        Matrix[] GetBoneTransforms(int count);
    }
}

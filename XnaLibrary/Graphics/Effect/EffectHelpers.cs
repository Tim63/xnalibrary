﻿#region File Description
//-----------------------------------------------------------------------------
// EffectHelpers.cs
// 
// Update: 2014/04/24
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace XnaLibrary.Graphics.Effect
{
    [Flags]
    internal enum EffectDirtyFlags
    {
        None = 0,
        WorldViewProj = (1 << 0),
        World = (1 << 1),
        EyePosition = (1 << 2),
        MaterialColor = (1 << 3),
        Fog = (1 << 4),
        FogEnable = (1 << 5),
        LightViewProj = (1 << 6),
        Technique = (1 << 7),
        All = -1
    }

    internal static class EffectHelpers
    {
        /// <summary>
        /// 標準のキー ライト、フィル ライト、バック ライトのリグを設定します。
        /// </summary>
        internal static void EnableDefaultLighting(DirectionalLight light0, DirectionalLight light1, DirectionalLight light2, out Vector3 ambient)
        {
            // キー ライト。
            light0.Direction = new Vector3(-0.5265408f, -0.5735765f, -0.6275069f);
            light0.DiffuseColor = new Vector3(1, 0.9607844f, 0.8078432f);
            light0.SpecularColor = new Vector3(1, 0.9607844f, 0.8078432f);
            light0.Enabled = true;

            // フィル ライト。
            light1.Direction = new Vector3(0.7198464f, 0.3420201f, 0.6040227f);
            light1.DiffuseColor = new Vector3(0.9647059f, 0.7607844f, 0.4078432f);
            light1.SpecularColor = Vector3.Zero;
            light1.Enabled = true;

            // バック ライト。
            light2.Direction = new Vector3(0.4545195f, -0.7660444f, 0.4545195f);
            light2.DiffuseColor = new Vector3(0.3231373f, 0.3607844f, 0.3937255f);
            light2.SpecularColor = new Vector3(0.3231373f, 0.3607844f, 0.3937255f);
            light2.Enabled = true;

            // アンビエント ライト。
            ambient = new Vector3(0.05333332f, 0.09882354f, 0.1819608f);
        }

        /// <summary>
        /// ワールド ビュー射影行列を遅延再計算します。
        /// これは、現在のエフェクト パラメーター設定に基づいて行われます。
        /// </summary>
        internal static void SetWorldViewProj(ref EffectDirtyFlags dirtyFlags,
                                                    ref Matrix world, ref Matrix view, ref Matrix projection, ref Matrix worldView,
                                                    EffectParameter worldViewProjParam)
        {
            // ワールド ビュー射影行列を再計算するかどうか
            if ((dirtyFlags & EffectDirtyFlags.WorldViewProj) != 0)
            {
                Matrix worldViewProj;

                Matrix.Multiply(ref world, ref view, out worldView);
                Matrix.Multiply(ref worldView, ref projection, out worldViewProj);

                worldViewProjParam.SetValue(worldViewProj);

                dirtyFlags &= ~EffectDirtyFlags.WorldViewProj;
            }
        }

        /// <summary>
        /// ワールド ビュー射影行列を遅延再計算します。
        /// これは、現在のエフェクト パラメーター設定に基づいて行われます。
        /// </summary>
        internal static void SetWorldViewProj(ref EffectDirtyFlags dirtyFlags,
                                              ref Matrix world, ref Matrix view, ref Matrix projection, ref Matrix worldView,
                                              EffectParameter worldViewProjParam, EffectParameter viewProjParam)
        {
            // ワールド ビュー射影行列を再計算するかどうか
            if ((dirtyFlags & EffectDirtyFlags.WorldViewProj) != 0)
            {
                Matrix worldViewProj;
                Matrix viewProj;

                Matrix.Multiply(ref world, ref view, out worldView);
                Matrix.Multiply(ref worldView, ref projection, out worldViewProj);

                worldViewProjParam.SetValue(worldViewProj);

                Matrix.Multiply(ref view, ref projection, out viewProj);

                viewProjParam.SetValue(viewProj);

                dirtyFlags &= ~EffectDirtyFlags.WorldViewProj;
            }
        }

        /// <summary>
        /// フォグの量を計算するための、オブジェクト空間の頂点位置を
        /// 使用して内積を取ることができるベクトルを設定します。
        /// </summary>
        internal static void SetFogVector(ref EffectDirtyFlags dirtyFlags, ref Matrix worldView,
                                          bool fogEnabled, float fogStart, float fogEnd,
                                          EffectParameter fogVectorParam)
        {
            if (fogEnabled)
            {
                // フォグ ベクトルを再計算するかどうか
                if ((dirtyFlags & (EffectDirtyFlags.Fog | EffectDirtyFlags.FogEnable)) != 0)
                {
                    if (fogStart == fogEnd)
                    {
                        // 縮退の場合: 開始と終了が同じ場合は、すべてに 100% の
                        // フォグを適用します。
                        fogVectorParam.SetValue(new Vector4(0, 0, 0, 1));
                    }
                    else
                    {
                        // 頂点位置をビュー空間に変換して、その結果の
                        // Z 値を取得し、フォグの開始と終了の距離に基づいて
                        // スケーリングおよびオフセットします。
                        // ここでは Z 成分のみを考慮するので、シェーダーはこれらすべてを
                        // 単一の内積で行うことができます。その際、ワールド ビュー行列の
                        //  Z 行のみが使用されます。

                        var scale = 1 / (fogStart - fogEnd);

                        var fogVector = Vector4.Zero;
                        fogVector.X = worldView.M13 * scale;
                        fogVector.Y = worldView.M23 * scale;
                        fogVector.Z = worldView.M33 * scale;
                        fogVector.W = (worldView.M43 + fogStart) * scale;

                        fogVectorParam.SetValue(fogVector);
                    }

                    dirtyFlags &= ~(EffectDirtyFlags.Fog | EffectDirtyFlags.FogEnable);
                }
            }
            else
            {
                // フォグが無効になっている場合は、フォグ ベクトルが
                // ゼロにリセットされていることを確認します。
                if ((dirtyFlags & EffectDirtyFlags.FogEnable) != 0)
                {
                    fogVectorParam.SetValue(Vector4.Zero);

                    dirtyFlags &= ~EffectDirtyFlags.FogEnable;
                }
            }
        }

        /// <summary>
        /// ワールドの逆転置行列と目の位置を遅延再計算します。
        /// これは、現在のエフェクト パラメーター設定に基づいて行われます。
        /// </summary>
        internal static void SetLightingMatrices(ref EffectDirtyFlags dirtyFlags,
                                                 ref Matrix world, ref Matrix view,
                                                 EffectParameter worldParam, EffectParameter worldInverseTransposeParam, EffectParameter eyePositionParam)
        {
            // ワールド行列とワールドの逆転置行列を設定します。
            if ((dirtyFlags & EffectDirtyFlags.World) != 0)
            {
                Matrix worldTranspose;
                Matrix worldInverseTranspose;

                Matrix.Invert(ref world, out worldTranspose);
                Matrix.Transpose(ref worldTranspose, out worldInverseTranspose);

                worldParam.SetValue(world);
                worldInverseTransposeParam.SetValue(worldInverseTranspose);

                dirtyFlags &= ~EffectDirtyFlags.World;
            }

            // 目の位置を設定します。
            if ((dirtyFlags & EffectDirtyFlags.EyePosition) != 0)
            {
                Matrix viewInverse;

                Matrix.Invert(ref view, out viewInverse);

                eyePositionParam.SetValue(viewInverse.Translation);

                dirtyFlags &= ~EffectDirtyFlags.EyePosition;
            }
        }

        /// <summary>
        /// ディフューズ/放射/アルファのマテリアル カラー パラメーターを設定します。
        /// </summary>
        internal static void SetMaterialColor(float alpha, ref Vector3 diffuseColor, ref Vector3 emissiveColor, ref Vector3 ambientLightColor,
                                              EffectParameter diffuseColorParam, EffectParameter emissiveColorParam)
        {
            // 目的のライティング モデル:
            //
            //     ((AmbientLightColor + sum(diffuse directional light)) * DiffuseColor) + EmissiveColor
            //
            // ライティングが無効になっている場合、アンビエント ライトと
            // 指向性ライトは無視され、以下のようになります。
            //
            //     DiffuseColor + EmissiveColor
            //
            // ライティングが無効になっている場合は、CPU 上で 
            // diffuse+emissive を事前計算することによって、
            // シェーダー命令を 1 つ省略できます。その後、シェーダーは
            //  DiffuseColor を直接使用し、
            // 放射パラメーターを無視できます。
            //
            // ライティングが有効になっている場合は、アンビエントと放射の設定を
            // 結合できます。放射
            // パラメーターを emissive+(ambient*diffuse) に設定すると、
            // シェーダーがアンビエントの効果を追加する必要が
            // なくなり、以下のように計算が単純になります。
            //
            //     (sum(diffuse directional light) * DiffuseColor) + EmissiveColor
            //
            // さらに最適化を行うには、マテリアルのアルファをディフューズ 
            // カラー パラメーターに結合し、このアルファですべてのカラー値を
            // 事前に乗算します。

            Vector4 diffuse = new Vector4();
            Vector3 emissive = new Vector3();

            diffuse.X = diffuseColor.X * alpha;
            diffuse.Y = diffuseColor.Y * alpha;
            diffuse.Z = diffuseColor.Z * alpha;
            diffuse.W = alpha;

            emissive.X = (emissiveColor.X + ambientLightColor.X * diffuseColor.X) * alpha;
            emissive.Y = (emissiveColor.Y + ambientLightColor.Y * diffuseColor.Y) * alpha;
            emissive.Z = (emissiveColor.Z + ambientLightColor.Z * diffuseColor.Z) * alpha;

            diffuseColorParam.SetValue(diffuse);
            emissiveColorParam.SetValue(emissive);
        }
    }
}
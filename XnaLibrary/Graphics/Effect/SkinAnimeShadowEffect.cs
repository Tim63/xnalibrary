﻿#region File Description
//-----------------------------------------------------------------------------
// SkinnedShadowEffect.cs
// 
// Update: 2014/04/24
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace XnaLibrary.Graphics.Effect
{
    using Effect = Microsoft.Xna.Framework.Graphics.Effect;

    /// <summary>
    /// 影の描画ができるスキンアニメーションエフェクト
    /// </summary>
    public class SkinAnimeShadowEffect : Effect, IEffectMatrices, IEffectLights, ISkinAnimeEffect, IShadowEffect
    {
        #region Constants

        /// <summary>
        /// ボーンの最大数
        /// </summary>
        public const int MaxBones = 79;

        #endregion

        #region Effect Parameters

        EffectParameter textureParam;
        EffectParameter diffuseColorParam;
        EffectParameter emissiveColorParam;
        EffectParameter specularColorParam;
        EffectParameter specularPowerParam;
        EffectParameter eyePositionParam;
        EffectParameter fogColorParam;
        EffectParameter fogVectorParam;
        EffectParameter worldParam;
        EffectParameter worldInverseTransposeParam;
        EffectParameter worldViewProjParam;
        EffectParameter bonesParam;
        EffectParameter lightViewProjParam;
        EffectParameter shadowMapParam;
        EffectParameter shadowMapSizeParam;
        EffectParameter shadowBrightnessParam;
        EffectParameter depthBiasParam;

        #endregion

        #region Effect Techniques

        EffectTechnique createShadowMapTech;
        EffectTechnique drawWithShadowTech;
        EffectTechnique normalDrawTech;

        #endregion

        #region Fields

        /// <summary>
        /// 変更があり、再計算が必要な項目のフラグ
        /// </summary>
        EffectDirtyFlags dirtyFlags = EffectDirtyFlags.All;

        Matrix worldView;

        #endregion

        #region Properties

        /// <summary>
        /// ワールド行列
        /// </summary>
        public Matrix World
        {
            get { return world; }
            set
            {
                world = value;
                dirtyFlags |= EffectDirtyFlags.World | EffectDirtyFlags.WorldViewProj;
            }
        }
        Matrix world = Matrix.Identity;

        /// <summary>
        /// ビュー行列
        /// </summary>
        public Matrix View
        {
            get { return view; }
            set
            {
                view = value;
                dirtyFlags |= EffectDirtyFlags.WorldViewProj | EffectDirtyFlags.EyePosition;
            }
        }
        Matrix view = Matrix.Identity;

        /// <summary>
        /// 射影行列
        /// </summary>
        public Matrix Projection
        {
            get { return projection; }
            set
            {
                projection = value;
                dirtyFlags |= EffectDirtyFlags.WorldViewProj;
            }
        }
        Matrix projection = Matrix.Identity;

        /// <summary>
        /// 光源のビュー行列
        /// </summary>
        public Matrix LightView
        {
            get { return lightView; }
            set
            {
                lightView = value;
                dirtyFlags |= EffectDirtyFlags.LightViewProj;
            }
        }
        Matrix lightView = Matrix.Identity;

        /// <summary>
        /// 光源の射影行列
        /// </summary>
        public Matrix LightProjection
        {
            get { return lightProjection; }
            set
            {
                lightProjection = value;
                dirtyFlags |= EffectDirtyFlags.LightViewProj;
            }
        }
        Matrix lightProjection = Matrix.Identity;

        /// <summary>
        /// 環境光の色
        /// </summary>
        public Vector3 AmbientLightColor
        {
            get { return ambientLightColor; }
            set
            {
                ambientLightColor = value;
                dirtyFlags |= EffectDirtyFlags.MaterialColor;
            }
        }
        Vector3 ambientLightColor = Vector3.Zero;

        /// <summary>
        /// 散乱色
        /// </summary>
        public Vector3 DiffuseColor
        {
            get { return diffuseColor; }
            set
            {
                diffuseColor = value;
                dirtyFlags |= EffectDirtyFlags.MaterialColor;
            }
        }
        Vector3 diffuseColor = Vector3.One;

        /// <summary>
        /// 放射色
        /// </summary>
        public Vector3 EmissiveColor
        {
            get { return emissiveColor; }
            set
            {
                emissiveColor = value;
                dirtyFlags |= EffectDirtyFlags.MaterialColor;
            }
        }
        Vector3 emissiveColor = Vector3.Zero;

        /// <summary>
        /// 鏡面反射色
        /// </summary>
        public Vector3 SpecularColor
        {
            get { return specularColorParam.GetValueVector3(); }
            set { specularColorParam.SetValue(value); }
        }

        /// <summary>
        /// 鏡面反射の強さ
        /// </summary>
        public float SpecularPower
        {
            get { return specularPowerParam.GetValueSingle(); }
            set { specularPowerParam.SetValue(value); }
        }

        /// <summary>
        /// アルファ値
        /// </summary>
        public float Alpha
        {
            get { return alpha; }
            set
            {
                alpha = value;
                dirtyFlags |= EffectDirtyFlags.MaterialColor;
            }
        }
        float alpha = 1.0f;

        /// <summary>
        /// 最初の指向性ライト
        /// Enabled を false にしても、自動的に ture 変更されます
        /// </summary>
        public DirectionalLight DirectionalLight0 { get { return directionalLight0; } }
        DirectionalLight directionalLight0;

        /// <summary>
        /// ２番目の指向性ライト
        /// </summary>
        public DirectionalLight DirectionalLight1 { get { return directionalLight1; } }
        DirectionalLight directionalLight1;

        /// <summary>
        /// ３番目の指向性ライト
        /// </summary>
        public DirectionalLight DirectionalLight2 { get { return directionalLight2; } }
        DirectionalLight directionalLight2;

        /// <summary>
        /// フォグの有効フラグを取得または設定します。
        /// </summary>
        public bool FogEnabled
        {
            get { return fogEnabled; }

            set
            {
                if (fogEnabled != value)
                {
                    fogEnabled = value;
                    dirtyFlags |= EffectDirtyFlags.FogEnable;
                }
            }
        }
        bool fogEnabled;

        /// <summary>
        /// フォグの開始距離を取得または設定します。
        /// </summary>
        public float FogStart
        {
            get { return fogStart; }

            set
            {
                fogStart = value;
                dirtyFlags |= EffectDirtyFlags.Fog;
            }
        }
        float fogStart;

        /// <summary>
        /// フォグの終了距離を取得または設定します。
        /// </summary>
        public float FogEnd
        {
            get { return fogEnd; }

            set
            {
                fogEnd = value;
                dirtyFlags |= EffectDirtyFlags.Fog;
            }
        }
        float fogEnd;

        /// <summary>
        /// フォグ カラーを取得または設定します。
        /// </summary>
        public Vector3 FogColor
        {
            get { return fogColorParam.GetValueVector3(); }
            set { fogColorParam.SetValue(value); }
        }

        /// <summary>
        /// ライトの進行方向
        /// </summary>
        public Vector3 LightDirection
        {
            get { return DirectionalLight0.Direction; }
            set { DirectionalLight0.Direction = value; }
        }

        /// <summary>
        /// テクスチャー
        /// </summary>
        public Texture2D Texture
        {
            get { return textureParam.GetValueTexture2D(); }
            set { textureParam.SetValue(value); }
        }

        /// <summary>
        /// シャドウマップ
        /// </summary>
        public Texture2D ShadowMap
        {
            get { return shadowMapParam.GetValueTexture2D(); }
            set { shadowMapParam.SetValue(value); }
        }

        /// <summary>
        /// シャドウマップの大きさ
        /// </summary>
        public int ShadowMapSize
        {
            get { return shadowMapSizeParam.GetValueInt32(); }
            set { shadowMapSizeParam.SetValue(value); }
        }

        /// <summary>
        /// 影の明るさ
        /// </summary>
        public float ShadowBrightness
        {
            get { return shadowBrightnessParam.GetValueSingle(); }
            set { shadowBrightnessParam.SetValue(value); }
        }

        /// <summary>
        /// バイアス値
        /// </summary>
        public float DepthBias
        {
            get { return depthBiasParam.GetValueSingle(); }
            set { depthBiasParam.SetValue(value); }
        }

        /// <summary>
        /// シャドウマップを作るか
        /// </summary>
        public bool CreateShadowMap
        {
            get { return createShadowMap; }
            set
            {
                if (createShadowMap != value)
                {
                    createShadowMap = value;
                    dirtyFlags |= EffectDirtyFlags.Technique;
                }
            }
        }
        bool createShadowMap;

        /// <summary>
        /// 影を有効にするか
        /// </summary>
        public bool ShadowEnabled
        {
            get { return shadowEnabled; }
            set
            {
                if (shadowEnabled != value)
                {
                    shadowEnabled = value;
                    dirtyFlags |= EffectDirtyFlags.Technique;
                }
            }
        }
        bool shadowEnabled;

        /// <summary>
        /// ライティングを有効にするか
        /// </summary>
        public bool LightingEnabled
        {
            get { return true; }
            set { throw new System.NotSupportedException("ライティングを無効にできません"); }
        }

        #endregion

        #region Initialization

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="device">グラフィックデバイス</param>
        public SkinAnimeShadowEffect(GraphicsDevice device)
            : base(device, Resources.SkinAnimeShadowEffect)
        {
            CacheEffectParameters(null);

            DirectionalLight0.Enabled = true;

            SpecularColor = Vector3.One;
            SpecularPower = 16;

            var bones = new Matrix[MaxBones];
            for (int i = 0; i < MaxBones; i++)
            {
                bones[i] = Matrix.Identity;
            }

            SetBoneTransforms(bones);
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="cloneSource">複製元のエフェクト</param>
        protected SkinAnimeShadowEffect(SkinAnimeShadowEffect cloneSource)
            : base(cloneSource)
        {
            CacheEffectParameters(cloneSource);

            world = cloneSource.world;
            view = cloneSource.view;
            projection = cloneSource.projection;

            diffuseColor = cloneSource.diffuseColor;
            emissiveColor = cloneSource.emissiveColor;
            ambientLightColor = cloneSource.ambientLightColor;

            fogEnabled = cloneSource.fogEnabled;
            fogStart = cloneSource.fogStart;
            fogEnd = cloneSource.fogEnd;

            alpha = cloneSource.alpha;

            lightView = cloneSource.lightView;
            lightProjection = cloneSource.lightProjection;

            createShadowMap = cloneSource.createShadowMap;
            shadowEnabled = cloneSource.shadowEnabled;
        }

        /// <summary>
        /// エフェクトパラメーターへのショートカットの参照の検索
        /// </summary>
        /// <param name="cloneSource">複製元のエフェクト</param>
        private void CacheEffectParameters(SkinAnimeShadowEffect cloneSource)
        {
            createShadowMapTech = Techniques["CreateShadowMap"];
            drawWithShadowTech = Techniques["DrawWithShadow"];
            normalDrawTech = Techniques["NormalDraw"];

            textureParam = Parameters["Texture"];
            diffuseColorParam = Parameters["DiffuseColor"];
            emissiveColorParam = Parameters["EmissiveColor"];
            specularColorParam = Parameters["SpecularColor"];
            specularPowerParam = Parameters["SpecularPower"];
            eyePositionParam = Parameters["EyePosition"];
            fogColorParam = Parameters["FogColor"];
            fogVectorParam = Parameters["FogVector"];
            worldParam = Parameters["World"];
            worldInverseTransposeParam = Parameters["WorldInverseTranspose"];
            worldViewProjParam = Parameters["WorldViewProj"];
            bonesParam = Parameters["Bones"];
            lightViewProjParam = Parameters["LightViewProj"];
            shadowMapParam = Parameters["ShadowMap"];
            shadowMapSizeParam = Parameters["ShadowMapSize"];
            shadowBrightnessParam = Parameters["ShadowBrightness"];
            depthBiasParam = Parameters["DepthBias"];

            directionalLight0 = new DirectionalLight(Parameters["Light0Direction"],
                                                     Parameters["Light0DiffuseColor"],
                                                     Parameters["Light0SpecularColor"],
                                                     cloneSource != null ? cloneSource.directionalLight0 : null);
            directionalLight0.Enabled = true;

            directionalLight1 = new DirectionalLight(Parameters["Light1Direction"],
                                                     Parameters["Light1DiffuseColor"],
                                                     Parameters["Light1SpecularColor"],
                                                     cloneSource != null ? cloneSource.directionalLight1 : null);

            directionalLight2 = new DirectionalLight(Parameters["Light2Direction"],
                                                     Parameters["Light2DiffuseColor"],
                                                     Parameters["Light2SpecularColor"],
                                                     cloneSource != null ? cloneSource.directionalLight2 : null);
        }

        #endregion

        #region Apply

        /// <summary>
        /// エフェクトをレンダリングする直前にエフェクトステートを適用する
        /// </summary>
        protected override void OnApply()
        {
            // ワールド ビュー射影行列またはフォグ ベクトルを再計算するかどうか
            EffectHelpers.SetWorldViewProj(ref dirtyFlags,
                                           ref world, ref view, ref projection, ref worldView,
                                           worldViewProjParam);

            // フォグ ベクトルを再計算するかどうか
            EffectHelpers.SetFogVector(ref dirtyFlags, ref worldView, fogEnabled, fogStart, fogEnd, fogVectorParam);

            // ディフューズ/放射/アルファのマテリアル カラー パラメーターを
            // 再計算するかどうか
            if ((dirtyFlags & EffectDirtyFlags.MaterialColor) != 0)
            {
                EffectHelpers.SetMaterialColor(alpha, ref diffuseColor, ref emissiveColor, ref ambientLightColor,
                                               diffuseColorParam, emissiveColorParam);

                dirtyFlags &= ~EffectDirtyFlags.MaterialColor;
            }

            // ワールドの逆転置行列と目の位置を再計算するかどうか
            EffectHelpers.SetLightingMatrices(ref dirtyFlags,
                                              ref world, ref view, worldParam,
                                              worldInverseTransposeParam, eyePositionParam);

            // 最初の指向性ライトを有効にする
            if (!directionalLight0.Enabled)
            {
                directionalLight0.Enabled = true;
            }

            if ((dirtyFlags & EffectDirtyFlags.LightViewProj) != 0)
            {
                Matrix lightViewProj;
                Matrix.Multiply(ref lightView, ref lightProjection, out lightViewProj);
                lightViewProjParam.SetValue(lightViewProj);
                dirtyFlags &= ~EffectDirtyFlags.LightViewProj;
            }

            if ((dirtyFlags & EffectDirtyFlags.Technique) != 0)
            {
                if (CreateShadowMap)
                {
                    CurrentTechnique = createShadowMapTech;
                }
                else if (ShadowEnabled)
                {
                    CurrentTechnique = drawWithShadowTech;
                }
                else
                {
                    CurrentTechnique = normalDrawTech;
                }
                dirtyFlags |= ~EffectDirtyFlags.Technique;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// 複製する
        /// </summary>
        /// <returns>複製したエフェクト</returns>
        public override Effect Clone()
        {
            return new SkinAnimeShadowEffect(this);
        }

        /// <summary>
        /// デフォルトのライティングを有効化
        /// </summary>
        public void EnableDefaultLighting()
        {
            EffectHelpers.EnableDefaultLighting(directionalLight0, directionalLight1, directionalLight2, out ambientLightColor);
            dirtyFlags |= EffectDirtyFlags.MaterialColor;
        }

        /// <summary>
        /// ボーンの行列を設定
        /// </summary>
        /// <param name="boneTransforms">ボーンの行列</param>
        public void SetBoneTransforms(Matrix[] boneTransforms)
        {
            if ((boneTransforms == null) || (boneTransforms.Length == 0))
            {
                throw new System.ArgumentNullException("boneTransforms");
            }

            if (boneTransforms.Length > MaxBones)
            {
                throw new System.ArgumentException();
            }

            bonesParam.SetValue(boneTransforms);
        }

        /// <summary>
        /// ボーンの行列のコピーの取得
        /// </summary>
        public Matrix[] GetBoneTransforms(int count)
        {
            if (count <= 0 || count > MaxBones)
            {
                throw new System.ArgumentOutOfRangeException("count");
            }

            Matrix[] bones = bonesParam.GetValueMatrixArray(count);

            for (int i = 0; i < bones.Length; i++)
            {
                bones[i].M44 = 1;
            }

            return bones;
        }

        #endregion
    }
}

﻿#region File Description
//-----------------------------------------------------------------------------
// IShadowEffect.cs
// 
// Update: 2014/04/18
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace XnaLibrary.Graphics.Effect
{
    /// <summary>
    ///  影の描画を対応したエフェクトの基本となるプロパティを定義
    /// </summary>
    public interface IShadowEffect : IEffectMatrices, IEffectLights
    {
        /// <summary>
        /// 光源のビュー行列
        /// </summary>
        Matrix LightView { get; set; }

        /// <summary>
        /// 光源の射影行列
        /// </summary>
        Matrix LightProjection { get; set; }

        /// <summary>
        /// シャドウマップ
        /// </summary>
        Texture2D ShadowMap { get; set; }

        /// <summary>
        /// ライトの進行方向
        /// </summary>
        Vector3 LightDirection { get; set; }

        /// <summary>
        /// シャドウマップの大きさ
        /// </summary>
        int ShadowMapSize { get; set; }

        /// <summary>
        /// 影の明るさ
        /// </summary>
        float ShadowBrightness { get; set; }

        /// <summary>
        /// バイアス値
        /// </summary>
        float DepthBias { get; set; }

        /// <summary>
        /// シャドウマップを作るか
        /// </summary>
        bool CreateShadowMap { get; set; }

        /// <summary>
        /// 影を有効にするか
        /// </summary>
        bool ShadowEnabled { get; set; }
    }
}

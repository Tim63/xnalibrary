﻿#region File Description
//-----------------------------------------------------------------------------
// Camera.cs
//
// Update: 2014/04/24
// Reference: https://github.com/willcraftia/TestXna/
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
#endregion

namespace XnaLibrary.Graphics
{
    /// <summary>
    /// カメラ
    /// </summary>
    public class Camera : ICamera
    {
        #region Fields

        /// <summary>
        /// ダーティーフラグ
        /// </summary>
        CameraDirtyFlags dirtyFlags;

        #endregion

        #region Properties

        /// <summary>
        /// 位置
        /// </summary>
        public Vector3 Position
        {
            get { return position; }
            set
            {
                position = value;
                dirtyFlags |= CameraDirtyFlags.View | CameraDirtyFlags.Frustum;
            }
        }
        Vector3 position;

        /// <summary>
        /// 方向
        /// </summary>
        public Quaternion Orientation
        {
            get { return orientation; }
            set
            {
                orientation = value;
                dirtyFlags |= CameraDirtyFlags.View | CameraDirtyFlags.Frustum;
            }
        }
        Quaternion orientation;

        /// <summary>
        /// カメラの前方
        /// </summary>
        public Vector3 Forward
        {
            get
            {
                var baseForward = Vector3.Forward;
                Vector3 result;
                Vector3.Transform(ref baseForward, ref orientation, out result);
                return result;
            }
            set
            {
                var baseForward = Vector3.Forward;
                var forward = value;
                QuaternionHelpers.CreateRotation(ref baseForward, ref forward, out orientation);
                dirtyFlags |= CameraDirtyFlags.View | CameraDirtyFlags.Frustum;
            }
        }

        /// <summary>
        /// 右方向
        /// </summary>
        public Vector3 Right
        {
            get
            {
                var baseRight = Vector3.Right;
                Vector3 result;
                Vector3.Transform(ref baseRight, ref orientation, out result);
                return result;
            }
        }

        /// <summary>
        /// 上方向
        /// </summary>
        public Vector3 Up
        {
            get
            {
                var baseUp = Vector3.Up;
                Vector3 result;
                Vector3.Transform(ref baseUp, ref orientation, out result);
                return result;
            }
        }

        /// <summary>
        /// アスペクト比
        /// </summary>
        public float AspectRatio
        {
            get { return aspectRatio; }
            set
            {
                aspectRatio = value;
                dirtyFlags |= CameraDirtyFlags.Projection | CameraDirtyFlags.Frustum;
            }
        }
        float aspectRatio;

        /// <summary>
        /// 視野角
        /// </summary>
        public float FieldOfView
        {
            get { return fieldOfView; }
            set
            {
                fieldOfView = value;
                dirtyFlags |= CameraDirtyFlags.Projection | CameraDirtyFlags.Frustum;
            }
        }
        float fieldOfView;

        /// <summary>
        /// ニアクリップ面の距離
        /// </summary>
        public float NearClipDistance
        {
            get { return nearClipDistance; }
            set
            {
                nearClipDistance = value;
                dirtyFlags |= CameraDirtyFlags.Projection | CameraDirtyFlags.Frustum;
            }
        }
        float nearClipDistance;

        /// <summary>
        /// ファークリップ面の距離
        /// </summary>
        public float FarClipDistance
        {
            get { return farClipDistance; }
            set
            {
                farClipDistance = value;
                dirtyFlags |= CameraDirtyFlags.Projection | CameraDirtyFlags.Frustum;
            }
        }
        float farClipDistance;

        /// <summary>
        /// ビュー行列
        /// </summary>
        public Matrix View
        {
            get
            {
                if ((dirtyFlags & CameraDirtyFlags.View) != 0)
                {
                    UpdateView(ref view);
                    dirtyFlags &= ~CameraDirtyFlags.View;
                }
                return view;
            }
        }
        Matrix view;

        /// <summary>
        /// 射影行列
        /// </summary>
        public Matrix Projection
        {
            get
            {
                if ((dirtyFlags & CameraDirtyFlags.Projection) != 0)
                {
                    UpdateProjection(ref projection);
                    dirtyFlags &= ~CameraDirtyFlags.Projection;
                }
                return projection;
            }
        }
        Matrix projection;

        /// <summary>
        /// 視錐台
        /// </summary>
        public BoundingFrustum Frustum
        {
            get
            {
                if ((dirtyFlags & CameraDirtyFlags.Frustum) != 0)
                {
                    UpdateFrustum(ref boundingFrustum);
                    dirtyFlags &= ~CameraDirtyFlags.Frustum;
                }
                return boundingFrustum;
            }
        }
        BoundingFrustum boundingFrustum;

        #endregion

        #region Constructor

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Camera()
        {
            dirtyFlags = CameraDirtyFlags.All;

            Position = Vector3.Zero;
            Orientation = Quaternion.Identity;
            boundingFrustum = new BoundingFrustum(Matrix.Identity);

            AspectRatio = 16.0f / 9.0f;
            FieldOfView = MathHelper.PiOver4;
            NearClipDistance = 1.0f;
            FarClipDistance = 5000.0f;

            Update();
        }

        #endregion

        #region Update

        /// <summary>
        /// 更新
        /// </summary>
        public virtual void Update()
        {
            if ((dirtyFlags & CameraDirtyFlags.View) != 0)
            {
                UpdateView(ref view);
                dirtyFlags &= ~CameraDirtyFlags.View;
            }
            if ((dirtyFlags & CameraDirtyFlags.Projection) != 0)
            {
                UpdateProjection(ref projection);
                dirtyFlags &= ~CameraDirtyFlags.Projection;
            }
            if ((dirtyFlags & CameraDirtyFlags.Frustum) != 0)
            {
                UpdateFrustum(ref boundingFrustum);
                dirtyFlags &= ~CameraDirtyFlags.Frustum;
            }
        }

        /// <summary>
        /// ビュー行列の更新
        /// </summary>
        /// <param name="view">ビュー行列</param>
        protected virtual void UpdateView(ref Matrix view)
        {
            Matrix rotation;
            Matrix.CreateFromQuaternion(ref orientation, out rotation);

            Matrix transposeRotation;
            Matrix.Transpose(ref rotation, out transposeRotation);

            Vector3 translation;
            Vector3.Transform(ref position, ref transposeRotation, out translation);

            view = transposeRotation;
            view.M41 = -translation.X;
            view.M42 = -translation.Y;
            view.M43 = -translation.Z;
        }

        /// <summary>
        /// 射影行列の更新
        /// </summary>
        /// <param name="projection">射影行列</param>
        protected virtual void UpdateProjection(ref Matrix projection)
        {
            Matrix.CreatePerspectiveFieldOfView(fieldOfView, aspectRatio, nearClipDistance, farClipDistance, out projection);
        }

        /// <summary>
        /// 視錐台の更新
        /// </summary>
        /// <param name="frustum">視錐台</param>
        protected virtual void UpdateFrustum(ref BoundingFrustum frustum)
        {
            frustum.Matrix = View * Projection;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// 移動
        /// </summary>
        /// <param name="vector">移動量</param>
        public void Move(ref Vector3 vector)
        {
            Vector3 translation;
            Vector3.Transform(ref vector, ref orientation, out translation);

            Position += translation;
        }

        /// <summary>
        /// 移動
        /// </summary>
        /// <param name="vector">移動量</param>
        public void Move(Vector3 vector)
        {
            Move(ref vector);
        }

        /// <summary>
        /// 移動
        /// </summary>
        /// <param name="x">カメラの右方向の移動量</param>
        /// <param name="y">カメラの上方向の移動量</param>
        /// <param name="z">カメラの手前方向の移動量</param>
        public void Move(float x, float y, float z)
        {
            var vector = new Vector3(x, y, z);
            Move(ref vector);
        }

        /// <summary>
        /// 軸と平行に移動
        /// </summary>
        /// <param name="vector">移動量</param>
        public void MoveAxis(ref Vector3 vector)
        {
            Position += vector;
        }

        /// <summary>
        /// 軸と平行に移動
        /// </summary>
        /// <param name="vector">移動量</param>
        public void MoveAxis(Vector3 vector)
        {
            MoveAxis(ref vector);
        }

        /// <summary>
        /// 軸と平行に移動
        /// </summary>
        /// <param name="x">X軸の移動量</param>
        /// <param name="y">Y軸の移動量</param>
        /// <param name="z">Z軸の移動量</param>
        public void MoveAxis(float x, float y, float z)
        {
            var vector = new Vector3(x, y, z);
            MoveAxis(ref vector);
        }

        /// <summary>
        /// 回転
        /// </summary>
        /// <param name="rotation">回転</param>
        public void Rotate(ref Quaternion rotation)
        {
            Quaternion newOrientation;
            Quaternion.Concatenate(ref orientation, ref rotation, out newOrientation);
            newOrientation.Normalize();

            Orientation = newOrientation;
        }

        /// <summary>
        /// 回転
        /// </summary>
        /// <param name="rotation">回転</param>
        public void Rotate(Quaternion rotation)
        {
            Rotate(ref rotation);
        }

        /// <summary>
        /// 回転
        /// </summary>
        /// <param name="axis">回転軸</param>
        /// <param name="angle">回転量</param>
        public void Rotate(ref Vector3 axis, float angle)
        {
            Quaternion rotation;
            Quaternion.CreateFromAxisAngle(ref axis, angle, out rotation);

            Rotate(ref rotation);
        }

        /// <summary>
        /// 回転
        /// </summary>
        /// <param name="axis">回転軸</param>
        /// <param name="angle">回転量</param>
        public void Rotate(Vector3 axis, float angle)
        {
            Rotate(ref axis, angle);
        }

        /// <summary>
        /// 指定した地点を中心として回転
        /// </summary>
        /// <param name="rotation">回転</param>
        /// <param name="center">中心</param>
        public void RotateOffCenter(ref Quaternion rotation, ref Vector3 center)
        {
            var distance = position - center;
            Vector3 newDistance;
            Vector3.Transform(ref distance, ref rotation, out newDistance);

            Quaternion newOrientation;
            Quaternion.Concatenate(ref orientation, ref rotation, out newOrientation);

            Position = center + newDistance;
            Orientation = newOrientation;
        }

        /// <summary>
        /// 指定した地点を中心として回転
        /// </summary>
        /// <param name="rotation">回転</param>
        /// <param name="center">中心</param>
        public void RotateOffCenter(Quaternion rotation, Vector3 center)
        {
            RotateOffCenter(ref rotation, ref center);
        }

        /// <summary>
        /// 指定した地点を中心として回転
        /// </summary>
        /// <param name="axis">回転軸</param>
        /// <param name="angle">回転量</param>
        /// <param name="center">中心</param>
        public void RotateOffCenter(ref Vector3 axis, float angle, ref Vector3 center)
        {
            Quaternion rotation;
            Quaternion.CreateFromAxisAngle(ref axis, angle, out rotation);

            RotateOffCenter(ref rotation, ref center);
        }

        /// <summary>
        /// 指定した地点を中心として回転
        /// </summary>
        /// <param name="axis">回転軸</param>
        /// <param name="angle">回転量</param>
        /// <param name="center">中心</param>
        public void RotateOffCenter(Vector3 axis, float angle, Vector3 center)
        {
            RotateOffCenter(ref axis, angle, ref center);
        }

        /// <summary>
        /// ヨー回転を加える
        /// </summary>
        /// <param name="angle">回転量</param>
        /// <param name="useCameraUpVector">ヨー回転軸をカメラの上ベクトルにするかどうか</param>
        public void Yaw(float angle, bool useCameraUpVector)
        {
            var baseAxis = Vector3.UnitY;

            Vector3 axis;
            if (useCameraUpVector)
            {
                Vector3.Transform(ref baseAxis, ref orientation, out axis);
            }
            else
            {
                axis = baseAxis;
            }

            Rotate(ref axis, angle);
        }

        /// <summary>
        /// ヨー回転を加える
        /// </summary>
        /// <param name="angle">回転量</param>
        public void Yaw(float angle)
        {
            Yaw(angle, false);
        }

        /// <summary>
        /// ピッチ回転を加える
        /// </summary>
        /// <param name="angle">回転量</param>
        public void Pitch(float angle)
        {
            var baseAxis = Vector3.UnitX;

            Vector3 axis;
            Vector3.Transform(ref baseAxis, ref orientation, out axis);

            Rotate(ref axis, angle);
        }

        /// <summary>
        /// ロール回転を加える
        /// </summary>
        /// <param name="angle">回転量</param>
        public void Roll(float angle)
        {
            var baseAxis = Vector3.UnitZ;

            Vector3 axis;
            Vector3.Transform(ref baseAxis, ref orientation, out axis);

            Rotate(ref axis, angle);
        }

        #endregion
    }
}
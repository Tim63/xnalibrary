﻿#region File Description
//-----------------------------------------------------------------------------
// ICamera.cs
//
// Update: 2014/04/08
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
#endregion

namespace XnaLibrary.Graphics
{
    /// <summary>
    /// カメラの基本となるインターフェース
    /// </summary>
    public interface ICamera
    {
        #region Properties

        /// <summary>
        /// ビュー行列
        /// </summary>
        Matrix View { get; }

        /// <summary>
        /// 射影行列
        /// </summary>
        Matrix Projection { get; }

        #endregion

        #region Methods

        /// <summary>
        /// 更新
        /// </summary>
        void Update();

        #endregion
    }
}

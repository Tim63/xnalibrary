﻿#region File Description
//-----------------------------------------------------------------------------
// IShadowDrawable.cs
// 
// Update: 2013/10/21
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace XnaLibrary.Graphics
{
    /// <summary>
    /// 影の描画に対応するためのプロパティ、メソッドを定義
    /// </summary>
    public interface IShadowDrawable
    {
        #region Properties

        /// <summary>
        /// シャドウマップ
        /// </summary>
        RenderTarget2D ShadowMap { get; set; }

        /// <summary>
        /// ライトの進行方向
        /// </summary>
        Vector3 LightDirection { get; set; }

        /// <summary>
        /// 光源のビュー行列
        /// </summary>
        Matrix LightView { get; set; }

        /// <summary>
        /// 光源の射影行列
        /// </summary>
        Matrix LightProjection { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// シャドウマップの作成
        /// </summary>
        void CreateShadowMap();

        /// <summary>
        /// 影をつけて描画
        /// </summary>
        /// <param name="gameTime">ゲームタイム</param>
        void DrawWithShadow(GameTime gameTime);

        #endregion
    }
}

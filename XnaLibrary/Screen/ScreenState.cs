﻿#region File Description
//-----------------------------------------------------------------------------
// ScreenState.cs
//
// Update: 2014/01/08
//-----------------------------------------------------------------------------
#endregion

namespace XnaLibrary.Screen
{
    /// <summary>
    /// 画面の状態
    /// </summary>
    public enum ScreenState
    {
        /// <summary>
        /// 初期化されていない
        /// </summary>
        UnInitialized,

        /// <summary>
        /// 読み込み中
        /// </summary>
        Loading,

        /// <summary>
        /// 読み込みスレッド待ち
        /// </summary>
        WaitThread,

        /// <summary>
        /// 読み込み完了
        /// </summary>
        LoadCompleted,

        /// <summary>
        /// アクティブ
        /// </summary>
        Active,

        /// <summary>
        /// 終了する
        /// </summary>
        End,
    }
}
﻿#region File Description
//-----------------------------------------------------------------------------
// ScreenManager.cs
//
// Update: 2014/04/24
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
#if !Redistributable
using Microsoft.Xna.Framework.GamerServices;
#endif
using Microsoft.Xna.Framework.Graphics;
using XnaLibrary.Input;
using XnaLibrary.Sound;
#endregion

namespace XnaLibrary.Screen
{
    /// <summary>
    /// スクリーンマネージャー
    /// </summary>
    public class ScreenManager : IDisposable
    {
        #region Fields

        /// <summary>
        /// 現在のスクリーン
        /// </summary>
        BaseScreen currentScreen;

#if !Redistributable
        /// <summary>
        /// ガイドによるポーズか
        /// </summary>
        bool isPausedForGuide;
#endif

        #endregion

        #region Properties

        /// <summary>
        /// ゲームのメインクラス
        /// </summary>
        public Game Game { get; private set; }

        /// <summary>
        /// グラフィックデバイス
        /// </summary>
        public GraphicsDevice GraphicsDevice { get; private set; }

        /// <summary>
        /// インプットマネージャー
        /// </summary>
        protected InputManager InputManager { get; set; }

        /// <summary>
        /// サウンドマネージャー
        /// </summary>
        public SoundManager SoundManager { get; protected set; }

        /// <summary>
        /// ブランクテクスチャ
        /// </summary>
        public Texture2D BlankTexture { get; private set; }

        /// <summary>
        /// 初期画面
        /// </summary>
        public BaseScreen StartupScreen { get; set; }

        /// <summary>
        /// フォーカスをなくした時にポーズするかどうか
        /// </summary>
        public bool IsLostForcusPauseEnabled { get; set; }

        /// <summary>
        /// ゲームパッドが抜かれた時にポーズするかどうか
        /// </summary>
        public bool IsGamePadDisconnectedPauseEnabled { get; private set; }

#if !Redistributable
        /// <summary>
        /// ガイドが表示された時にポーズするかどうか
        /// </summary>
        public bool IsGuideShowPauseEnabled { get; set; }
#endif

        /// <summary>
        /// ポーズ処理を呼び出すか
        /// </summary>
        internal bool IsCallingPause { get; set; }

        /// <summary>
        /// リソースの解放済みかどうか
        /// </summary>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// ブランクテクスチャの大きさ
        /// </summary>
        protected virtual int BlankTextureSize { get { return 2; } }

        #endregion

        #region Initialization

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="game">ゲームのメインクラス</param>
        public ScreenManager(Game game)
        {
            Game = game;
            GraphicsDevice = game.GraphicsDevice;

            currentScreen = null;
            StartupScreen = null;

            InputManager = new InputManager();
            SoundManager = new SoundManager();

            IsLostForcusPauseEnabled = true;
            IsGamePadDisconnectedPauseEnabled = true;

#if !Redistributable
            isPausedForGuide = false;
            IsGuideShowPauseEnabled = true;
#endif
        }

        /// <summary>
        /// 初期化
        /// </summary>
        public virtual void Initialize()
        {
            InputManager.Initialize();

            GraphicsDevice.Textures[0] = null;
            BlankTexture = new Texture2D(GraphicsDevice, BlankTextureSize, BlankTextureSize);
            var colors = new Color[BlankTextureSize * BlankTextureSize];
            for (int i = 0; i < colors.Length; i++)
            {
                colors[i] = Color.White;
            }
            BlankTexture.SetData(colors);
            GraphicsDevice.Textures[0] = null;

            if (StartupScreen == null)
            {
                throw new NullReferenceException("StartupScreen が設定されていません");
            }
            currentScreen = StartupScreen;
            if (currentScreen.ScreenState == ScreenState.UnInitialized)
            {
                currentScreen.Load();
            }
        }

        /// <summary>
        /// デストラクタ
        /// </summary>
        ~ScreenManager()
        {
            Dispose(false);
        }

        /// <summary>
        /// 使用されている リソースの開放を行う
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 使用されている リソースの開放を行う
        /// </summary>
        /// <param name="disposing">
        /// アンマネージ リソース と マネージ リソースの両方開放するなら true
        /// アンマネージ リソースだけを解放する場合は false
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {
                    if (currentScreen != null)
                    {
                        if (!currentScreen.IsDisposed)
                        {
                            currentScreen.Dispose();
                        }
                        currentScreen = null;
                    }
                    if (SoundManager != null)
                    {
                        if (!SoundManager.IsDisposed)
                        {
                            SoundManager.Dispose();
                        }
                    }

                    InputManager = null;

                    if (BlankTexture != null)
                    {
                        BlankTexture.Dispose();
                        BlankTexture = null;
                    }

                    Game = null;
                    GraphicsDevice = null;
                }
                IsDisposed = true;
            }
        }

        #endregion

        #region Update and Draw

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime">ゲームタイム</param>
        public void Update(GameTime gameTime)
        {
            InputManager.Update();
            SoundManager.Update();

            if (currentScreen != null)
            {
                switch (currentScreen.ScreenState)
                {
                    case ScreenState.End:
                        var nextScreen = currentScreen.NextScreen;
                        if (nextScreen != null)
                        {
                            currentScreen.Dispose();
                            if (nextScreen.ScreenState == ScreenState.UnInitialized)
                            {
                                nextScreen.Load();
                            }
                            currentScreen = nextScreen;
                        }
                        else
                        {
                            Game.Exit();
                        }
                        break;

                    case ScreenState.Loading:
                    case ScreenState.WaitThread:
                    case ScreenState.LoadCompleted:
                        currentScreen.LoadingUpdate(gameTime, InputManager);
                        break;

                    case ScreenState.Active:
                        if (!currentScreen.IsPaused && currentScreen.IsPauseScreenEnabled)
                        {
                            if ((IsGamePadDisconnectedPauseEnabled && IsGamePadDisconnected(InputManager)) ||
                                (IsLostForcusPauseEnabled && !Game.IsActive))
                            {
                                IsCallingPause = true;
#if !Redistributable
                                isPausedForGuide = false;
#endif
                            }
#if !Redistributable
                            if (IsGuideShowPauseEnabled)
                            {
                                if (!IsCallingPause && Guide.IsVisible)
                                {
                                    isPausedForGuide = true;
                                    IsCallingPause = true;
                                }

                                if (isPausedForGuide && !Guide.IsVisible)
                                {
                                    isPausedForGuide = false;
                                    IsCallingPause = false;
                                }
                            }
#endif
                            if (IsCallingPause)
                            {
                                currentScreen.Pause();
                            }
                        }

                        if (currentScreen.IsPaused)
                        {
                            currentScreen.PauseUpdate(gameTime, InputManager);
                        }
                        else
                        {
                            currentScreen.Update(gameTime, InputManager);
                        }
                        break;
                }
            }
            else
            {
                Game.Exit();
            }
        }

        /// <summary>
        /// ゲームパッドが抜かれたか
        /// </summary>
        /// <param name="inputManager">インプットマネージャー</param>
        /// <returns>ゲームパッドが抜かれたか</returns>
        protected virtual bool IsGamePadDisconnected(InputManager inputManager)
        {
            return (inputManager.IsJustDisconnected(PlayerIndex.One) ||
                inputManager.IsJustDisconnected(PlayerIndex.Two) ||
                inputManager.IsJustDisconnected(PlayerIndex.Three) ||
                inputManager.IsJustDisconnected(PlayerIndex.Four));
        }

        /// <summary>
        /// 描画
        /// </summary>
        /// <param name="gameTime">ゲームタイム</param>
        public void Draw(GameTime gameTime)
        {
            if (currentScreen != null)
            {
                switch (currentScreen.ScreenState)
                {
                    case ScreenState.Loading:
                    case ScreenState.WaitThread:
                    case ScreenState.LoadCompleted:
                        currentScreen.LoadingDraw(gameTime);
                        break;

                    case ScreenState.Active:
                    case ScreenState.End:
                        if (currentScreen.IsPaused)
                        {
                            currentScreen.PauseDraw(gameTime);
                        }
                        else
                        {
                            currentScreen.Draw(gameTime);
                        }
                        break;
                }
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// 四角の描画
        /// </summary>
        /// <param name="spriteBatch">スプライトバッチ</param>
        /// <param name="rectangle">表示する範囲</param>
        /// <param name="color">色</param>
        public void DrawRectangle(SpriteBatch spriteBatch, Rectangle rectangle, Color color)
        {
            spriteBatch.Draw(BlankTexture, rectangle, color);
        }

        /// <summary>
        /// 現在のビューポート全体に四角を描画
        /// </summary>
        /// <param name="spriteBatch">スプライトバッチ</param>
        /// <param name="color">色</param>
        public void DrawRectangleToViewport(SpriteBatch spriteBatch, Color color)
        {
            var rectangle = GraphicsDevice.Viewport.Bounds;
            rectangle.X = 0;
            rectangle.Y = 0;
            DrawRectangle(spriteBatch, rectangle, color);
        }

        #endregion
    }
}

﻿#region File Description
//-----------------------------------------------------------------------------
// OverlayScreen.cs
//
// Update: 2014/01/08
//-----------------------------------------------------------------------------
#endregion

namespace XnaLibrary.Screen
{
    /// <summary>
    /// オーバーレイ画面
    /// </summary>
    public abstract class OverlayScreen : BaseScreen
    {
        #region Properties

        /// <summary>
        /// 親画面
        /// </summary>
        public BaseScreen ParentScreen { get; private set; }

        /// <summary>
        /// 有効になっているかどうか
        /// </summary>
        public bool IsEnabled { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="screen">親画面</param>
        public OverlayScreen(BaseScreen screen)
            : base(screen.ScreenManager)
        {
            ParentScreen = screen;

            IsEnabled = false;

            IsAsyncLoadEnabled = false;
            IsPauseScreenEnabled = false;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// オーバーレイを有効化
        /// </summary>
        public void EnableOverlay()
        {
            IsEnabled = true;

            Reset();
        }

        /// <summary>
        /// オーバーレイを無効化
        /// </summary>
        public void DisableOverlay()
        {
            IsEnabled = false;
        }

        /// <summary>
        /// リセット
        /// </summary>
        protected virtual void Reset() { }

        #endregion
    }
}

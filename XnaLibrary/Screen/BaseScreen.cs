﻿#region File Description
//-----------------------------------------------------------------------------
// BaseScreen.cs
//
// Update: 2014/04/20
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XnaLibrary.Content;
using XnaLibrary.Input;
#endregion

namespace XnaLibrary.Screen
{
    /// <summary>
    /// 各画面となる抽象クラス
    /// </summary>
    public abstract class BaseScreen : IDisposable
    {
        #region Fields

        /// <summary>
        /// スレッド
        /// </summary>
        Thread thread;

        #endregion

        #region Properties

        /// <summary>
        /// ゲームのメインクラス
        /// </summary>
        public Game Game { get; private set; }

        /// <summary>
        /// スクリーンマネージャー
        /// </summary>
        public ScreenManager ScreenManager { get; private set; }

        /// <summary>
        /// コンテンツマネージャー
        /// </summary>
        public LockLoadContentManager Content { get; private set; }

        /// <summary>
        /// グラフィックデバイス
        /// </summary>
        public GraphicsDevice GraphicsDevice { get; private set; }

        /// <summary>
        /// ポーズのオーバーレイ画面
        /// </summary>
        protected OverlayScreen PauseOverlay { get; set; }

        /// <summary>
        /// 画面の状態
        /// </summary>
        public ScreenState ScreenState { get; private set; }

        /// <summary>
        /// 非同期読み込みをするか
        /// </summary>
        public bool IsAsyncLoadEnabled { get; protected set; }

        /// <summary>
        /// リソースの解放済みかどうか
        /// </summary>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// ポーズ中かどうか
        /// </summary>
        public bool IsPaused { get; private set; }

        /// <summary>
        /// ポーズ画面を使うか
        /// </summary>
        public bool IsPauseScreenEnabled { get; protected set; }

        /// <summary>
        /// 次のスクリーン
        /// </summary>
        protected internal BaseScreen NextScreen { get; private set; }

        #endregion

        #region Initialization

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="screenManager">スクリーンマネージャー</param>
        public BaseScreen(ScreenManager screenManager)
        {
            this.ScreenManager = screenManager;
            this.Game = screenManager.Game;
            this.GraphicsDevice = screenManager.GraphicsDevice;

            Content = new LockLoadContentManager(Game.Services);

            ScreenState = ScreenState.UnInitialized;

            IsAsyncLoadEnabled = true;
            IsDisposed = false;
            IsPauseScreenEnabled = false;
            IsPaused = false;
        }

        /// <summary>
        /// 読み込み
        /// </summary>
        public void Load()
        {
            ScreenState = ScreenState.Loading;

            if (IsAsyncLoadEnabled)
            {
                Game.Exiting += CancelLoading;
                thread = new Thread(new ThreadStart(Initialize));
                thread.Name = GetType().Name + "_LoadingThread";
                thread.Start();
            }
            else
            {
                Initialize();
                Game.ResetElapsedTime();
                ScreenState = ScreenState.Active;
            }
        }

        /// <summary>
        /// 初期化
        /// </summary>
        protected virtual void Initialize()
        {
            ScreenState = ScreenState.WaitThread;
            if (IsAsyncLoadEnabled)
            {
                Game.Exiting -= CancelLoading;
            }
        }

        /// <summary>
        /// デストラクタ
        /// </summary>
        ~BaseScreen()
        {
            Dispose(false);
        }

        /// <summary>
        /// 使用されている リソースの開放を行う
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 使用されている リソースの開放を行う
        /// </summary>
        /// <param name="disposing">
        /// アンマネージ リソース と マネージ リソースの両方開放するなら true
        /// アンマネージ リソースだけを解放する場合は false
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {
                    if (PauseOverlay != null)
                    {
                        PauseOverlay.Dispose();
                        PauseOverlay = null;
                    }

                    ScreenManager = null;
                    NextScreen = null;

                    Game = null;
                    GraphicsDevice = null;
                    ScreenManager = null;
                    NextScreen = null;

                    thread = null;
                }
                if (Content != null)
                {
                    Content.Unload();
                    Content.Dispose();
                    Content = null;
                }

                IsDisposed = true;
            }
        }

        /// <summary>
        /// ロード中に終了する際のイベント用メソッド
        /// </summary>
        /// <param name="sender">イベントのソース</param>
        /// <param name="e">イベントデータ</param>
        private void CancelLoading(object sender, EventArgs e)
        {
            if (thread != null && thread.IsAlive)
            {
                thread.Abort();
                thread.Join();
            }
        }

        #endregion

        #region Update and Draw

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime">ゲームタイム</param>
        /// <param name="inputManager">インプットマネージャー</param>
        public abstract void Update(GameTime gameTime, InputManager inputManager);

        /// <summary>
        /// ロード中の更新
        /// </summary>
        /// <param name="gameTime">ゲームタイム</param>
        /// <param name="inputManager">インプットマネージャー</param>
        public virtual void LoadingUpdate(GameTime gameTime, InputManager inputManager)
        {
            if (ScreenState == ScreenState.WaitThread)
            {
                thread.Join();
                ScreenState = ScreenState.LoadCompleted;
            }
        }

        /// <summary>
        /// ポーズ中の更新
        /// </summary>
        /// <param name="gameTime">ゲームタイム</param>
        /// <param name="inputManager">インプットマネージャー</param>
        public virtual void PauseUpdate(GameTime gameTime, InputManager inputManager)
        {
            PauseOverlay.Update(gameTime, inputManager);
        }

        /// <summary>
        /// 画面を終了する
        /// </summary>
        public void EndScreen()
        {
            EndScreen(null);
        }

        /// <summary>
        /// 画面を終了する
        /// </summary>
        /// <param name="screen">次の画面</param>
        public void EndScreen(BaseScreen screen)
        {
            NextScreen = screen;
            ScreenState = ScreenState.End;
        }

        /// <summary>
        /// 描画
        /// </summary>
        /// <param name="gameTime">ゲームタイム</param>
        public abstract void Draw(GameTime gameTime);

        /// <summary>
        /// ロード中の描画
        /// </summary>
        /// <param name="gameTime">ゲームタイム</param>
        public virtual void LoadingDraw(GameTime gameTime)
        {
            if (ScreenState == ScreenState.LoadCompleted)
            {
                ScreenState = ScreenState.Active;
            }
        }

        /// <summary>
        /// ポーズ中の描画
        /// </summary>
        /// <param name="gameTime">ゲームタイム</param>
        public virtual void PauseDraw(GameTime gameTime)
        {
            PauseOverlay.Draw(gameTime);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// ポーズ
        /// </summary>
        public virtual void Pause()
        {
            if (IsPauseScreenEnabled)
            {
                ScreenManager.IsCallingPause = false;
                IsPaused = true;

                PauseOverlay.EnableOverlay();
            }
        }

        /// <summary>
        /// ポーズの解除
        /// </summary>
        public virtual void UnPause()
        {
            if (IsPauseScreenEnabled)
            {
                IsPaused = false;

                PauseOverlay.DisableOverlay();
            }
        }

        #endregion
    }
}

﻿#region File Description
//-----------------------------------------------------------------------------
// InputManager.cs
// 
// Update: 2013/09/12
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
#endregion

namespace XnaLibrary.Input
{
    /// <summary>
    /// インプットマネージャー
    /// </summary>
    public class InputManager
    {
        #region Constants

        /// <summary>
        /// 最大人数
        /// </summary>
        protected const int MaxPlayers = 4;

        /// <summary>
        /// スティックのデッドゾーン
        /// </summary>
        protected const float StickDeadZone = 3 / 8f;

        /// <summary>
        /// トリガーのデッドゾーン
        /// </summary>
        protected const float TriggerDeadZone = 1 / 8f;

        /// <summary>
        /// デッドゾーン処理の種類
        /// </summary>
        protected readonly GamePadDeadZone DeadZoneType;

        #endregion

        #region Properties

        /// <summary>
        /// 前回更新時のキーボード情報
        /// </summary>
        protected KeyboardState LastKeyState { get; private set; }

        /// <summary>
        /// 現在のキーボード情報
        /// </summary>
        protected KeyboardState CurrentKeyState { get; private set; }

        /// <summary>
        /// 前回更新時のパッド情報
        /// </summary>
        protected GamePadState[] LastPadState { get; private set; }

        /// <summary>
        /// 現在のパッド情報
        /// </summary>
        protected GamePadState[] CurrentPadState { get; private set; }

        /// <summary>
        /// 前回更新時のチャットパッドの状態
        /// </summary>
        protected KeyboardState[] LastChatPadState { get; private set; }

        /// <summary>
        /// 現在のチャットパッドの状態
        /// </summary>
        protected KeyboardState[] CurrentChatPadState { get; private set; }

        /// <summary>
        /// 初期化済みか
        /// </summary>
        public bool IsInitialized { get; private set; }

        #endregion

        #region Initialize

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public InputManager()
            : this(GamePadDeadZone.IndependentAxes)
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="deadZone">デッドゾーン処理の種類</param>
        public InputManager(GamePadDeadZone deadZone)
        {
            DeadZoneType = deadZone;
            IsInitialized = false;
        }

        /// <summary>
        /// 初期化
        /// </summary>
        public virtual void Initialize()
        {
            CurrentKeyState = Keyboard.GetState();

            LastPadState = new GamePadState[MaxPlayers];
            CurrentPadState = new GamePadState[MaxPlayers];

            LastChatPadState = new KeyboardState[MaxPlayers];
            CurrentChatPadState = new KeyboardState[MaxPlayers];

            for (int i = 0; i < MaxPlayers; i++)
            {
                CurrentPadState[i] = GamePad.GetState((PlayerIndex)i, DeadZoneType);
                CurrentChatPadState[i] = Keyboard.GetState((PlayerIndex)i);
            }

            IsInitialized = true;
        }

        #endregion

        #region Update

        /// <summary>
        /// 更新
        /// </summary>
        public virtual void Update()
        {
            LastKeyState = CurrentKeyState;
            CurrentKeyState = Keyboard.GetState();

            for (int i = 0; i < MaxPlayers; i++)
            {
                LastPadState[i] = CurrentPadState[i];
                CurrentPadState[i] = GamePad.GetState((PlayerIndex)i, DeadZoneType);
                LastChatPadState[i] = CurrentChatPadState[i];
                CurrentChatPadState[i] = Keyboard.GetState((PlayerIndex)i);
            }
        }

        #endregion

        #region Keyboard Inputs

        /// <summary>
        /// キーが押されているか
        /// </summary>
        /// <param name="key">状態を取得したいキー</param>
        /// <returns>押されているかどうか</returns>
        public bool IsKeyDown(Keys key)
        {
            return CurrentKeyState.IsKeyDown(key);
        }

        /// <summary>
        /// キーが離されているか
        /// </summary>
        /// <param name="key">状態を取得したいキー</param>
        /// <returns>離されているかどうか</returns>
        public bool IsKeyUp(Keys key)
        {
            return CurrentKeyState.IsKeyUp(key);
        }

        /// <summary>
        /// 前回更新時にキーが押されていたか
        /// </summary>
        /// <param name="key">状態を取得したいキー</param>
        /// <returns>押されていたかどうか</returns>
        public bool IsLastKeyDown(Keys key)
        {
            return LastKeyState.IsKeyDown(key);
        }

        /// <summary>
        /// 前回更新時にキーが離されていたか
        /// </summary>
        /// <param name="key">状態を取得したいキー</param>
        /// <returns>離されていたかどうか</returns>
        public bool IsLastKeyUp(Keys key)
        {
            return LastKeyState.IsKeyUp(key);
        }

        /// <summary>
        /// キーがこのフレームで押されたか
        /// </summary>
        /// <param name="key">判断するキー</param>
        /// <returns>押されたかどうか</returns>
        public bool IsJustKeyDown(Keys key)
        {
            return (IsLastKeyUp(key) && IsKeyDown(key));
        }

        /// <summary>
        /// キーがこのフレームで離されたか
        /// </summary>
        /// <param name="key">判断するキー</param>
        /// <returns>離されたかどうか</returns>
        public bool IsJustKeyUp(Keys key)
        {
            return (IsLastKeyDown(key) && IsKeyUp(key));
        }

        #endregion

        #region GamePad Inputs

        #region Connection

        /// <summary>
        /// コントローラが繋がっているかどうか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス</param>
        /// <returns>繋がっているかどうか</returns>
        public bool IsConnected(PlayerIndex playerIndex)
        {
            return CurrentPadState[(int)playerIndex].IsConnected;
        }

        /// <summary>
        /// 現在のフレームでコントローラが抜かれたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス</param>
        /// <returns>抜かれたかどうか</returns>
        public bool IsJustDisconnected(PlayerIndex playerIndex)
        {
            return (LastPadState[(int)playerIndex].IsConnected &&
                !CurrentPadState[(int)playerIndex].IsConnected);
        }

        #endregion

        #region Buttons

        /// <summary>
        /// ボタンが押されているか
        /// </summary>
        /// <param name="button">ボタン</param>
        /// <returns>押されているかどうか</returns>
        public bool IsButtonDown(Buttons button)
        {
            return IsButtonDown(button, null);
        }

        /// <summary>
        /// ボタンが押されているか
        /// </summary>
        /// <param name="button">ボタン</param>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>押されているかどうか</returns>
        public bool IsButtonDown(Buttons button, PlayerIndex? playerIndex)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return CurrentPadState[(int)playerIndex.Value].IsButtonDown(button);
            }
            else
            {
                return (IsButtonDown(button, PlayerIndex.One) ||
                    IsButtonDown(button, PlayerIndex.Two) ||
                    IsButtonDown(button, PlayerIndex.Three) ||
                    IsButtonDown(button, PlayerIndex.Four));
            }
        }

        /// <summary>
        /// ボタンが離されているか
        /// </summary>
        /// <param name="button">ボタン</param>
        /// <returns>離されているかどうか</returns>
        public bool IsButtonUp(Buttons button)
        {
            return IsButtonUp(button, null);
        }

        /// <summary>
        /// ボタンが離されているか
        /// </summary>
        /// <param name="button">ボタン</param>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>離されているかどうか</returns>
        public bool IsButtonUp(Buttons button, PlayerIndex? playerIndex)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return CurrentPadState[(int)playerIndex.Value].IsButtonUp(button);
            }
            else
            {
                return (IsButtonUp(button, PlayerIndex.One) ||
                    IsButtonUp(button, PlayerIndex.Two) ||
                    IsButtonUp(button, PlayerIndex.Three) ||
                    IsButtonUp(button, PlayerIndex.Four));
            }
        }

        /// <summary>
        /// 前回更新時にボタンが押されていたか
        /// </summary>
        /// <param name="button">ボタン</param>
        /// <returns>押されていたかどうか</returns>
        public bool IsLastButtonDown(Buttons button)
        {
            return IsLastButtonDown(button, null);
        }

        /// <summary>
        /// 前回更新時にボタンが押されていたか
        /// </summary>
        /// <param name="button">ボタン</param>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>押されていたかどうか</returns>
        public bool IsLastButtonDown(Buttons button, PlayerIndex? playerIndex)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return LastPadState[(int)playerIndex.Value].IsButtonDown(button);
            }
            else
            {
                return (IsLastButtonDown(button, PlayerIndex.One) ||
                    IsLastButtonDown(button, PlayerIndex.Two) ||
                    IsLastButtonDown(button, PlayerIndex.Three) ||
                    IsLastButtonDown(button, PlayerIndex.Four));
            }
        }

        /// <summary>
        /// 前回更新時にボタンが離されていたか
        /// </summary>
        /// <param name="button">ボタン</param>
        /// <returns>離されていたかどうか</returns>
        public bool IsLastButtonUp(Buttons button)
        {
            return IsLastButtonUp(button, null);
        }

        /// <summary>
        /// 前回更新時にボタンが離されていたか
        /// </summary>
        /// <param name="button">ボタン</param>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>離されていたかどうか</returns>
        public bool IsLastButtonUp(Buttons button, PlayerIndex? playerIndex)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return LastPadState[(int)playerIndex.Value].IsButtonUp(button);
            }
            else
            {
                return (IsLastButtonUp(button, PlayerIndex.One) ||
                    IsLastButtonUp(button, PlayerIndex.Two) ||
                    IsLastButtonUp(button, PlayerIndex.Three) ||
                    IsLastButtonUp(button, PlayerIndex.Four));
            }
        }

        /// <summary>
        /// 現在のフレームでボタンが押されたか
        /// </summary>
        /// <param name="button">ボタン</param>
        /// <returns>押されているかどうか</returns>
        public bool IsJustButtonDown(Buttons button)
        {
            return IsJustButtonDown(button, null);
        }

        /// <summary>
        /// 現在のフレームでボタンが押されたか
        /// </summary>
        /// <param name="button">ボタン</param>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>押されているかどうか</returns>
        public bool IsJustButtonDown(Buttons button, PlayerIndex? playerIndex)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (IsLastButtonUp(button, playerIndex.Value) && IsButtonDown(button, playerIndex.Value));
            }
            else
            {
                return (IsJustButtonDown(button, PlayerIndex.One) ||
                    IsJustButtonDown(button, PlayerIndex.Two) ||
                    IsJustButtonDown(button, PlayerIndex.Three) ||
                    IsJustButtonDown(button, PlayerIndex.Four));
            }
        }

        /// <summary>
        /// 現在のフレームでボタンが離されたか
        /// </summary>
        /// <param name="button">ボタン</param>
        /// <returns>離されているかどうか</returns>
        public bool IsJustButtonUp(Buttons button)
        {
            return IsJustButtonUp(button, null);
        }

        /// <summary>
        /// 現在のフレームでボタンが離されたか
        /// </summary>
        /// <param name="button">ボタン</param>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>離されているかどうか</returns>
        public bool IsJustButtonUp(Buttons button, PlayerIndex? playerIndex)
        {
            if (!playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return IsLastButtonDown(button, playerIndex.Value) && IsButtonUp(button, playerIndex.Value);
            }
            else
            {
                return (IsJustButtonUp(button, PlayerIndex.One) ||
                    IsJustButtonUp(button, PlayerIndex.Two) ||
                    IsJustButtonUp(button, PlayerIndex.Three) ||
                    IsJustButtonUp(button, PlayerIndex.Four));
            }
        }

        #endregion

        #region Triggers

        #region Left Trigger

        #region Get State

        /// <summary>
        /// 現在のフレームの左トリガーの入力を取得する
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス</param>
        /// <returns>左トリガーの値</returns>
        public float GetCurrentLeftTrigger(PlayerIndex playerIndex)
        {
            if (!IsConnected(playerIndex))
            {
                return 0.0f;
            }

            return CurrentPadState[(int)playerIndex].Triggers.Left;
        }

        /// <summary>
        /// 前回更新時の左トリガーの入力を取得する
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス</param>
        /// <returns>左トリガーの値</returns>
        public float GetLastLeftTrigger(PlayerIndex playerIndex)
        {
            if (!IsConnected(playerIndex))
            {
                return 0.0f;
            }

            return LastPadState[(int)playerIndex].Triggers.Left;
        }

        #endregion

        /// <summary>
        /// 左トリガーボタンが押されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsLeftTriggerDown(PlayerIndex? playerIndex)
        {
            return IsLeftTriggerDown(playerIndex, TriggerDeadZone);
        }

        /// <summary>
        /// 左トリガーボタンが押されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">必要な入力量(0.0f～1.0f)</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsLeftTriggerDown(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetCurrentLeftTrigger(playerIndex.Value) >= value);
            }
            else
            {
                return (IsLeftTriggerDown(PlayerIndex.One, value) ||
                    IsLeftTriggerDown(PlayerIndex.Two, value) ||
                    IsLeftTriggerDown(PlayerIndex.Three, value) ||
                    IsLeftTriggerDown(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 前回更新時に左トリガーボタンが押されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastLeftTriggerDown(PlayerIndex? playerIndex)
        {
            return IsLastLeftTriggerDown(playerIndex, TriggerDeadZone);
        }

        /// <summary>
        /// 前回更新時に左トリガーボタンが押されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">必要な入力量(0.0f～1.0f)</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastLeftTriggerDown(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetLastLeftTrigger(playerIndex.Value) >= value);
            }
            else
            {
                return (IsLastLeftTriggerDown(PlayerIndex.One, value) ||
                    IsLastLeftTriggerDown(PlayerIndex.Two, value) ||
                    IsLastLeftTriggerDown(PlayerIndex.Three, value) ||
                    IsLastLeftTriggerDown(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 現在のフレームで左トリガーボタンが押されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsJustLeftTriggerDown(PlayerIndex? playerIndex)
        {
            return IsJustLeftTriggerDown(playerIndex, TriggerDeadZone);
        }

        /// <summary>
        /// 現在のフレームで左トリガーボタンが押されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">必要な入力量(0.0f～1.0f)</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsJustLeftTriggerDown(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (!IsLastLeftTriggerDown(playerIndex, value) && IsLeftTriggerDown(playerIndex, value));
            }
            else
            {
                return (IsJustLeftTriggerDown(PlayerIndex.One, value) ||
                    IsJustLeftTriggerDown(PlayerIndex.Two, value) ||
                    IsJustLeftTriggerDown(PlayerIndex.Three, value) ||
                    IsJustLeftTriggerDown(PlayerIndex.Four, value));
            }
        }

        #endregion

        #region Right Trigger

        #region Get State

        /// <summary>
        /// 現在のフレームの右トリガーの入力を取得する
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス</param>
        /// <returns>右トリガーの値</returns>
        public float GetCurrentRightTrigger(PlayerIndex playerIndex)
        {
            if (!IsConnected(playerIndex))
            {
                return 0.0f;
            }

            return CurrentPadState[(int)playerIndex].Triggers.Right;
        }

        /// <summary>
        /// 前回更新時の右トリガーの入力を取得する
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス</param>
        /// <returns>右トリガーの値</returns>
        public float GetLastRightTrigger(PlayerIndex playerIndex)
        {
            if (!IsConnected(playerIndex))
            {
                return 0.0f;
            }

            return LastPadState[(int)playerIndex].Triggers.Right;
        }

        #endregion

        /// <summary>
        /// 右トリガーボタンが押されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsRightTriggerDown(PlayerIndex? playerIndex)
        {
            return IsRightTriggerDown(playerIndex, TriggerDeadZone);
        }

        /// <summary>
        /// 右トリガーボタンが押されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">必要な入力量(0.0f～1.0f)</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsRightTriggerDown(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetCurrentRightTrigger(playerIndex.Value) >= value);
            }
            else
            {
                return (IsRightTriggerDown(PlayerIndex.One, value) ||
                    IsRightTriggerDown(PlayerIndex.Two, value) ||
                    IsRightTriggerDown(PlayerIndex.Three, value) ||
                    IsRightTriggerDown(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 前回更新時に右トリガーボタンが押されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastRightTriggerDown(PlayerIndex? playerIndex)
        {
            return IsLastRightTriggerDown(playerIndex, TriggerDeadZone);
        }

        /// <summary>
        /// 前回更新時に右トリガーボタンが押されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">必要な入力量(0.0f～1.0f)</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastRightTriggerDown(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetLastRightTrigger(playerIndex.Value) >= value);
            }
            else
            {
                return (IsLastRightTriggerDown(PlayerIndex.One, value) ||
                    IsLastRightTriggerDown(PlayerIndex.Two, value) ||
                    IsLastRightTriggerDown(PlayerIndex.Three, value) ||
                    IsLastRightTriggerDown(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 現在のフレームで右トリガーボタンが押されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsJustRightTriggerDown(PlayerIndex? playerIndex)
        {
            return IsJustRightTriggerDown(playerIndex, TriggerDeadZone);
        }

        /// <summary>
        /// 現在のフレームで右トリガーボタンが押されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">必要な入力量(0.0f～1.0f)</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsJustRightTriggerDown(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (!IsLastRightTriggerDown(playerIndex, value) && IsRightTriggerDown(playerIndex, value));
            }
            else
            {
                return (IsJustRightTriggerDown(PlayerIndex.One, value) ||
                    IsJustRightTriggerDown(PlayerIndex.Two, value) ||
                    IsJustRightTriggerDown(PlayerIndex.Three, value) ||
                    IsJustRightTriggerDown(PlayerIndex.Four, value));
            }
        }

        #endregion

        #endregion

        #region ThumbSticks

        #region Left ThumbStick

        #region Get State

        /// <summary>
        /// 現在のフレームの左のアナログスティックを入力を取得する
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス</param>
        /// <returns>左スティックの入力値</returns>
        public Vector2 GetCurrentThumbStickLeft(PlayerIndex playerIndex)
        {
            if (!IsConnected(playerIndex))
            {
                return Vector2.Zero;
            }

            return CurrentPadState[(int)playerIndex].ThumbSticks.Left;
        }

        /// <summary>
        /// 前回更新時の左のアナログスティックを入力を取得する
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス</param>
        /// <returns>左スティックの入力値</returns>
        public Vector2 GetLastThumbStickLeft(PlayerIndex playerIndex)
        {
            if (!IsConnected(playerIndex))
            {
                return Vector2.Zero;
            }

            return LastPadState[(int)playerIndex].ThumbSticks.Left;
        }

        #endregion

        #region Left Direction

        /// <summary>
        /// 左スティックが左方向に倒されているか
        /// </summary>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickLeftInputLeft()
        {
            return IsThumbStickLeftInputLeft(null, StickDeadZone);
        }

        /// <summary>
        /// 左スティックが左方向に倒されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickLeftInputLeft(PlayerIndex? playerIndex)
        {
            return IsThumbStickLeftInputLeft(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 左スティックが左方向に倒されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">左方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickLeftInputLeft(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetCurrentThumbStickLeft(playerIndex.Value).X <= -value);
            }
            else
            {
                return (IsThumbStickLeftInputLeft(PlayerIndex.One, value) ||
                    IsThumbStickLeftInputLeft(PlayerIndex.Two, value) ||
                    IsThumbStickLeftInputLeft(PlayerIndex.Three, value) ||
                    IsThumbStickLeftInputLeft(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 前回更新時に左スティックが左方向に倒されていたか
        /// </summary>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickLeftInputLeft()
        {
            return IsLastThumbStickLeftInputLeft(null, StickDeadZone);
        }

        /// <summary>
        /// 前回更新時に左スティックが左方向に倒されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickLeftInputLeft(PlayerIndex? playerIndex)
        {
            return IsLastThumbStickLeftInputLeft(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 前回更新時に左スティックが左方向に倒されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">左方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickLeftInputLeft(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetLastThumbStickLeft(playerIndex.Value).X <= -value);
            }
            else
            {
                return (IsLastThumbStickLeftInputLeft(PlayerIndex.One, value) ||
                    IsLastThumbStickLeftInputLeft(PlayerIndex.Two, value) ||
                    IsLastThumbStickLeftInputLeft(PlayerIndex.Three, value) ||
                    IsLastThumbStickLeftInputLeft(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 現在のフレームで左スティックが左方向に倒されたか
        /// </summary>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickLeftInputLeft()
        {
            return IsJustThumbStickLeftInputLeft(null, StickDeadZone);
        }

        /// <summary>
        /// 現在のフレームで左スティックが左方向に倒されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickLeftInputLeft(PlayerIndex? playerIndex)
        {
            return IsJustThumbStickLeftInputLeft(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 現在のフレームで左スティックが左方向に倒されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">左方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickLeftInputLeft(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (!IsLastThumbStickLeftInputLeft(playerIndex, value) && IsThumbStickLeftInputLeft(playerIndex, value));
            }
            else
            {
                return (IsJustThumbStickLeftInputLeft(PlayerIndex.One, value) ||
                    IsJustThumbStickLeftInputLeft(PlayerIndex.Two, value) ||
                    IsJustThumbStickLeftInputLeft(PlayerIndex.Three, value) ||
                    IsJustThumbStickLeftInputLeft(PlayerIndex.Four, value));
            }
        }

        #endregion

        #region Right Direction

        /// <summary>
        /// 左スティックが右方向に倒されているか
        /// </summary>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickLeftInputRight()
        {
            return IsThumbStickLeftInputRight(null, StickDeadZone);
        }

        /// <summary>
        /// 左スティックが右方向に倒されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickLeftInputRight(PlayerIndex? playerIndex)
        {
            return IsThumbStickLeftInputRight(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 左スティックが右方向に倒されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">右方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickLeftInputRight(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetCurrentThumbStickLeft(playerIndex.Value).X >= value);
            }
            else
            {
                return (IsThumbStickLeftInputRight(PlayerIndex.One, value) ||
                    IsThumbStickLeftInputRight(PlayerIndex.Two, value) ||
                    IsThumbStickLeftInputRight(PlayerIndex.Three, value) ||
                    IsThumbStickLeftInputRight(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 前回更新時に左スティックが右方向に倒されていたか
        /// </summary>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickLeftInputRight()
        {
            return IsLastThumbStickLeftInputRight(null, StickDeadZone);
        }

        /// <summary>
        /// 前回更新時に左スティックが右方向に倒されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickLeftInputRight(PlayerIndex? playerIndex)
        {
            return IsLastThumbStickLeftInputRight(playerIndex, StickDeadZone);
        }
        /// <summary>
        /// 前回更新時に左スティックが右方向に倒されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">右方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickLeftInputRight(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetLastThumbStickLeft(playerIndex.Value).X >= value);
            }
            else
            {
                return (IsLastThumbStickLeftInputRight(PlayerIndex.One, value) ||
                    IsLastThumbStickLeftInputRight(PlayerIndex.Two, value) ||
                    IsLastThumbStickLeftInputRight(PlayerIndex.Three, value) ||
                    IsLastThumbStickLeftInputRight(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 現在のフレームで左スティックが右方向に倒されたか
        /// </summary>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickLeftInputRight()
        {
            return IsJustThumbStickLeftInputRight(null, StickDeadZone);
        }

        /// <summary>
        /// 現在のフレームで左スティックが右方向に倒されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickLeftInputRight(PlayerIndex? playerIndex)
        {
            return IsJustThumbStickLeftInputRight(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 現在のフレームで左スティックが右方向に倒されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">右方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickLeftInputRight(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (!IsLastThumbStickLeftInputRight(playerIndex, value) && IsThumbStickLeftInputRight(playerIndex, value));
            }
            else
            {
                return (IsJustThumbStickLeftInputRight(PlayerIndex.One, value) ||
                    IsJustThumbStickLeftInputRight(PlayerIndex.Two, value) ||
                    IsJustThumbStickLeftInputRight(PlayerIndex.Three, value) ||
                    IsJustThumbStickLeftInputRight(PlayerIndex.Four, value));
            }
        }

        #endregion

        #region Up Direction

        /// <summary>
        /// 左スティックが上方向に倒されているか
        /// </summary>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickLeftInputUp()
        {
            return IsThumbStickLeftInputUp(null, StickDeadZone);
        }

        /// <summary>
        /// 左スティックが上方向に倒されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickLeftInputUp(PlayerIndex? playerIndex)
        {
            return IsThumbStickLeftInputUp(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 左スティックが上方向に倒されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">上方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickLeftInputUp(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetCurrentThumbStickLeft(playerIndex.Value).Y >= value);
            }
            else
            {
                return (IsThumbStickLeftInputUp(PlayerIndex.One, value) ||
                    IsThumbStickLeftInputUp(PlayerIndex.Two, value) ||
                    IsThumbStickLeftInputUp(PlayerIndex.Three, value) ||
                    IsThumbStickLeftInputUp(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 前回更新時に左スティックが上方向に倒されていたか
        /// </summary>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickLeftInputUp()
        {
            return IsLastThumbStickLeftInputUp(null, StickDeadZone);
        }

        /// <summary>
        /// 前回更新時に左スティックが上方向に倒されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickLeftInputUp(PlayerIndex? playerIndex)
        {
            return IsLastThumbStickLeftInputUp(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 前回更新時に左スティックが上方向に倒されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">上方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickLeftInputUp(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetLastThumbStickLeft(playerIndex.Value).Y >= value);
            }
            else
            {
                return (IsLastThumbStickLeftInputUp(PlayerIndex.One, value) ||
                    IsLastThumbStickLeftInputUp(PlayerIndex.Two, value) ||
                    IsLastThumbStickLeftInputUp(PlayerIndex.Three, value) ||
                    IsLastThumbStickLeftInputUp(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 現在のフレームで左スティックが上方向に倒されたか
        /// </summary>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickLeftInputUp()
        {
            return IsJustThumbStickLeftInputUp(null, StickDeadZone);
        }

        /// <summary>
        /// 現在のフレームで左スティックが上方向に倒されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickLeftInputUp(PlayerIndex? playerIndex)
        {
            return IsJustThumbStickLeftInputUp(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 現在のフレームで左スティックが上方向に倒されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">上方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickLeftInputUp(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (!IsLastThumbStickLeftInputUp(playerIndex, value) && IsThumbStickLeftInputUp(playerIndex, value));
            }
            else
            {
                return (IsJustThumbStickLeftInputUp(PlayerIndex.One, value) ||
                    IsJustThumbStickLeftInputUp(PlayerIndex.Two, value) ||
                    IsJustThumbStickLeftInputUp(PlayerIndex.Three, value) ||
                    IsJustThumbStickLeftInputUp(PlayerIndex.Four, value));
            }
        }

        #endregion

        #region Down Direction

        /// <summary>
        /// 左スティックが下方向に倒されているか
        /// </summary>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickLeftInputDown()
        {
            return IsThumbStickLeftInputDown(null, StickDeadZone);
        }

        /// <summary>
        /// 左スティックが下方向に倒されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickLeftInputDown(PlayerIndex? playerIndex)
        {
            return IsThumbStickLeftInputDown(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 左スティックが下方向に倒されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">下方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickLeftInputDown(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetCurrentThumbStickLeft(playerIndex.Value).Y <= -value);
            }
            else
            {
                return (IsThumbStickLeftInputDown(PlayerIndex.One, value) ||
                    IsThumbStickLeftInputDown(PlayerIndex.Two, value) ||
                    IsThumbStickLeftInputDown(PlayerIndex.Three, value) ||
                    IsThumbStickLeftInputDown(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 前回更新時に左スティックが下方向に倒されていたか
        /// </summary>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickLeftInputDown()
        {
            return IsLastThumbStickLeftInputDown(null, StickDeadZone);
        }

        /// <summary>
        /// 前回更新時に左スティックが下方向に倒されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickLeftInputDown(PlayerIndex? playerIndex)
        {
            return IsLastThumbStickLeftInputDown(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 前回更新時に左スティックが下方向に倒されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">下方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickLeftInputDown(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetLastThumbStickLeft(playerIndex.Value).Y <= -value);
            }
            else
            {
                return (IsLastThumbStickLeftInputDown(PlayerIndex.One, value) ||
                    IsLastThumbStickLeftInputDown(PlayerIndex.Two, value) ||
                    IsLastThumbStickLeftInputDown(PlayerIndex.Three, value) ||
                    IsLastThumbStickLeftInputDown(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 現在のフレームで左スティックが下方向に倒されたか
        /// </summary>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickLeftInputDown()
        {
            return IsJustThumbStickLeftInputDown(null, StickDeadZone);
        }

        /// <summary>
        /// 現在のフレームで左スティックが下方向に倒されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickLeftInputDown(PlayerIndex? playerIndex)
        {
            return IsJustThumbStickLeftInputDown(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 現在のフレームで左スティックが下方向に倒されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">下方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickLeftInputDown(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (!IsLastThumbStickLeftInputDown(playerIndex, value) && IsThumbStickLeftInputDown(playerIndex, value));
            }
            else
            {
                return (IsJustThumbStickLeftInputDown(PlayerIndex.One, value) ||
                    IsJustThumbStickLeftInputDown(PlayerIndex.Two, value) ||
                    IsJustThumbStickLeftInputDown(PlayerIndex.Three, value) ||
                    IsJustThumbStickLeftInputDown(PlayerIndex.Four, value));
            }
        }

        #endregion

        #endregion

        #region Right ThumbStick

        #region Get State

        /// <summary>
        /// 現在のフレームの右のアナログスティックを入力値を取得する
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス</param>
        /// <returns>右スティックの入力値</returns>
        public Vector2 GetCurrentThumbStickRight(PlayerIndex playerIndex)
        {
            if (!IsConnected(playerIndex))
            {
                return Vector2.Zero;
            }

            return CurrentPadState[(int)playerIndex].ThumbSticks.Right;
        }

        /// <summary>
        /// 前回更新時の右のアナログスティックを入力値を取得する
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス</param>
        /// <returns>右スティックの入力値</returns>
        public Vector2 GetLastThumbStickRight(PlayerIndex playerIndex)
        {
            if (!IsConnected(playerIndex))
            {
                return Vector2.Zero;
            }

            return LastPadState[(int)playerIndex].ThumbSticks.Right;
        }

        #endregion

        #region Left Direction

        /// <summary>
        /// 右スティックが左方向に倒されているか
        /// </summary>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickRightInputLeft()
        {
            return IsThumbStickRightInputLeft(null, StickDeadZone);
        }

        /// <summary>
        /// 右スティックが左方向に倒されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickRightInputLeft(PlayerIndex? playerIndex)
        {
            return IsThumbStickRightInputLeft(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 右スティックが左方向に倒されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">左方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickRightInputLeft(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetCurrentThumbStickRight(playerIndex.Value).X <= -value);
            }
            else
            {
                return (IsThumbStickRightInputLeft(PlayerIndex.One, value) ||
                    IsThumbStickRightInputLeft(PlayerIndex.Two, value) ||
                    IsThumbStickRightInputLeft(PlayerIndex.Three, value) ||
                    IsThumbStickRightInputLeft(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 前回更新時に右スティックが左方向に倒されていたか
        /// </summary>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickRightInputLeft()
        {
            return IsLastThumbStickRightInputLeft(null, StickDeadZone);
        }

        /// <summary>
        /// 前回更新時に右スティックが左方向に倒されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickRightInputLeft(PlayerIndex? playerIndex)
        {
            return IsLastThumbStickRightInputLeft(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 前回更新時に右スティックが左方向に倒されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">左方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickRightInputLeft(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetLastThumbStickRight(playerIndex.Value).X <= -value);
            }
            else
            {
                return (IsLastThumbStickRightInputLeft(PlayerIndex.One, value) ||
                    IsLastThumbStickRightInputLeft(PlayerIndex.Two, value) ||
                    IsLastThumbStickRightInputLeft(PlayerIndex.Three, value) ||
                    IsLastThumbStickRightInputLeft(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 現在のフレームで右スティックが左方向に倒されたか
        /// </summary>
        /// <returns>入力されたかどうか</returns>
        public bool IsJustThumbStickRightInputLeft()
        {
            return IsJustThumbStickRightInputLeft(null, StickDeadZone);
        }

        /// <summary>
        /// 現在のフレームで右スティックが左方向に倒されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力されたかどうか</returns>
        public bool IsJustThumbStickRightInputLeft(PlayerIndex? playerIndex)
        {
            return IsJustThumbStickRightInputLeft(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 現在のフレームで右スティックが左方向に倒されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">左方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力されたかどうか</returns>
        public bool IsJustThumbStickRightInputLeft(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (!IsLastThumbStickRightInputLeft(playerIndex, value) && IsThumbStickRightInputLeft(playerIndex, value));
            }
            else
            {
                return (IsJustThumbStickRightInputLeft(PlayerIndex.One, value) ||
                    IsJustThumbStickRightInputLeft(PlayerIndex.Two, value) ||
                    IsJustThumbStickRightInputLeft(PlayerIndex.Three, value) ||
                    IsJustThumbStickRightInputLeft(PlayerIndex.Four, value));
            }
        }

        #endregion

        #region Right Direction

        /// <summary>
        /// 右スティックが右方向に倒されているか
        /// </summary>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickRightInputRight()
        {
            return IsThumbStickRightInputRight(null, StickDeadZone);
        }

        /// <summary>
        /// 右スティックが右方向に倒されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickRightInputRight(PlayerIndex? playerIndex)
        {
            return IsThumbStickRightInputRight(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 右スティックが右方向に倒されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">右方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickRightInputRight(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetCurrentThumbStickRight(playerIndex.Value).X >= value);
            }
            else
            {
                return (IsThumbStickRightInputRight(PlayerIndex.One, value) ||
                    IsThumbStickRightInputRight(PlayerIndex.Two, value) ||
                    IsThumbStickRightInputRight(PlayerIndex.Three, value) ||
                    IsThumbStickRightInputRight(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 前回更新時に右スティックが右方向に倒されていたか
        /// </summary>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickRightInputRight()
        {
            return IsLastThumbStickRightInputRight(null, StickDeadZone);
        }

        /// <summary>
        /// 前回更新時に右スティックが右方向に倒されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickRightInputRight(PlayerIndex? playerIndex)
        {
            return IsLastThumbStickRightInputRight(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 前回更新時に右スティックが右方向に倒されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">右方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickRightInputRight(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetLastThumbStickRight(playerIndex.Value).X >= value);
            }
            else
            {
                return (IsLastThumbStickRightInputRight(PlayerIndex.One, value) ||
                    IsLastThumbStickRightInputRight(PlayerIndex.Two, value) ||
                    IsLastThumbStickRightInputRight(PlayerIndex.Three, value) ||
                    IsLastThumbStickRightInputRight(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 現在のフレームで右スティックが右方向に倒されたか
        /// </summary>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickRightInputRight()
        {
            return IsJustThumbStickRightInputRight(null, StickDeadZone);
        }

        /// <summary>
        /// 現在のフレームで右スティックが右方向に倒されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickRightInputRight(PlayerIndex? playerIndex)
        {
            return IsJustThumbStickRightInputRight(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 現在のフレームで右スティックが右方向に倒されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">右方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickRightInputRight(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (!IsLastThumbStickRightInputRight(playerIndex, value) && IsThumbStickRightInputRight(playerIndex, value));
            }
            else
            {
                return (IsJustThumbStickRightInputRight(PlayerIndex.One, value) ||
                    IsJustThumbStickRightInputRight(PlayerIndex.Two, value) ||
                    IsJustThumbStickRightInputRight(PlayerIndex.Three, value) ||
                    IsJustThumbStickRightInputRight(PlayerIndex.Four, value));
            }
        }

        #endregion

        #region Up Direction

        /// <summary>
        /// 右スティックが上方向に倒されているか
        /// </summary>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickRightInputUp()
        {
            return IsThumbStickRightInputUp(null, StickDeadZone);
        }

        /// <summary>
        /// 右スティックが上方向に倒されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickRightInputUp(PlayerIndex? playerIndex)
        {
            return IsThumbStickRightInputUp(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 右スティックが上方向に倒されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">上方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickRightInputUp(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetCurrentThumbStickRight(playerIndex.Value).Y >= value);
            }
            else
            {
                return (IsThumbStickRightInputUp(PlayerIndex.One, value) ||
                    IsThumbStickRightInputUp(PlayerIndex.Two, value) ||
                    IsThumbStickRightInputUp(PlayerIndex.Three, value) ||
                    IsThumbStickRightInputUp(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 前回更新時に右スティックが上方向に倒されていたか
        /// </summary>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickRightInputUp()
        {
            return IsLastThumbStickRightInputUp(null, StickDeadZone);
        }

        /// <summary>
        /// 前回更新時に右スティックが上方向に倒されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickRightInputUp(PlayerIndex? playerIndex)
        {
            return IsLastThumbStickRightInputUp(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 前回更新時に右スティックが上方向に倒されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">上方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickRightInputUp(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetLastThumbStickRight(playerIndex.Value).Y >= value);
            }
            else
            {
                return (IsLastThumbStickRightInputUp(PlayerIndex.One, value) ||
                    IsLastThumbStickRightInputUp(PlayerIndex.Two, value) ||
                    IsLastThumbStickRightInputUp(PlayerIndex.Three, value) ||
                    IsLastThumbStickRightInputUp(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 現在のフレームで右スティックが上方向に倒されたか
        /// </summary>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickRightInputUp()
        {
            return IsJustThumbStickRightInputUp(null, StickDeadZone);
        }

        /// <summary>
        /// 現在のフレームで右スティックが上方向に倒されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickRightInputUp(PlayerIndex? playerIndex)
        {
            return IsJustThumbStickRightInputUp(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 現在のフレームで右スティックが上方向に倒されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">上方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力されたどうか</returns>
        public bool IsJustThumbStickRightInputUp(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (!IsLastThumbStickRightInputUp(playerIndex, value) && IsThumbStickRightInputUp(playerIndex, value));
            }
            else
            {
                return (IsJustThumbStickRightInputUp(PlayerIndex.One, value) ||
                    IsJustThumbStickRightInputUp(PlayerIndex.Two, value) ||
                    IsJustThumbStickRightInputUp(PlayerIndex.Three, value) ||
                    IsJustThumbStickRightInputUp(PlayerIndex.Four, value));
            }
        }

        #endregion

        #region Down Direction

        /// <summary>
        /// 右スティックが下方向に倒されているか
        /// </summary>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickRightInputDown()
        {
            return IsThumbStickRightInputDown(null, StickDeadZone);
        }

        /// <summary>
        /// 右スティックが下方向に倒されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickRightInputDown(PlayerIndex? playerIndex)
        {
            return IsThumbStickRightInputDown(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 右スティックが下方向に倒されているか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">下方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsThumbStickRightInputDown(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetCurrentThumbStickRight(playerIndex.Value).Y <= -value);
            }
            else
            {
                return (IsThumbStickRightInputDown(PlayerIndex.One, value) ||
                    IsThumbStickRightInputDown(PlayerIndex.Two, value) ||
                    IsThumbStickRightInputDown(PlayerIndex.Three, value) ||
                    IsThumbStickRightInputDown(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 前回更新時に右スティックが下方向に倒されていたか
        /// </summary>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickRightInputDown()
        {
            return IsLastThumbStickRightInputDown(null, StickDeadZone);
        }

        /// <summary>
        /// 前回更新時に右スティックが下方向に倒されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickRightInputDown(PlayerIndex? playerIndex)
        {
            return IsLastThumbStickRightInputDown(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 前回更新時に右スティックが下方向に倒されていたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">下方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力があったかどうか</returns>
        public bool IsLastThumbStickRightInputDown(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (GetLastThumbStickRight(playerIndex.Value).Y <= -value);
            }
            else
            {
                return (IsLastThumbStickRightInputDown(PlayerIndex.One, value) ||
                    IsLastThumbStickRightInputDown(PlayerIndex.Two, value) ||
                    IsLastThumbStickRightInputDown(PlayerIndex.Three, value) ||
                    IsLastThumbStickRightInputDown(PlayerIndex.Four, value));
            }
        }

        /// <summary>
        /// 現在のフレームで右スティックが下方向に倒されたか
        /// </summary>
        /// <returns>入力されたかどうか</returns>
        public bool IsJustThumbStickRightInputDown()
        {
            return IsJustThumbStickRightInputDown(null, StickDeadZone);
        }

        /// <summary>
        /// 現在のフレームで右スティックが下方向に倒されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>入力されたかどうか</returns>
        public bool IsJustThumbStickRightInputDown(PlayerIndex? playerIndex)
        {
            return IsJustThumbStickRightInputDown(playerIndex, StickDeadZone);
        }

        /// <summary>
        /// 現在のフレームで右スティックが下方向に倒されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <param name="value">下方向への必要な入力量(-1.0f(反対方向)～1.0f)</param>
        /// <returns>入力されたかどうか</returns>
        public bool IsJustThumbStickRightInputDown(PlayerIndex? playerIndex, float value)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (!IsLastThumbStickRightInputDown(playerIndex, value) && IsThumbStickRightInputDown(playerIndex, value));
            }
            else
            {
                return (IsJustThumbStickRightInputDown(PlayerIndex.One, value) ||
                    IsJustThumbStickRightInputDown(PlayerIndex.Two, value) ||
                    IsJustThumbStickRightInputDown(PlayerIndex.Three, value) ||
                    IsJustThumbStickRightInputDown(PlayerIndex.Four, value));
            }
        }

        #endregion

        #endregion

        #endregion

        #region Vibration

        /// <summary>
        /// コントローラの振動を設定する
        /// </summary>
        /// <param name="leftMotor">左モーターの強さ</param>
        /// <param name="rightMotor">右モーターの強さ</param>
        /// <param name="playerIndex">プレイヤーインデックス</param>
        /// <returns>設定できたかどうか</returns>
        public bool SetVibration(float leftMotor, float rightMotor, PlayerIndex playerIndex)
        {
            if (IsConnected(playerIndex))
            {
                return GamePad.SetVibration(playerIndex, leftMotor, rightMotor);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// コントローラの振動をストップする
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス</param>
        /// <returns>設定できたかどうか</returns>
        public bool StopVibration(PlayerIndex playerIndex)
        {
            return SetVibration(0.0f, 0.0f, playerIndex);
        }

        #endregion

        #region ChatPad

        /// <summary>
        /// チャットパッドのキーが押されているか
        /// </summary>
        /// <param name="key">状態を取得したいキー</param>
        /// <returns>押されているかどうか</returns>
        public bool IsChatPadKeyDown(Keys key)
        {
            return IsChatPadKeyDown(key, null);
        }

        /// <summary>
        /// チャットパッドのキーが押されているか
        /// </summary>
        /// <param name="key">状態を取得したいキー</param>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>押されているかどうか</returns>
        public bool IsChatPadKeyDown(Keys key, PlayerIndex? playerIndex)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return CurrentChatPadState[(int)playerIndex.Value].IsKeyDown(key);
            }
            else
            {
                return (IsChatPadKeyDown(key, PlayerIndex.One) ||
                    IsChatPadKeyDown(key, PlayerIndex.Two) ||
                    IsChatPadKeyDown(key, PlayerIndex.Three) ||
                    IsChatPadKeyDown(key, PlayerIndex.Four));
            }
        }

        /// <summary>
        /// チャットパッドのキーが離されているか
        /// </summary>
        /// <param name="key">状態を取得したいキー</param>
        /// <returns>離されているかどうか</returns>
        public bool IsChatPadKeyUp(Keys key)
        {
            return IsChatPadKeyUp(key, null);
        }

        /// <summary>
        /// チャットパッドのキーが離されているか
        /// </summary>
        /// <param name="key">状態を取得したいキー</param>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>離されているかどうか</returns>
        public bool IsChatPadKeyUp(Keys key, PlayerIndex? playerIndex)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return CurrentChatPadState[(int)playerIndex.Value].IsKeyUp(key);
            }
            else
            {
                return (IsChatPadKeyUp(key, PlayerIndex.One) ||
                    IsChatPadKeyUp(key, PlayerIndex.Two) ||
                    IsChatPadKeyUp(key, PlayerIndex.Three) ||
                    IsChatPadKeyUp(key, PlayerIndex.Four));
            }
        }

        /// <summary>
        /// 前回更新時にチャットパッドのキーが押されていたか
        /// </summary>
        /// <param name="key">状態を取得したいキー</param>
        /// <returns>押されていたかどうか</returns>
        public bool IsLastChatPadKeyDown(Keys key)
        {
            return IsLastChatPadKeyDown(key, null);
        }

        /// <summary>
        /// 前回更新時にチャットパッドのキーが押されていたか
        /// </summary>
        /// <param name="key">状態を取得したいキー</param>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>押されていたかどうか</returns>
        public bool IsLastChatPadKeyDown(Keys key, PlayerIndex? playerIndex)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return LastChatPadState[(int)playerIndex.Value].IsKeyDown(key);
            }
            else
            {
                return (IsLastChatPadKeyDown(key, PlayerIndex.One) ||
                    IsLastChatPadKeyDown(key, PlayerIndex.Two) ||
                    IsLastChatPadKeyDown(key, PlayerIndex.Three) ||
                    IsLastChatPadKeyDown(key, PlayerIndex.Four));
            }
        }

        /// <summary>
        /// 前回更新時にチャットパッドのキーが離されていたか
        /// </summary>
        /// <param name="key">状態を取得したいキー</param>
        /// <returns>離されていたかどうか</returns>
        public bool IsLastChatPadKeyUp(Keys key)
        {
            return IsLastChatPadKeyUp(key, null);
        }

        /// <summary>
        /// 前回更新時にチャットパッドのキーが離されていたか
        /// </summary>
        /// <param name="key">状態を取得したいキー</param>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>離されていたかどうか</returns>
        public bool IsLastChatPadKeyUp(Keys key, PlayerIndex? playerIndex)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return LastChatPadState[(int)playerIndex.Value].IsKeyUp(key);
            }
            else
            {
                return (IsLastChatPadKeyUp(key, PlayerIndex.One) ||
                    IsLastChatPadKeyUp(key, PlayerIndex.Two) ||
                    IsLastChatPadKeyUp(key, PlayerIndex.Three) ||
                    IsLastChatPadKeyUp(key, PlayerIndex.Four));
            }
        }

        /// <summary>
        /// チャットパッドのキーがこのフレームで押されたか
        /// </summary>
        /// <param name="key">判断するキー</param>
        /// <returns>押されたかどうか</returns>
        public bool IsChatPadJustKeyDown(Keys key)
        {
            return IsChatPadJustKeyDown(key, null);
        }

        /// <summary>
        /// チャットパッドのキーがこのフレームで押されたか
        /// </summary>
        /// <param name="key">判断するキー</param>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>押されたかどうか</returns>
        public bool IsChatPadJustKeyDown(Keys key, PlayerIndex? playerIndex)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (IsLastChatPadKeyUp(key, playerIndex.Value) && IsChatPadKeyDown(key, playerIndex.Value));
            }
            else
            {
                return (IsChatPadJustKeyDown(key, PlayerIndex.One) ||
                    IsChatPadJustKeyDown(key, PlayerIndex.Two) ||
                    IsChatPadJustKeyDown(key, PlayerIndex.Three) ||
                    IsChatPadJustKeyDown(key, PlayerIndex.Four));
            }
        }

        /// <summary>
        /// キーがこのフレームで離されたか
        /// </summary>
        /// <param name="key">判断するキー</param>
        /// <returns>離されたかどうか</returns>
        public bool IsChatPadJustKeyUp(Keys key)
        {
            return IsChatPadJustKeyUp(key, null);
        }

        /// <summary>
        /// キーがこのフレームで離されたか
        /// </summary>
        /// <param name="key">判断するキー</param>
        /// <param name="playerIndex">プレイヤーインデックス（ null で全プレイヤー）</param>
        /// <returns>離されたかどうか</returns>
        public bool IsChatPadJustKeyUp(Keys key, PlayerIndex? playerIndex)
        {
            if (playerIndex.HasValue)
            {
                if (!IsConnected(playerIndex.Value))
                {
                    return false;
                }

                return (IsLastChatPadKeyDown(key, playerIndex.Value) && IsChatPadKeyUp(key, playerIndex.Value));
            }
            else
            {
                return (IsChatPadJustKeyUp(key, PlayerIndex.One) ||
                    IsChatPadJustKeyUp(key, PlayerIndex.Two) ||
                    IsChatPadJustKeyUp(key, PlayerIndex.Three) ||
                    IsChatPadJustKeyUp(key, PlayerIndex.Four));
            }
        }

        #endregion

        #endregion

        #region Combination Inputs

        #region Direction Inputs

        /// <summary>
        /// 現在のフレームで左方向が入力されたか
        /// </summary>
        /// <returns>入力があるかどうか</returns>
        public bool IsJustInputLeft()
        {
            return IsJustInputLeft(null);
        }

        /// <summary>
        /// 現在のフレームで左方向が入力されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsJustInputLeft(PlayerIndex? playerIndex)
        {
            return (IsJustKeyDown(Keys.Left) ||
                IsJustButtonDown(Buttons.DPadLeft, playerIndex) ||
                IsJustThumbStickLeftInputLeft(playerIndex));
        }

        /// <summary>
        /// 現在のフレームで右方向が入力されたか
        /// </summary>
        /// <returns>入力があるかどうか</returns>
        public bool IsJustInputRight()
        {
            return IsJustInputRight(null);
        }

        /// <summary>
        /// 現在のフレームで右方向が入力されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsJustInputRight(PlayerIndex? playerIndex)
        {
            return (IsJustKeyDown(Keys.Right) ||
                IsJustButtonDown(Buttons.DPadRight, playerIndex) ||
                IsJustThumbStickLeftInputRight(playerIndex));
        }

        /// <summary>
        /// 現在のフレームで上方向が入力されたか
        /// </summary>
        /// <returns>入力があるかどうか</returns>
        public bool IsJustInputUp()
        {
            return IsJustInputUp(null);
        }

        /// <summary>
        /// 現在のフレームで上方向が入力されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsJustInputUp(PlayerIndex? playerIndex)
        {
            return (IsJustKeyDown(Keys.Up) ||
                IsJustButtonDown(Buttons.DPadUp, playerIndex) ||
                IsJustThumbStickLeftInputUp(playerIndex));
        }

        /// <summary>
        /// 現在のフレームで下方向が入力されたか
        /// </summary>
        /// <returns>入力があるかどうか</returns>
        public bool IsJustInputDown()
        {
            return IsJustInputDown(null);
        }

        /// <summary>
        /// 現在のフレームで下方向が入力されたか
        /// </summary>
        /// <param name="playerIndex">プレイヤーインデックス</param>
        /// <returns>入力があるかどうか</returns>
        public bool IsJustInputDown(PlayerIndex? playerIndex)
        {
            return (IsJustKeyDown(Keys.Down) ||
                IsJustButtonDown(Buttons.DPadDown, playerIndex) ||
                IsJustThumbStickLeftInputDown(playerIndex));
        }

        #endregion

        #endregion
    }
}

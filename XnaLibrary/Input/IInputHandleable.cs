﻿#region File Description
//-----------------------------------------------------------------------------
// IInputHandleable.cs
// 
// Update: 2014/04/24
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
#endregion

namespace XnaLibrary.Input
{
    /// <summary>
    /// 入力の処理に対応するためのメソッドを定義
    /// </summary>
    public interface IInputHandleable
    {
        /// <summary>
        /// 入力を処理
        /// </summary>
        /// <param name="gameTime">ゲームタイム</param>
        /// <param name="inputManager">インプットマネージャー</param>
        void HandleInput(GameTime gameTime, InputManager inputManager);
    }
}

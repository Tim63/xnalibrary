﻿#region File Description
//-----------------------------------------------------------------------------
// Program.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace CompileEffect
{
    /// <summary>
    /// コマンド ライン ユーティリティーによって、
    /// .fx エフェクト ソース ファイルを XNA Game Studio で
    /// 使用可能なバイナリ ファイルにコンパイルします。 
    /// </summary>
    class Program
    {
        static int Main(string[] args)
        {
            // 正しい数のコマンド ライン引数があることを確認します。
            if (args.Length != 4)
            {
                Console.Error.WriteLine("Usage: CompileEffect <targetPlatform> <targetProfile> <input.fx> <output_name>");
                return 1;
            }

            // コマンド ライン引数を解析します。
            TargetPlatform targetPlatform;

            if (!Enum.TryParse(args[0], true, out targetPlatform))
            {
                Console.Error.WriteLine("Invalid target platform {0}. Valid options are {1}.", args[0], GetEnumValues<TargetPlatform>());
                return 1;
            }

            GraphicsProfile targetProfile;

            if (!Enum.TryParse(args[1], true, out targetProfile))
            {
                Console.Error.WriteLine("Invalid target profile {0}. Valid options are {1}.", args[1], GetEnumValues<GraphicsProfile>());
                return 1;
            }

            string inputFilename = args[2];
            string outputFilename = args[3];

            try
            {
                Console.WriteLine("Compiling {0} -> {1} for {2}, {3}", Path.GetFileName(inputFilename), outputFilename, targetPlatform, targetProfile);

                ContentBuildLogger logger = new CustomLogger();

                // エフェクト ソース コードをインポートします。
                EffectImporter importer = new EffectImporter();
                ContentImporterContext importerContext = new CustomImporterContext(logger);
                EffectContent sourceEffect = importer.Import(inputFilename, importerContext);

                // エフェクトをコンパイルします。
                EffectProcessor processor = new EffectProcessor();
                ContentProcessorContext processorContext = new CustomProcessorContext(targetPlatform, targetProfile, logger);
                CompiledEffectContent compiledEffect = processor.Process(sourceEffect, processorContext);

                // コンパイルされたエフェクト コードを書き出します。
                File.WriteAllBytes(outputFilename + ".bin", compiledEffect.GetEffectCode());
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error: {0}", e.Message);
                return 1;
            }

            return 0;
        }


        /// <summary>
        /// ヘルパーが、列挙型のすべての有効値を、コンマで区切られた
        /// リストで返します。
        /// </summary>
        static string GetEnumValues<T>()
        {
            T[] values = (T[])Enum.GetValues(typeof(T));

            return string.Join(", ", from t in values select t.ToString());
        }
    }
}

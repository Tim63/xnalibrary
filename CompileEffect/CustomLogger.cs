﻿#region File Description
//-----------------------------------------------------------------------------
// CustomLogger.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework.Content.Pipeline;
#endregion

namespace CompileEffect
{
    /// <summary>
    /// Content Pipeline の出力メッセージをキャプチャーするための
    /// カスタム ロガー クラス。この実装は、単純にメッセージを
    /// コンソールに出力し、警告があれば例外をスローします。
    /// </summary>
    class CustomLogger : ContentBuildLogger
    {
        /// <summary>
        /// 優先度の低いメッセージをログに記録します。
        /// </summary>
        public override void LogMessage(string message, params object[] messageArgs)
        {
            Console.WriteLine(message, messageArgs);
        }


        /// <summary>
        /// 優先度の高いメッセージをログに記録します。
        /// </summary>
        public override void LogImportantMessage(string message, params object[] messageArgs)
        {
            Console.WriteLine(message, messageArgs);
        }


        /// <summary>
        /// 警告メッセージをログに記録します。
        /// </summary>
        public override void LogWarning(string helpLink, ContentIdentity contentIdentity, string message, params object[] messageArgs)
        {
            throw new Exception("Warning: " + string.Format(message, messageArgs));
        }
    }
}

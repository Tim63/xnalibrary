﻿#region File Description
//-----------------------------------------------------------------------------
// CustomProcessorContext.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace CompileEffect
{
    /// <summary>
    /// Content Pipeline のプロセッサを実行するための
    /// カスタム コンテキスト オブジェクト。
    /// </summary>
    class CustomProcessorContext : ContentProcessorContext
    {
        /// <summary>
        /// コンストラクター。
        /// </summary>
        public CustomProcessorContext(TargetPlatform targetPlatform, GraphicsProfile targetProfile, ContentBuildLogger logger)
        {
            this.targetPlatform = targetPlatform;
            this.targetProfile = targetProfile;
            this.logger = logger;
        }


        /// <summary>
        /// ビルドのターゲット プラットフォームを取得します。
        /// </summary>
        public override TargetPlatform TargetPlatform
        {
            get { return targetPlatform; } 
        }

        TargetPlatform targetPlatform;


        /// <summary>
        /// ビルドのターゲット プロファイルを取得します。
        /// </summary>
        public override GraphicsProfile TargetProfile
        {
            get { return targetProfile; } 
        }

        GraphicsProfile targetProfile;


        /// <summary>
        /// コンテンツ ビルドのメッセージと警告を報告するための
        /// ロガーを取得します。
        /// </summary>
        public override ContentBuildLogger Logger
        {
            get { return logger; }
        }

        ContentBuildLogger logger;


        /// <summary>
        /// 現在のビルド構成 (Debug、Release、など) を取得します。
        /// </summary>
        public override string BuildConfiguration
        {
            get { return string.Empty; } 
        }


        /// <summary>
        /// 現在のコンテンツ ビルドの中間ディレクトリを取得します。
        /// </summary>
        public override string IntermediateDirectory
        {
            get { return string.Empty; } 
        }


        /// <summary>
        /// 現在のコンテンツ ビルドの最終出力ディレクトリを取得します。
        /// </summary>
        public override string OutputDirectory
        {
            get { return string.Empty; } 
        }


        /// <summary>
        /// 現在ビルドしているアセットの最終出力ファイル名を取得します。
        /// </summary>
        public override string OutputFilename
        {
            get { return string.Empty; } 
        }


        /// <summary>
        /// 辞書を使用して、カスタム パラメーター データをプロセッサに
        /// 渡すことができます。
        /// </summary>
        public override OpaqueDataDictionary Parameters
        {
            get { return parameters; } 
        }

        OpaqueDataDictionary parameters = new OpaqueDataDictionary();


        /// <summary>
        /// プロセッサが読み取った追加の入力ファイルをすべて記録します。
        /// </summary>
        public override void AddDependency(string filename)
        { 
        }


        /// <summary>
        /// プロセッサが書き込んだ追加の出力ファイルをすべて記録します。
        /// </summary>
        public override void AddOutputFile(string filename)
        { 
        }


        /// <summary>
        /// 異なるプロセッサを使用するように Content Pipeline に要求します。
        /// その際、メモリー内のオブジェクトを異なるフォーマットに変換します。
        /// </summary>
        public override TOutput Convert<TInput, TOutput>(TInput input, string processorName, OpaqueDataDictionary processorParameters)
        {
            throw new NotImplementedException(); 
        }


        /// <summary>
        /// 異なるインポーターとプロセッサを使用するように Content Pipeline に
        /// 要求します。その際、ソース アセット ファイルから読み取り、
        /// メモリー内のオブジェクトを返します。
        /// </summary>
        public override TOutput BuildAndLoadAsset<TInput, TOutput>(ExternalReference<TInput> sourceAsset, string processorName, OpaqueDataDictionary processorParameters, string importerName)
        {
            throw new NotImplementedException(); 
        }


        /// <summary>
        /// 異なるインポーターとプロセッサを使用するように Content Pipeline に
        /// 要求します。その際、ソース アセット ファイルを .xnb フォーマットで
        /// ビルドし、出力される .xnb への参照を返します。
        /// </summary>
        public override ExternalReference<TOutput> BuildAsset<TInput, TOutput>(ExternalReference<TInput> sourceAsset, string processorName, OpaqueDataDictionary processorParameters, string importerName, string assetName)
        {
            throw new NotImplementedException(); 
        }
    }
}

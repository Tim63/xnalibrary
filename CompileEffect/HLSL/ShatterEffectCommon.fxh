﻿//-----------------------------------------------------------------------------
// ShatterEffectCommon.fxh
//
// Update: 2013/09/24
//
// Orignal File:
//     ShatterEffect.fx
//     http://xbox.create.msdn.com/ja-JP/education/catalog/sample/shatter
//     Microsoft XNA Community Game Platform
//     Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------

#ifndef SHATTER_EFFECT_COMMON_H
#define SHATTER_EFFECT_COMMON_H

float RotationAmount;
float TranslationAmount;
float Time;
float GroundHeight;
float Gravity = 250;

float4x4 ViewProj;

// YawPitchRoll 行列を作成するためのヘルパー関数
// プロセッサで生成されたランダムな回転値で頂点を回転させるために使用されます。
float4x4 CreateYawPitchRollMatrix(float x, float y, float z)
{
	float4x4 result;
		
	result[0][0] =  cos(z) * cos(y) + sin(z) * sin(x) * sin(y);
	result[0][1] = -sin(z) * cos(y) + cos(z) * sin(x) * sin(y);
	result[0][2] =  cos(x) * sin(y);
	result[0][3] =  0;
	
	result[1][0] =  sin(z) * cos(x);
	result[1][1] =  cos(z) * cos(x);
	result[1][2] = -sin(x);
	result[1][3] =  0;
	
	result[2][0] = cos(z) * -sin(y) + sin(z) * sin(x) * cos(y);
	result[2][1] = sin(z) *  sin(y) + cos(z) * sin(x) * cos(y);
	result[2][2] = cos(x) *  cos(y);
	result[2][3] = 0;
	
	result[3][0] = 0;
	result[3][1] = 0;
	result[3][2] = 0;
	result[3][3] = 1;    

	return result;
}

struct ShatterCommonVSOutput
{
	float4 Position;
	float4 RawPosition;
	float3 WorldPosition;
	float3 WorldNormal;
};

// 粉砕計算
//
// 粉砕エフェクトは比較的単純です。まず、プロセッサで生成したランダム値に基づいて 
// YawPitchRoll 行列を作成します。次に、この回転行列を使用してトライアングルの中心を
// 軸に回転するように頂点の位置をトランスフォームします。その後、頂点をその法線に沿って、
// 外側に向かって移動させます。最後に、時間の 2 乗の関数で頂点をその Y 軸に沿って落下させ、
// 落下エフェクトを生み出します。
ShatterCommonVSOutput ComputeShatter(float3 position, float3 normal, float3 rotationalVelocity, float3 triangleCenter)
{
	ShatterCommonVSOutput output = (ShatterCommonVSOutput)0;

	// 回転行列を作成します
	rotationalVelocity *= RotationAmount;
	float4x4 rotMatrix  = CreateYawPitchRollMatrix(rotationalVelocity.x, 
												   rotationalVelocity.y, 
												   rotationalVelocity.z);
	
	// 頂点のトライアングルの中心点を軸にして頂点を回転させます
	position = triangleCenter + mul(position - triangleCenter, rotMatrix);
	
	// その法線に沿って頂点を移動させます
	position += normal * TranslationAmount;    
	
	// 時間の 2 乗の関数で頂点を下向きに移動させ、適切な落下のエフェクトを生み出します。
	position.y -= Time * Time * Gravity;                 
	
	position = mul(float4(position, 1.0f), World);
	if (position.y < GroundHeight)
	{
		position.y = GroundHeight;
	}

	output.RawPosition   = float4(mul(position, WorldInverseTranspose), 1.0f);
	output.Position      = mul(float4(position, 1.0f), ViewProj);
	output.WorldPosition = position;
	// ライティングを正確に計算するために、法線も回転させる必要があります。
	output.WorldNormal  = normalize(mul(mul(normal, rotMatrix), WorldInverseTranspose));

	return output;
}

#endif
﻿//-----------------------------------------------------------------------------
// Lighting.fxh
// 
// Update: 2013/10/21
//-----------------------------------------------------------------------------

#ifndef LIGHTNIG_H
#define LIGHTNIG_H

float4 DiffuseColor;
float3 EmissiveColor;
float3 SpecularColor;
float  SpecularPower;

float3 EyePosition;

float3 Light0Direction;
float3 Light0DiffuseColor;
float3 Light0SpecularColor;

float3 Light1Direction;
float3 Light1DiffuseColor;
float3 Light1SpecularColor;

float3 Light2Direction;
float3 Light2DiffuseColor;
float3 Light2SpecularColor;

struct ColorPair
{
	float3 Diffuse;
	float3 Specular;
};

ColorPair ComputeLights(float3 eyeVector, float3 worldNormal)
{
	float3x3 lightDirections = float3x3(Light0Direction, Light1Direction, Light2Direction);
	float3x3 lightDiffuses	 = float3x3(Light0DiffuseColor, Light1DiffuseColor, Light2DiffuseColor);
	float3x3 lightSpeculars  = float3x3(Light0SpecularColor, Light1SpecularColor, Light2SpecularColor);
	float3x3 halfVectors	 = float3x3(normalize(eyeVector - lightDirections[0]),
										normalize(eyeVector - lightDirections[1]),
										normalize(eyeVector - lightDirections[2]));

	float3 dotL = mul(-lightDirections, worldNormal);
	float3 dotH = mul(halfVectors, worldNormal);
	
	float3 zeroL = step(0, dotL);

	float3 diffuse  = zeroL * dotL;
	float3 specular = pow(max(dotH, 0) * zeroL, SpecularPower);

	ColorPair result;
	
	result.Diffuse  = mul(diffuse,  lightDiffuses) * DiffuseColor.rgb + EmissiveColor;
	result.Specular = mul(specular, lightSpeculars) * SpecularColor;

	return result;
}

#endif
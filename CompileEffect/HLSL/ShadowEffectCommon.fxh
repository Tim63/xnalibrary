//-----------------------------------------------------------------------------
// ShadowEffectCommon.fxh
// 
// Update: 2014/04/09
//-----------------------------------------------------------------------------

#ifndef SHADOW_EFFECT_COMMON_H
#define SHADOW_EFFECT_COMMON_H

float4x4 LightViewProj;

float DepthBias = 0.001f;

int ShadowMapSize;

float ShadowBrightness = 0.3f;

texture ShadowMap;
sampler ShadowMapSampler : register(s1) = sampler_state
{
	Texture = <ShadowMap>;
	MipFilter = POINT;
	MinFilter = POINT;
	MagFilter = POINT;
	AddressU = Clamp;
	AddressV = Clamp;
};

float4 PSCreateShadowMap(VSOutputCreateShadowMap input) : COLOR0
{
	float4 output = float4(1.0f, 1.0f, 1.0f, 1.0f);

	output.x = input.ShadowTexCoord.z / input.ShadowTexCoord.w;

	return output;
}

float ComputeShadow(float4 lightPosition)
{
	float2 shadowTexCoord = lightPosition.xy / lightPosition.w * float2(0.5f, -0.5f) + 0.5f;
	float depth = lightPosition.z / lightPosition.w - DepthBias;

	float shadow = 0;
	float invShadowMapSize = 1.0f / ShadowMapSize;
	[unroll]
	for (float y = -1.5f; y <= 1.5f; y += 1.0f)
	{
		for (float x = -1.5f; x <= 1.5f; x += 1.0f)
		{
			shadow += (tex2D(ShadowMapSampler, shadowTexCoord + float2(x, y) * invShadowMapSize).x < depth) ? ShadowBrightness : 1.0f;
		}
	}

	shadow *= 0.0625f;

	return shadow;
}

float4 PSDrawWithShadow(VSOutputDrawWithShadow input) : COLOR0
{
	float4 color = tex2D(TextureSampler, input.TexCoord) * input.Diffuse;

	float3 eyeVector = normalize(EyePosition - input.WorldPosition);

	ColorPair lightResult = ComputeLights(eyeVector, input.WorldNormal);

	color.rgb *= lightResult.Diffuse;
	color.rgb += lightResult.Specular * color.a;

	color.rgb *= ComputeShadow(input.LightPosition);
	color.rgb = lerp(color.rgb, FogColor * color.a, input.FogFactor);

	return color;
}

float4 PSNormalDraw(VSOutputNormalDraw input) : COLOR0
{
	float4 color = tex2D(TextureSampler, input.TexCoord) * input.Diffuse;

	float3 eyeVector = normalize(EyePosition - input.WorldPosition);

	ColorPair lightResult = ComputeLights(eyeVector, input.WorldNormal);

	color.rgb *= lightResult.Diffuse;
	color.rgb += lightResult.Specular * color.a;
	color.rgb = lerp(color.rgb, FogColor * color.a, input.FogFactor);

	return color;
}

#endif
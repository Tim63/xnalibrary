﻿//-----------------------------------------------------------------------------
// Structures.fxh
// 
// Update: 2013/09/19
//-----------------------------------------------------------------------------

#ifndef STRUCTURES_H
#define STRUCTURES_H

// Vertex Shader Inputs

struct VSInputDraw
{
	float4 Position : POSITION0;
	float3 Normal   : NORMAL0;
	float2 TexCoord : TEXCOORD0;
};

struct VSInputDrawSkinned
{
	float4 Position    : POSITION0;
	float3 Normal      : NORMAL0;
	float2 TexCoord    : TEXCOORD0;
	int4   BoneIndices : BLENDINDICES0;
	float4 BoneWeights : BLENDWEIGHT0;
};

struct VSInputDrawShatter
{
	float4 Position           : POSITION0;
	float3 Normal             : NORMAL0;
	float2 TexCoord           : TEXCOORD0;
	float3 TriangleCenter     : TEXCOORD1;
	float3 RotationalVelocity : TEXCOORD2;
};

// Vertex Shader Outputs

struct VSOutputDrawWithShadow
{
	float4 Position      : POSITION0;
	float2 TexCoord      : TEXCOORD0;
	float4 LightPosition : TEXCOORD1;
	float3 WorldPosition : TEXCOORD2;
	float3 WorldNormal   : TEXCOORD3;
	float  FogFactor     : TEXCOORD4;
	float4 Diffuse       : COLOR0;
};

struct VSOutputNormalDraw
{
	float4 Position      : POSITION0;
	float2 TexCoord      : TEXCOORD0;
	float3 WorldPosition : TEXCOORD1;
	float3 WorldNormal   : TEXCOORD2;
	float  FogFactor     : TEXCOORD3;
	float4 Diffuse       : COLOR0;
};

// Create ShadowMap Inputs, Output

struct VSInputCreateShadowMap
{
	float4 Position : POSITION0;
};

struct VSInputCreateShadowMapSkinned
{
	float4 Position    : POSITION0;
	float4 BoneIndices : BLENDINDICES0;
	float4 BoneWeights : BLENDWEIGHT0;
};

struct VSInputCreateShadowMapShatter
{
	float4 Position           : POSITION0;
	float3 Normal             : NORMAL0;
	float3 TriangleCenter     : TEXCOORD1;
	float3 RotationalVelocity : TEXCOORD2;
};

struct VSOutputCreateShadowMap
{
	float4 Position       : POSITION0;
	float4 ShadowTexCoord : TEXCOORD0;
};

#endif
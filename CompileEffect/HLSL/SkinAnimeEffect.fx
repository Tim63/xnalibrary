﻿//-----------------------------------------------------------------------------
// SkinAnimeEffect.fx
// 
// Update: 2014/04/24
//-----------------------------------------------------------------------------

#define MaxBones 81

#include "Structures.fxh"
#include "Lighting.fxh"
#include "Common.fxh"
#include "SkinAnimeEffectCommon.fxh"

VSOutputNormalDraw VSNormalDraw(VSInputDrawSkinned input) 
{
	VSOutputNormalDraw output = (VSOutputNormalDraw)0;    

	float4x3 skinTransform = CreateSkinTransform(input.BoneIndices, input.BoneWeights);
	float4 skinPosition    = float4(mul(input.Position, skinTransform), 1.0f);

	output.Position        = mul(skinPosition, WorldViewProj);
	output.WorldPosition   = mul(skinPosition, World).xyz;
	output.WorldNormal     = normalize(mul(mul(input.Normal, (float3x3)skinTransform), WorldInverseTranspose));
	output.FogFactor       = saturate(dot(skinPosition, FogVector));

	output.Diffuse         = float4(1.0f, 1.0f, 1.0f, DiffuseColor.a);
	output.TexCoord        = input.TexCoord;

	return output;
}

float4 PSNormalDraw(VSOutputNormalDraw input) : COLOR0
{
	float4 color = tex2D(TextureSampler, input.TexCoord) * input.Diffuse;

	float3 eyeVector = normalize(EyePosition - input.WorldPosition);

	ColorPair lightResult = ComputeLights(eyeVector, input.WorldNormal);

	color.rgb *= lightResult.Diffuse;
	color.rgb += lightResult.Specular * color.a;
	color.rgb = lerp(color.rgb, FogColor * color.a, input.FogFactor);

	return color;
}

technique NormalDraw
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 VSNormalDraw();
		PixelShader  = compile ps_3_0 PSNormalDraw();
	}
}

﻿//-----------------------------------------------------------------------------
// Common.fxh
// 
// Update: 2013/08/12
//-----------------------------------------------------------------------------

#ifndef COMMON_H
#define COMMON_H

float4x4 World;
float4x4 WorldViewProj;
float3x3 WorldInverseTranspose;

float4 FogVector;
float3 FogColor;

texture Texture;
sampler TextureSampler : register(s0) = sampler_state
{
	Texture = <Texture>;
};

#endif
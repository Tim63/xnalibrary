﻿//-----------------------------------------------------------------------------
// ShatterEffect.fx
//
// Update: 2013/08/12
//
// Orignal File:
//     http://xbox.create.msdn.com/ja-JP/education/catalog/sample/shatter
//     Microsoft XNA Community Game Platform
//     Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------

#include "Structures.fxh"
#include "Lighting.fxh"
#include "Common.fxh"
#include "ShatterEffectCommon.fxh"
#include "ShadowEffectCommon.fxh"

VSOutputCreateShadowMap VSCreateShadowMap(VSInputDrawShatter input)
{
	VSOutputCreateShadowMap output = (VSOutputCreateShadowMap)0;

	ShatterCommonVSOutput shatter = ComputeShatter(input.Position, input.Normal, input.RotationalVelocity, input.TriangleCenter);

	output.Position       = mul(float4(shatter.WorldPosition, 1.0f), LightViewProj);
	output.ShadowTexCoord = output.Position;

	return output;
}

VSOutputDrawWithShadow VSDrawWithShadow(VSInputDrawShatter input)
{
	VSOutputDrawWithShadow output = (VSOutputDrawWithShadow)0;

	ShatterCommonVSOutput shatter = ComputeShatter(input.Position, input.Normal, input.RotationalVelocity, input.TriangleCenter);

	output.Position      = shatter.Position;
	output.WorldPosition = shatter.WorldPosition;
	output.WorldNormal   = shatter.WorldNormal;
	output.FogFactor     = saturate(dot(shatter.RawPosition, FogVector));

	output.Diffuse       = float4(1.0f, 1.0f, 1.0f, DiffuseColor.a);
	output.TexCoord      = input.TexCoord;
	output.LightPosition = mul(float4(shatter.WorldPosition, 1.0f), LightViewProj);

	return output;
}

VSOutputNormalDraw VSNormalDraw(VSInputDrawShatter input)
{
	VSOutputNormalDraw output = (VSOutputNormalDraw)0;    
	
	ShatterCommonVSOutput shatter = ComputeShatter(input.Position, input.Normal, input.RotationalVelocity, input.TriangleCenter);

	output.Position      = shatter.Position;
	output.WorldPosition = shatter.WorldPosition;
	output.WorldNormal   = shatter.WorldNormal;
	output.FogFactor     = saturate(dot(shatter.RawPosition, FogVector));

	output.Diffuse       = float4(1.0f, 1.0f, 1.0f, DiffuseColor.a);
	output.TexCoord      = input.TexCoord;
	
	return output;
}

technique CreateShadowMap
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 VSCreateShadowMap();
		PixelShader  = compile ps_3_0 PSCreateShadowMap();
	}
}

technique DrawWithShadow
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 VSDrawWithShadow();
		PixelShader  = compile ps_3_0 PSDrawWithShadow();
	}
}

technique NormalDraw
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 VSNormalDraw();
		PixelShader  = compile ps_3_0 PSNormalDraw();
	}
}

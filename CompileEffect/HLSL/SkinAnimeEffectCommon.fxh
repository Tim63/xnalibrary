﻿//-----------------------------------------------------------------------------
// SkinAnimeEffectCommon.fxh
// 
// Update: 2014/04/24
//-----------------------------------------------------------------------------

#ifndef SKINNED_EFFECT_COMMON
#define SKINNED_EFFECT_COMMON

#ifndef MaxBones
#define MaxBones 72
#endif

float4x3 Bones[MaxBones];

float4x3 CreateSkinTransform(float4 indices, float4 weights)
{
	float4x3 skinTransform = 0;

	skinTransform += Bones[indices.x] * weights.x;
	skinTransform += Bones[indices.y] * weights.y;
	skinTransform += Bones[indices.z] * weights.z;
	skinTransform += Bones[indices.w] * weights.w;

	return skinTransform;
}

#endif
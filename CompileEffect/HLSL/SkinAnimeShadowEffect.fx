//-----------------------------------------------------------------------------
// SkinAnimeShadowEffect.fx
// 
// Update: 2014/04/24
//-----------------------------------------------------------------------------

#define MaxBones 79

#include "Structures.fxh"
#include "Lighting.fxh"
#include "Common.fxh"
#include "SkinAnimeEffectCommon.fxh"
#include "ShadowEffectCommon.fxh"

VSOutputCreateShadowMap VSCreateShadowMap(VSInputCreateShadowMapSkinned input)
{
	VSOutputCreateShadowMap output = (VSOutputCreateShadowMap)0;

	float4x3 skinTransform = CreateSkinTransform(input.BoneIndices, input.BoneWeights);
	float4 skinPosition    = float4(mul(input.Position, skinTransform), 1.0f);

	output.Position        = mul(mul(skinPosition, World), LightViewProj);
	output.ShadowTexCoord  = output.Position;

	return output;
}

VSOutputDrawWithShadow VSDrawWithShadow(VSInputDrawSkinned input)
{
	VSOutputDrawWithShadow output = (VSOutputDrawWithShadow)0;

	float4x3 skinTransform = CreateSkinTransform(input.BoneIndices, input.BoneWeights);
	float4 skinPosition    = float4(mul(input.Position, skinTransform), 1.0f);
	float4 worldPosition   = mul(skinPosition, World);

	output.Position        = mul(skinPosition, WorldViewProj);
	output.WorldPosition   = worldPosition.xyz;
	output.WorldNormal     = normalize(mul(mul(input.Normal, (float3x3)skinTransform), WorldInverseTranspose));
	output.FogFactor       = saturate(dot(skinPosition, FogVector));

	output.Diffuse         = float4(1.0f, 1.0f, 1.0f, DiffuseColor.a);
	output.TexCoord        = input.TexCoord;
	output.LightPosition   = mul(worldPosition, LightViewProj);

	return output;
}

VSOutputNormalDraw VSNormalDraw(VSInputDrawSkinned input) 
{
	VSOutputNormalDraw output = (VSOutputNormalDraw)0;    

	float4x3 skinTransform = CreateSkinTransform(input.BoneIndices, input.BoneWeights);
	float4 skinPosition    = float4(mul(input.Position, skinTransform), 1.0f);

	output.Position        = mul(skinPosition, WorldViewProj);
	output.WorldPosition   = mul(skinPosition, World).xyz;
	output.WorldNormal     = normalize(mul(mul(input.Normal, (float3x3)skinTransform), WorldInverseTranspose));
	output.FogFactor       = saturate(dot(skinPosition, FogVector));

	output.Diffuse         = float4(1.0f, 1.0f, 1.0f, DiffuseColor.a);
	output.TexCoord        = input.TexCoord;

	return output;
}

technique CreateShadowMap
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 VSCreateShadowMap();
		PixelShader  = compile ps_3_0 PSCreateShadowMap();
	}
}

technique DrawWithShadow
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 VSDrawWithShadow();
		PixelShader  = compile ps_3_0 PSDrawWithShadow();
	}
}

technique NormalDraw
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 VSNormalDraw();
		PixelShader  = compile ps_3_0 PSNormalDraw();
	}
}

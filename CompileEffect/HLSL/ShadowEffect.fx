//-----------------------------------------------------------------------------
// ShadowEffect.fx
// 
// Update: 2013/10/21
//-----------------------------------------------------------------------------

#include "Structures.fxh"
#include "Lighting.fxh"
#include "Common.fxh"
#include "ShadowEffectCommon.fxh"

VSOutputCreateShadowMap VSCreateShadowMap(VSInputCreateShadowMap input)
{
	VSOutputCreateShadowMap output = (VSOutputCreateShadowMap)0;

	output.Position       = mul(mul(input.Position, World), LightViewProj);
	output.ShadowTexCoord = output.Position;

	return output;
}

VSOutputDrawWithShadow VSDrawWithShadow(VSInputDraw input)
{
	VSOutputDrawWithShadow output = (VSOutputDrawWithShadow)0;

	float4 worldPosition = mul(input.Position, World);
	output.Position      = mul(input.Position, WorldViewProj);
	output.WorldPosition = worldPosition.xyz;
	output.WorldNormal   = normalize(mul(input.Normal, WorldInverseTranspose));
	output.FogFactor     = saturate(dot(input.Position, FogVector));

	output.Diffuse       = float4(1.0f, 1.0f, 1.0f, DiffuseColor.a);
	output.TexCoord      = input.TexCoord;
	output.LightPosition = mul(worldPosition, LightViewProj);

	return output;
}

VSOutputNormalDraw VSNormalDraw(VSInputDraw input) 
{
	VSOutputNormalDraw output = (VSOutputNormalDraw)0;    

	output.Position      = mul(input.Position, WorldViewProj);
	output.WorldPosition = mul(input.Position, World).xyz;
	output.WorldNormal   = normalize(mul(input.Normal, WorldInverseTranspose));
	output.FogFactor     = saturate(dot(input.Position, FogVector));

	output.Diffuse       = float4(1.0f, 1.0f, 1.0f, DiffuseColor.a);
	output.TexCoord      = input.TexCoord;

	return output;
}

technique CreateShadowMap
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 VSCreateShadowMap();
		PixelShader  = compile ps_3_0 PSCreateShadowMap();
	}
}

technique DrawWithShadow
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 VSDrawWithShadow();
		PixelShader  = compile ps_3_0 PSDrawWithShadow();
	}
}

technique NormalDraw
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 VSNormalDraw();
		PixelShader  = compile ps_3_0 PSNormalDraw();
	}
}

﻿#region File Description
//-----------------------------------------------------------------------------
// CustomImporterContext.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework.Content.Pipeline;
#endregion

namespace CompileEffect
{
    /// <summary>
    /// Content Pipeline のインポーターを実行するための
    /// カスタム コンテキスト オブジェクト。
    /// </summary>
    class CustomImporterContext : ContentImporterContext
    {
        /// <summary>
        /// コンストラクター。
        /// </summary>
        public CustomImporterContext(ContentBuildLogger logger)
        {
            this.logger = logger;
        }


        /// <summary>
        /// コンテンツ ビルドのメッセージと警告を報告するための
        /// ロガーを取得します。
        /// </summary>
        public override ContentBuildLogger Logger
        {
            get { return logger; }
        }

        ContentBuildLogger logger;


        /// <summary>
        /// 現在のコンテンツ ビルドの中間ディレクトリを取得します。
        /// </summary>
        public override string IntermediateDirectory
        {
            get { return string.Empty; }
        }


        /// <summary>
        /// 現在のコンテンツ ビルドの最終出力ディレクトリを取得します。
        /// </summary>
        public override string OutputDirectory
        {
            get { return string.Empty; } 
        }


        /// <summary>
        /// インポーターが読み取った追加の入力ファイルをすべて記録します。
        /// </summary>
        public override void AddDependency(string filename)
        { 
        }
    }
}

﻿#region File Description
//-----------------------------------------------------------------------------
// ShadowModelProcessor.cs
//
// Update: 2014/04/24
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using XnaLibraryContentPipeline.MaterialContents;
#endregion

namespace XnaLibraryContentPipeline.ShadowEffectPipeline
{
    /// <summary>
    /// 影の描画可能なモデル用モデルプロセッサ
    /// </summary>
    [ContentProcessor(DisplayName = "Shadow Model Processor")]
    public class ShadowModelProcessor : ModelProcessor
    {
        /// <summary>
        /// マテリアルの変換
        /// </summary>
        /// <param name="material">マテリアルコンテント</param>
        /// <param name="context">コンテンツプロセッサコンテクスト</param>
        /// <returns>変換したマテリアル</returns>
        protected override MaterialContent ConvertMaterial(MaterialContent material, ContentProcessorContext context)
        {
            var convertedMaterial = new ShadowMaterialContent();

            // 現在のマテリアル内のテクスチャに対してループ処理を実行し、新しいマテリアルに追加します。
            foreach (var textureContent in material.Textures)
            {
                convertedMaterial.Textures.Add(textureContent.Key, textureContent.Value);
            }

            // 現在のマテリアル内の不透明データに対してループ処理を実行し、新しいマテリアルに追加します。
            foreach (var opaqueData in material.OpaqueData)
            {
                convertedMaterial.OpaqueData.Add(opaqueData.Key, opaqueData.Value);
            }

            return base.ConvertMaterial(convertedMaterial, context);
        }

        /// <summary>
        /// 規定のエフェクトを設定できないようにする
        /// </summary>
        [Browsable(false)]
        [DefaultValue(MaterialProcessorDefaultEffect.BasicEffect)]
        public override MaterialProcessorDefaultEffect DefaultEffect
        {
            get { return MaterialProcessorDefaultEffect.BasicEffect; }
        }
    }
}
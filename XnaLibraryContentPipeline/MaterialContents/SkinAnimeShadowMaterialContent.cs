﻿#region File Description
//-----------------------------------------------------------------------------
// SkinAnimeShadowMaterialContent.cs
// 
// Update: 2014/04/24
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
#endregion

namespace XnaLibraryContentPipeline.MaterialContents
{
    /// <summary>
    /// 影の描画と、スキンアニメーションに対応するマテリアルコンテント
    /// </summary>
    public class SkinAnimeShadowMaterialContent : MaterialContent
    {
        #region Constants

        /// <summary>
        /// 散乱色のキー
        /// </summary>
        public const string DiffuseColorKey = "DiffuseColor";

        /// <summary>
        /// 鏡面反射色のキー
        /// </summary>
        public const string SpecularColorKey = "SpecularColor";

        /// <summary>
        /// 放射色のキー
        /// </summary>
        public const string EmissiveColorKey = "EmissiveColor";

        /// <summary>
        /// 鏡面反射の強さのキー
        /// </summary>
        public const string SpecularPowerKey = "SpecularPower";

        /// <summary>
        /// アルファ値のキー
        /// </summary>
        public const string AlphaKey = "Alpha";

        /// <summary>
        /// テクスチャーのキー
        /// </summary>
        public const string TextureKey = "Texture";

        #endregion

        #region Properties

        /// <summary>
        /// 散乱色
        /// </summary>
        [ContentSerializerIgnore]
        public Vector3? DiffuseColor
        {
            get { return base.GetValueTypeProperty<Vector3>(DiffuseColorKey); }
            set { base.SetProperty<Vector3?>(DiffuseColorKey, value); }
        }

        /// <summary>
        /// 鏡面反射色
        /// </summary>
        [ContentSerializerIgnore]
        public Vector3? SpecularColor
        {
            get { return base.GetValueTypeProperty<Vector3>(SpecularColorKey); }
            set { base.SetProperty<Vector3?>(SpecularColorKey, value); }
        }

        /// <summary>
        /// 放射色
        /// </summary>
        [ContentSerializerIgnore]
        public Vector3? EmissiveColor
        {
            get { return base.GetValueTypeProperty<Vector3>(EmissiveColorKey); }
            set { base.SetProperty<Vector3?>(EmissiveColorKey, value); }
        }

        /// <summary>
        /// 鏡面反射の強さ
        /// </summary>
        [ContentSerializerIgnore]
        public float? SpecularPower
        {
            get { return base.GetValueTypeProperty<float>(SpecularPowerKey); }
            set { base.SetProperty<float?>(SpecularPowerKey, value); }
        }

        /// <summary>
        /// アルファ値
        /// </summary>
        [ContentSerializerIgnore]
        public float? Alpha
        {
            get { return base.GetValueTypeProperty<float>(AlphaKey); }
            set { base.SetProperty<float?>(AlphaKey, value); }
        }

        /// <summary>
        /// テクスチャー
        /// </summary>
        [ContentSerializerIgnore]
        public ExternalReference<TextureContent> Texture
        {
            get { return base.GetTexture(TextureKey); }
            set { base.SetTexture(TextureKey, value); }
        }

        #endregion
    }
}

﻿#region File Description
//-----------------------------------------------------------------------------
// CustomEffectModelProcessor.cs
//
// Update: 2014/04/24
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
#endregion

namespace XnaLibraryContentPipeline.CustomEffectPipeline
{
    /// <summary>
    /// 独自エフェクト指定可能なモデル用モデルプロセッサ
    /// </summary>
    [ContentProcessor(DisplayName = "CustomEffect Model Processor")]
    public class CustomEffectModelProcessor : ModelProcessor
    {
        /// <summary>
        /// カスタムエフェクト
        /// </summary>
        [DisplayName("独自のエフェクトファイル")]
        [Description("独自のエフェクトファイルのパス")]
        [DefaultValue("Effect.fx")]
        public string CustomEffect
        {
            get { return customEffect; }
            set { customEffect = value; }
        }
        string customEffect = "Effect.fx";

        /// <summary>
        /// マテリアルの変換
        /// </summary>
        /// <param name="material">マテリアルコンテント</param>
        /// <param name="context">コンテンツプロセッサコンテクスト</param>
        /// <returns>変換したマテリアル</returns>
        protected override MaterialContent ConvertMaterial(MaterialContent material, ContentProcessorContext context)
        {
            if (string.IsNullOrEmpty(customEffect))
            {
                throw new ArgumentException("エフェクトファイルが設定されていません");
            }

            var effectMaterial = new EffectMaterialContent();
            effectMaterial.Effect = new ExternalReference<EffectContent>(Path.GetFullPath(customEffect));

            foreach (var textureContent in material.Textures)
            {
                effectMaterial.Textures.Add(textureContent.Key, textureContent.Value);
            }

            foreach (var opaqueData in material.OpaqueData)
            {
                effectMaterial.OpaqueData.Add(opaqueData.Key, opaqueData.Value);
            }

            return base.ConvertMaterial(effectMaterial, context);
        }

        /// <summary>
        /// 規定のエフェクトを設定できないようにする
        /// </summary>
        [Browsable(false)]
        [DefaultValue(MaterialProcessorDefaultEffect.BasicEffect)]
        public override MaterialProcessorDefaultEffect DefaultEffect
        {
            get { return MaterialProcessorDefaultEffect.BasicEffect; }
        }
    }
}
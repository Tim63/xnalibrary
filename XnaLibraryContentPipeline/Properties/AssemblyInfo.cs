using System.Reflection;
using System.Runtime.InteropServices;

// アセンブリに関する全般的な情報は、以下の一連の属性によって管理されます。 
// アセンブリに関連付けられている情報を変更するには、これらの属性値を変更します。
// 
[assembly: AssemblyTitle("XnaLibraryContentPipelines")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("XnaLibraryContentPipelines")]
[assembly: AssemblyCopyright("Copyright ©  2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// ComVisible 属性を false に設定すると、その型はこのアセンブリ内でCOM コンポーネントから 
// 参照不可能になります。COM からこのアセンブリ内の型にアクセスする場合は、 
// その型のComVisible 属性を true に設定してください。
[assembly: ComVisible(false)]

// このプロジェクトが COM に公開されている場合、次の GUID が typelib の ID になります。
[assembly: Guid("827a9223-4468-4265-8f22-2ef5bce59786")]

// アセンブリのバージョン情報は、次の 4 つの値で構成されています。
//
//      メジャー バージョン
//      マイナー バージョン 
//      ビルド番号
//      リビジョン
//
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

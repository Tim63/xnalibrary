﻿#region File Description
//-----------------------------------------------------------------------------
// SkinAnimeMaterialProcessor.cs
//
// Update: 2014/04/24
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using XnaLibrary.Graphics.Effect;
using XnaLibrary.Graphics.Model;
using XnaLibraryContentPipeline.MaterialContents;
#endregion

namespace XnaLibraryContentPipeline.SkinnedModelPipeline
{
    /// <summary>
    /// アニメーション付きスキンモデル用のマテリアルプロセッサ
    /// </summary>
    [ContentProcessor]
    [DesignTimeVisible(false)]
    class SkinAnimeMaterialProcessor : MaterialProcessor
    {
        /// <summary>
        /// カスタムエフェクト
        /// </summary>
        [DisplayName("独自のエフェクトファイル")]
        [Description("独自のエフェクトファイルのパス。指定しない場合、組み込みのエフェクトを使用する")]
        public string CustomEffect
        {
            get { return customEffect; }
            set { customEffect = value; }
        }
        string customEffect = null;

        /// <summary>
        /// マテリアルの変換
        /// </summary>
        /// <param name="material">マテリアルコンテント</param>
        /// <param name="context">コンテンツプロセッサコンテクスト</param>
        /// <returns>変換したマテリアル</returns>
        public override MaterialContent Process(MaterialContent material, ContentProcessorContext context)
        {
            if (!(material is BasicMaterialContent))
            {
                throw new InvalidContentException(string.Format(
                    "入力メッシュは対応していない '{0}' を使用しています。BasicMaterialContent のみ対応しています。",
                    material.GetType()));
            }

            MaterialContent convertedMaterial;

            if (string.IsNullOrEmpty(customEffect))
            {
                convertedMaterial = new SkinAnimeMaterialContent();
            }
            else
            {
                var effectMaterial = new EffectMaterialContent();

                // エフェクトファイルを外部参照で設定
                effectMaterial.Effect = new ExternalReference<EffectContent>(Path.GetFullPath(customEffect));

                convertedMaterial = effectMaterial;
            }

            // 現在のマテリアル内のテクスチャに対してループ処理を実行し、新しいマテリアルに追加します。
            foreach (var textureContent in material.Textures)
            {
                convertedMaterial.Textures.Add(textureContent.Key, textureContent.Value);
            }

            // 現在のマテリアル内の不透明データに対してループ処理を実行し、新しいマテリアルに追加します。
            foreach (var opaqueData in material.OpaqueData)
            {
                convertedMaterial.OpaqueData.Add(opaqueData.Key, opaqueData.Value);
            }

            // カスタムエフェクトを返す
            return base.Process(convertedMaterial, context);
        }
    }
}

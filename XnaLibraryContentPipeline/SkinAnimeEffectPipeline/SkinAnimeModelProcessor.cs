﻿#region File Description
//-----------------------------------------------------------------------------
// SkinnedModelProcessor.cs
//
// Update: 2014/04/24
//
// Orignal File:
//     Microsoft XNA Community Game Platform
//     Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using XnaLibrary.Graphics.Effect;
using XnaLibrary.Graphics.Model;
using XnaLibraryContentPipeline.MaterialContents;
#endregion

namespace XnaLibraryContentPipeline.SkinnedModelPipeline
{
    /// <summary>
    /// アニメーション付きスキンモデル用モデルプロセッサ
    /// </summary>
    [ContentProcessor(DisplayName = "SkinAnime Model Processor")]
    public class SkinAnimeModelProcessor : ModelProcessor
    {
        /// <summary>
        /// 最大ボーン数
        /// </summary>
        protected virtual int MaxBones { get { return SkinAnimeEffect.MaxBones; } }

        /// <summary>
        /// 利用するマテリアルプロセッサ名
        /// </summary>
        protected virtual string TargetMaterialProcessor { get { return "SkinAnimeMaterialProcessor"; } }

        /// <summary>
        /// アニメーションの fbx があるディレクトリの相対パス
        /// 指定しない場合、ファイル名と同名のディレクトリを使用する
        /// </summary>
        [DisplayName("ディレクトリの相対パス")]
        [Description("アニメーションの fbx があるディレクトリの相対パス。指定しない場合、ファイル名と同名のディレクトリを使用する")]
        public string DirectoryPath
        {
            get { return directoryPath; }
            set { directoryPath = value; }
        }
        string directoryPath = null;

        /// <summary>
        /// カスタムエフェクト
        /// </summary>
        [DisplayName("独自のエフェクトファイル")]
        [Description("独自のエフェクトファイルのパス。指定しない場合、組み込みのエフェクトを使用する")]
        public string CustomEffect
        {
            get { return customEffect; }
            set { customEffect = value; }
        }
        string customEffect = null;

        /// <summary>
        /// NodeContent から ModelContent への変換処理
        /// MergeAnimations に入力されたアニメーションをマージします。
        /// NodeContent の情報から独自のアニメーションデータとして SkinningData を構築します。
        /// ModelContent に変換した後、Tag プロパティに SkinningData をセットします。
        /// </summary>
        /// <param name="input">ノードコンテント</param>
        /// <param name="context">コンテンツプロセッサコンテクスト</param>
        /// <returns>モデルコンテント</returns>
        public override ModelContent Process(NodeContent input, ContentProcessorContext context)
        {
            var modelDirectory = Path.GetDirectoryName(input.Identity.SourceFilename);
            if (string.IsNullOrEmpty(directoryPath))
            {
                directoryPath = Path.Combine(modelDirectory, Path.GetFileNameWithoutExtension(input.Identity.SourceFilename));
            }
            else
            {
                directoryPath = new FileInfo(Path.Combine(modelDirectory, directoryPath)).FullName;
            }

            if (!Directory.Exists(directoryPath))
            {
                throw new InvalidContentException(
                    string.Format("{0} のアニメーションのディレクトリである {1} が存在しません。", input.Identity.SourceFilename, directoryPath));
            }

            var mergeAnimationFiles = "";
            foreach (var mergeFile in Directory.GetFiles(directoryPath, "*.fbx", SearchOption.TopDirectoryOnly))
            {
                if (mergeFile != input.Identity.SourceFilename)
                {
                    mergeAnimationFiles += directoryPath + @"\" + System.IO.Path.GetFileNameWithoutExtension(mergeFile) + "|";
                }
            }

            if (string.IsNullOrEmpty(mergeAnimationFiles))
            {
                throw new InvalidContentException(directoryPath + "内に アニメーションの fbx ファイルが見つかりませんでした。");
            }

            // アニメーションをマージする
            foreach (var mergeFile in mergeAnimationFiles.Split('|')
                                                         .Select(s => s.Trim())
                                                         .Where(s => !string.IsNullOrEmpty(s)))
            {
                MergeAnimation(input, context, mergeFile + ".fbx");
            }

            // メッシュが有効なデータか確認する
            ValidateMesh(input, context, null);

            // ボーンオブジェクトを取得
            var skeleton = MeshHelper.FindSkeleton(input);
            if (skeleton == null)
            {
                throw new InvalidContentException("スケルトンが見つかりませんでした。");
            }

            // モデルのメッシュ情報を平滑化
            FlattenTransforms(input, skeleton);

            // 各種ボーン情報を構築
            var bones = MeshHelper.FlattenSkeleton(skeleton);
            if (bones.Count > MaxBones)
            {
                throw new InvalidContentException(
                    string.Format("スケルトンに制限を超える {0} 個のボーンがありました。最大ボーン数は {1}です。", bones.Count, MaxBones));
            }

            var bindPose = new List<Matrix>();
            var inverseBindPose = new List<Matrix>();
            var skeletonHierarchy = new List<int>();
            var boneIndices = new Dictionary<string, int>();
            foreach (var bone in bones)
            {
                bindPose.Add(bone.Transform);
                inverseBindPose.Add(Matrix.Invert(bone.AbsoluteTransform));
                skeletonHierarchy.Add(bones.IndexOf(bone.Parent as BoneContent));
                boneIndices.Add(bone.Name, boneIndices.Count);
            }

            // アニメーションデータを構築
            var animationClips = ProcessAnimations(skeleton.Animations, bones);

            // ベースクラスのメソッドを利用してNodeContentからModelContentに変換
            var model = base.Process(input, context);

            // 構築したアニメーションデータ、各種ボーン情報をSkinningDataとしてTagプロパティにセット
            model.Tag = new SkinningData(animationClips, bindPose, inverseBindPose, skeletonHierarchy, boneIndices);

            return model;
        }

        /// <summary>
        /// アニメーションをマージする
        /// </summary>
        /// <param name="input">ノードコンテント</param>
        /// <param name="context">コンテンツプロセッサコンテクスト</param>
        /// <param name="mergeFile">マージするファイルのパス</param>
        private void MergeAnimation(NodeContent input, ContentProcessorContext context, string mergeFile)
        {
            var mergeModel = context.BuildAndLoadAsset<NodeContent, NodeContent>(new ExternalReference<NodeContent>(mergeFile), null);

            var rootBone = MeshHelper.FindSkeleton(input);
            if (rootBone == null)
            {
                context.Logger.LogWarning(null, input.Identity, "ルートボーンがありません。");
                return;
            }

            var mergeRoot = MeshHelper.FindSkeleton(mergeModel);
            if (mergeRoot == null)
            {
                context.Logger.LogWarning(null, input.Identity, "マージするモデル '{0}' にルートボーンがありません。", mergeFile);
                return;
            }

            foreach (var animationName in mergeRoot.Animations.Keys)
            {
                if (rootBone.Animations.ContainsKey(animationName))
                {
                    context.Logger.LogWarning(null, input.Identity,
                        "すでにアニメーション '{1}' が存在するため、'{0}' からマージ出来ませんでした。",
                        animationName, mergeFile);

                    continue;
                }

                context.Logger.LogImportantMessage("アニメーション '{0}' のマージに成功しました。", animationName);

                rootBone.Animations.Add(animationName, mergeRoot.Animations[animationName]);
            }
        }

        /// <summary>
        /// NodeContent に含まれているアニメーションデータから独自形式のアニメーションデータを構築
        /// </summary>
        /// <param name="animations">アニメーションのコレクション</param>
        /// <param name="bones">ボーン</param>
        /// <returns>構築したアニメーションデータ</returns>
        private static Dictionary<string, AnimationClip> ProcessAnimations(AnimationContentDictionary animations, IList<BoneContent> bones)
        {
            // ボーン名をキーにしたボーンインデックス配列を作成
            var boneMap = new Dictionary<string, int>();

            for (int i = 0; i < bones.Count; i++)
            {
                var boneName = bones[i].Name;

                if (!string.IsNullOrEmpty(boneName))
                {
                    boneMap.Add(boneName, i);
                }
            }

            // クリップ名をキーにしたアニメショーン配列を作成
            var animationClips = new Dictionary<string, AnimationClip>();
            foreach (var animation in animations)
            {
                var processed = ProcessAnimation(animation.Value, boneMap);

                animationClips.Add(animation.Key, processed);
            }

            if (animationClips.Count == 0)
            {
                throw new InvalidContentException("入力ファイルにアニメーションが含まれていません。");
            }

            return animationClips;
        }

        /// <summary>
        /// アニメーションデータを構築
        /// </summary>
        /// <param name="animation">アニメーションコンテント</param>
        /// <param name="boneMap">ボーンマップ</param>
        /// <returns>構築したアニメーションデータ</returns>
        private static AnimationClip ProcessAnimation(AnimationContent animation, Dictionary<string, int> boneMap)
        {
            var keyframes = new List<Keyframe>();

            foreach (var channel in animation.Channels)
            {
                // ボーン名をキーにしてボーンインデックスを取得
                int boneIndex;

                if (!boneMap.TryGetValue(channel.Key, out boneIndex))
                {
                    throw new InvalidContentException(
                        string.Format("スケルトンの一部でないボーン '{0}' のアニメーションが見つかりました。", channel.Key));
                }

                // キーフレーム配列を作成
                foreach (var keyframe in channel.Value)
                {
                    keyframes.Add(new Keyframe(boneIndex, keyframe.Time, keyframe.Transform));
                }
            }

            // キーフレーム配列を昇順にソート
            keyframes.Sort(CompareKeyframeTimes);

            if (keyframes.Count == 0)
            {
                throw new InvalidContentException("アニメーションにキーフレームがありません。");
            }

            if (animation.Duration <= TimeSpan.Zero)
            {
                throw new InvalidContentException("アニメーション時間が０です。");
            }

            return new AnimationClip(animation.Name, animation.Duration, keyframes);
        }

        /// <summary>
        /// キーフレーム配列を昇順でソートするための時間比較機能
        /// </summary>
        /// <param name="a">キーフレーム A</param>
        /// <param name="b">キーフレーム B</param>
        /// <returns>時間が長いか短いか同じか</returns>
        private static int CompareKeyframeTimes(Keyframe a, Keyframe b)
        {
            return a.Time.CompareTo(b.Time);
        }

        /// <summary>
        /// メッシュが有効なデータか確認
        /// </summary>
        /// <param name="node">ノードコンテント</param>
        /// <param name="context">コンテンツプロセッサコンテクスト</param>
        /// <param name="parentBoneName">親ボーンの名前</param>
        private static void ValidateMesh(NodeContent node, ContentProcessorContext context, string parentBoneName)
        {
            var mesh = node as MeshContent;

            if (mesh != null)
            {
                // メッシュを確認する
                if (parentBoneName != null)
                {
                    context.Logger.LogWarning(null, null,
                        "メッシュ '{0}' はボーン '{1}' の子です。ボーンの子であるメッシュは正しく処理できません。",
                        mesh.Name, parentBoneName);
                }

                if (!MeshHasSkinning(mesh))
                {
                    context.Logger.LogWarning(null, null,
                        "メッシュ '{0}' にスキニング情報がないため、削除されました。",
                        mesh.Name);

                    mesh.Parent.Children.Remove(mesh);
                    return;
                }
            }
            else if (node is BoneContent)
            {
                parentBoneName = node.Name;
            }

            // 再帰呼び出し
            foreach (var child in node.Children)
            {
                ValidateMesh(child, context, parentBoneName);
            }
        }

        /// <summary>
        /// メッシュがスキニング情報を含むか確認
        /// </summary>
        /// <param name="mesh">メッシュコンテント</param>
        /// <returns>スキニング情報を含むか</returns>
        private static bool MeshHasSkinning(MeshContent mesh)
        {
            foreach (var geometry in mesh.Geometry)
            {
                if (!geometry.Vertices.Channels.Contains(VertexChannelNames.Weights()))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// モデルの全メッシュ情報を平滑化
        /// </summary>
        /// <param name="node">ノードコンテント</param>
        /// <param name="skeleton">スケルトン</param>
        private static void FlattenTransforms(NodeContent node, BoneContent skeleton)
        {
            foreach (NodeContent child in node.Children)
            {
                // スケルトンは処理しない
                if (child == skeleton)
                {
                    continue;
                }

                // ローカル座標でシーンの階層を更新
                MeshHelper.TransformScene(child, child.Transform);

                // 単位マトリックスを設定
                child.Transform = Matrix.Identity;

                // 再帰呼び出し
                FlattenTransforms(child, skeleton);
            }
        }

        /// <summary>
        /// マテリアルの変換
        /// </summary>
        /// <param name="material">マテリアルコンテント</param>
        /// <param name="context">コンテンツプロセッサコンテクスト</param>
        /// <returns>変換したマテリアル</returns>
        protected override MaterialContent ConvertMaterial(MaterialContent material, ContentProcessorContext context)
        {
            OpaqueDataDictionary processorParameters = new OpaqueDataDictionary();
            processorParameters["CustomEffect"] = customEffect ?? "";
            processorParameters["ColorKeyColor"] = ColorKeyColor;
            processorParameters["ColorKeyEnabled"] = ColorKeyEnabled;
            processorParameters["TextureFormat"] = TextureFormat;
            processorParameters["GenerateMipmaps"] = GenerateMipmaps;
            processorParameters["ResizeTexturesToPowerOfTwo"] = ResizeTexturesToPowerOfTwo;

            return context.Convert<MaterialContent, MaterialContent>(material, TargetMaterialProcessor, processorParameters);

            /*
            if (!(material is BasicMaterialContent))
            {
                throw new InvalidContentException(string.Format(
                    "入力メッシュは対応していない '{0}' を使用しています。BasicMaterialContent のみ対応しています。",
                    material.GetType()));
            }

            MaterialContent convertedMaterial;

            if (string.IsNullOrEmpty(customEffect))
            {
                convertedMaterial = new SkinAnimeMaterialContent();
            }
            else
            {
                var effectMaterial = new EffectMaterialContent();

                // エフェクトファイルを外部参照で設定
                effectMaterial.Effect = new ExternalReference<EffectContent>(Path.GetFullPath(customEffect));

                convertedMaterial = effectMaterial;
            }

            // 現在のマテリアル内のテクスチャに対してループ処理を実行し、新しいマテリアルに追加します。
            foreach (var textureContent in material.Textures)
            {
                convertedMaterial.Textures.Add(textureContent.Key, textureContent.Value);
            }

            // 現在のマテリアル内の不透明データに対してループ処理を実行し、新しいマテリアルに追加します。
            foreach (var opaqueData in material.OpaqueData)
            {
                convertedMaterial.OpaqueData.Add(opaqueData.Key, opaqueData.Value);
            }

            // カスタムエフェクトを返す
            return base.ConvertMaterial(convertedMaterial, context);*/
        }

        /// <summary>
        /// 全てのマテリアルに BasicEffect を適用し、変更できないようにする
        /// </summary>
        [Browsable(false)]
        [DefaultValue(MaterialProcessorDefaultEffect.BasicEffect)]
        public override MaterialProcessorDefaultEffect DefaultEffect
        {
            get { return MaterialProcessorDefaultEffect.BasicEffect; }
        }
    }
}
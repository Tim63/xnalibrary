﻿#region File Description
//-----------------------------------------------------------------------------
// SkinAnimeShadowModelProcessor.cs
//
// Update: 2014/04/22
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using XnaLibrary.Graphics.Effect;
using XnaLibraryContentPipeline.MaterialContents;
#endregion

namespace XnaLibraryContentPipeline.SkinnedModelPipeline
{
    /// <summary>
    /// アニメーション付き、影の描画可能なスキンモデル用モデルプロセッサ
    /// </summary>
    [ContentProcessor(DisplayName = "SkinAnime Shadow Model Processor")]
    public class SkinAnimeShadowModelProcessor : SkinAnimeModelProcessor
    {
        /// <summary>
        /// 最大ボーン数
        /// </summary>
        protected override int MaxBones { get { return SkinAnimeShadowEffect.MaxBones; } }

        /// <summary>
        /// 利用するマテリアルプロセッサ名
        /// </summary>
        protected override string TargetMaterialProcessor { get { return "SkinAnimeShadowMaterialProcessor"; } }

        /*
        /// <summary>
        /// マテリアルの変換
        /// </summary>
        /// <param name="material">マテリアルコンテント</param>
        /// <param name="context">コンテンツプロセッサコンテクスト</param>
        /// <returns>変換したマテリアル</returns>
        protected override MaterialContent ConvertMaterial(MaterialContent material, ContentProcessorContext context)
        {
            if (!(material is BasicMaterialContent))
            {
                throw new InvalidContentException(string.Format(
                    "入力メッシュは対応していない '{0}' を使用しています。BasicMaterialContent のみ対応しています。",
                    material.GetType()));
            }

            var effectMaterial = new SkinnedShadowEffectMaterialContent();

            var effectContent = new EffectContent();
            effectContent.EffectCode = Properties.Resources.SkinnedShadowEffect;

            var effectProcessor = new EffectProcessor();
            effectMaterial.CompiledEffect = effectProcessor.Process(effectContent, context);

            // 現在のマテリアル内のテクスチャに対してループ処理を実行し、新しいマテリアルに追加します。
            foreach (var textureContent in material.Textures)
            {
                effectMaterial.Textures.Add(textureContent.Key, textureContent.Value);
            }

            // 現在のマテリアル内の不透明データに対してループ処理を実行し、新しいマテリアルに追加します。
            foreach (var opaqueData in material.OpaqueData)
            {
                effectMaterial.OpaqueData.Add(opaqueData.Key, opaqueData.Value);
            }

            // カスタムエフェクトを返す
            return base.ConvertMaterial(effectMaterial, context);
        }*/
    }
}
﻿#region File Description
//-----------------------------------------------------------------------------
// AnimationClipWriter.cs
//
// Update: 2014/04/24
//
// Orignal File:
//     TypeWriters.cs
//     Microsoft XNA Community Game Platform
//     Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using XnaLibrary.Content;
using XnaLibrary.Graphics.Model;
#endregion

namespace XnaLibraryContentPipeline.TypeWriters
{
    /// <summary>
    /// AnimationClip の書き出し
    /// </summary>
    [ContentTypeWriter]
    internal class AnimationClipWriter : BaseTypeWriter<AnimationClip>
    {
        protected override System.Reflection.Assembly RuntimeAssembly { get { return typeof(AnimationClip).Assembly; } }

        /// <summary>
        /// 書き出し
        /// </summary>
        /// <param name="output">コンテンツライター</param>
        /// <param name="value">書き出すデータ</param>
        protected override void Write(ContentWriter output, AnimationClip value)
        {
            output.WriteObject(value.Name);
            output.WriteObject(value.Duration);
            output.WriteObject(value.Keyframes);
        }
        /*
        /// <summary>
        /// ランタイムリーダーの取得
        /// </summary>
        /// <param name="targetPlatform">ターゲットプラットフォーム</param>
        /// <returns>ランタイムリーダー</returns>
        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return typeof(AnimationClipReader).AssemblyQualifiedName;
        }*/
    }
}

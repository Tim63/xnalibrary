﻿#region File Description
//-----------------------------------------------------------------------------
// BaseEffectTypeWriter.cs
//
// Update: 2014/04/22
//
// Reference: Microsoft.Xna.Framework.Graphics.dll
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Reflection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using XnaLibrary.Graphics.Effect;
using XnaLibraryContentPipeline.MaterialContents;
#endregion

namespace XnaLibraryContentPipeline.TypeWriters
{
    internal abstract class BaseTypeWriter<T> : ContentTypeWriter<T>
    {
        protected abstract Assembly RuntimeAssembly { get; }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            var text = GetType().Name.Replace("Writer", "Reader");
            text += TypeWriterHelpers.GetGenericArgumentRuntimeTypes(this, targetPlatform);
            if (RuntimeAssembly != null)
            {
                text += ", " + TypeWriterHelpers.GetAssemblyFullName(RuntimeAssembly, targetPlatform);
            }
            return TypeWriterHelpers.BaseNamespace + '.' + text;
        }
    }
}

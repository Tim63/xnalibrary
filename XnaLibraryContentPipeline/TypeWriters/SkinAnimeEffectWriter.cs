﻿#region File Description
//-----------------------------------------------------------------------------
// SkinAnimeEffectWriter.cs
//
// Update: 2014/04/24
//
// Reference: Microsoft.Xna.Framework.Graphics.dll
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using XnaLibrary.Graphics.Effect;
using XnaLibraryContentPipeline.MaterialContents;
#endregion

namespace XnaLibraryContentPipeline.TypeWriters
{
    /// <summary>
    /// SkinAnimeEffect の書き出し
    /// </summary>
    [ContentTypeWriter]
    internal class SkinAnimeEffectWriter : BaseTypeWriter<SkinAnimeMaterialContent>
    {
        protected override System.Reflection.Assembly RuntimeAssembly { get { return typeof(SkinAnimeEffect).Assembly; } }

        /// <summary>
        /// 書き出し
        /// </summary>
        /// <param name="output">コンテンツライター</param>
        /// <param name="value">書き出すデータ</param>
        protected override void Write(ContentWriter output, SkinAnimeMaterialContent value)
        {
            output.WriteExternalReference<TextureContent>(value.Texture);
            output.Write(value.DiffuseColor.GetValueOrDefault(Vector3.One));
            output.Write(value.EmissiveColor.GetValueOrDefault(Vector3.Zero));
            output.Write(value.SpecularColor.GetValueOrDefault(Vector3.One));
            output.Write(value.SpecularPower.GetValueOrDefault(16f));
            output.Write(value.Alpha.GetValueOrDefault(1f));
        }
    }
}

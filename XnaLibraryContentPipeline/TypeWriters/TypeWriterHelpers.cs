﻿#region File Description
//-----------------------------------------------------------------------------
// TypeWriterHelpers.cs
//
// Update: 2014/04/24
//
// Reference: Microsoft.Xna.Framework.Graphics.dll
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Reflection;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
#endregion

namespace XnaLibraryContentPipeline.TypeWriters
{
    internal class TypeWriterHelpers
    {
        private static Type typeWriterType = typeof(ContentTypeWriter);

        private static MethodInfo getAssemblyFullNameInfo =
            typeWriterType.GetMethod("GetAssemblyFullName", BindingFlags.Static | BindingFlags.NonPublic);

        private static MethodInfo getGenericArgumentRuntimeTypesInfo =
            typeWriterType.GetMethod("GetGenericArgumentRuntimeTypes", BindingFlags.Instance | BindingFlags.NonPublic);

        public const string BaseNamespace = "XnaLibrary.Content";

        /// <summary>
        /// アセンブリの表示名を取得
        /// </summary>
        /// <param name="assembly">アセンブリ</param>
        /// <param name="targetPlatform">ターゲットプラットフォーム</param>
        /// <returns>アセンブリの表示名</returns>
        public static string GetAssemblyFullName(Assembly assembly, TargetPlatform targetPlatform)
        {
            return getAssemblyFullNameInfo.Invoke(null, new object[] { assembly, targetPlatform }) as string;
        }

        /// <summary>
        /// ジェネリック型の引数のランタイムタイプを取得
        /// </summary>
        /// <param name="writer">コンテントタイプライター</param>
        /// <param name="targetPlatform">ターゲットプラットフォーム</param>
        /// <returns>ランタイムタイプ名</returns>
        public static string GetGenericArgumentRuntimeTypes(ContentTypeWriter writer, TargetPlatform targetPlatform)
        {
            return getGenericArgumentRuntimeTypesInfo.Invoke(writer, new object[] { targetPlatform }) as string;
        }
    }
}

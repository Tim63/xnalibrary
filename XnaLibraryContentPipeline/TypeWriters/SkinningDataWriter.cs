﻿#region File Description
//-----------------------------------------------------------------------------
// SkinningDataWriter.cs
//
// Update: 2014/04/24
//
// Orignal File:
//     TypeWriters.cs
//     Microsoft XNA Community Game Platform
//     Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using XnaLibrary.Content;
using XnaLibrary.Graphics.Model;
#endregion

namespace XnaLibraryContentPipeline.TypeWriters
{
    /// <summary>
    /// SkinnedData の書き出し
    /// </summary>
    [ContentTypeWriter]
    internal class SkinningDataWriter : BaseTypeWriter<SkinningData>
    {
        protected override System.Reflection.Assembly RuntimeAssembly { get { return typeof(SkinningData).Assembly; } }

        /// <summary>
        /// 書き出し
        /// </summary>
        /// <param name="output">コンテンツライター</param>
        /// <param name="value">書き出すデータ</param>
        protected override void Write(ContentWriter output, SkinningData value)
        {
            output.WriteObject(value.AnimationClips);
            output.WriteObject(value.BindPose);
            output.WriteObject(value.InverseBindPose);
            output.WriteObject(value.SkeletonHierarchy);
            output.WriteObject(value.BoneIndices);
        }
        /*
        /// <summary>
        /// ランタイムリーダーの取得
        /// </summary>
        /// <param name="targetPlatform">ターゲットプラットフォーム</param>
        /// <returns>ランタイムリーダー</returns>
        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return typeof(SkinningDataReader).AssemblyQualifiedName;
        }*/
    }
}
